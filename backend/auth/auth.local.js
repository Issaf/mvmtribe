const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const models = require('../models');

const localConfig = { usernameField: 'email' };

async function verifyCallback(email, password, done) {
    try {
        const user = await models.User.find({ where:{ email: email} })

        // Return if user not found in database
        if (!user) {
            return done(null, false, {type: 'error', msg: 'Oops! No user found'});
        }
        // Return if password is wrong
        if (!user.validPassword(password)) {
            return done(null, false, {type: 'error', msg: 'Oops! Wrong password'});
        }
        // If credentials are correct, return the user object
        return done(null, user);
    } catch(err) {
            return done(err);
    }
}

passport.use(new LocalStrategy(localConfig, verifyCallback));