const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const models = require('../models');
const config = require('../config/global.conf');
const _ = require('lodash');

const fbConfig = {
    clientID: config.FACEBOOK_APP_ID,
    clientSecret: config.FACEBOOK_APP_SECRET,
    callbackURL: 'http://localhost:4200' + '/api/social-auth/facebook/callback'
};

async function verifyCallback(accessToken, refreshToken, profile, done) {
    // find one or create a user here
    try {
        // effective email that would be saved with user account, fb email if received, {id}@fb.com email otherwise
        let userEmail;
        if(profile.emails.length > 0) {
            userEmail = profile.emails[0].value;
        } else {
            userEmail = profile.id + '@fb.com';
        }
        const user = await models.User.find({ where: { email: userEmail } });

        if(user) {
            models.User.updateLastLogin(user.id, models);
            return done(null, user);
        }
        // Return if user not found in database
        if (!user) {
            // create one and then return
            const newuser = await models.User.create({ email: userEmail, firstName: _.get(profile, 'name.givenName', null), verified: true, provider: profile.provider, userType: 'pro' });
            return done(null, newuser);
        }        
    } catch(err) {
            return done(err);
    }
}

 
passport.use(new FacebookStrategy(fbConfig, verifyCallback));