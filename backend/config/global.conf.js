// Use this file to create config.conf.js. config.conf.js contains any 
// configuration that could differe from environment to environment
// Configuration should be mentioned in such a way that there is 
// no need of checking environment in code
// All configs should be available through out the app using config.[configOf]

// var sequelizeConfig = require('./sequelize.conf');

var config = {};
config.env = 'development'; // development | staging | production


// Sequilize
switch (config.env) {
    case 'production':
        config.sequelize = sequelizeConfig.production;
        break;
    case 'staging':
        config.sequelize = sequelizeConfig.staging;
        break;
    case 'development':
        config.adminPassword = "SkJ4%sl@r93#dW";
        config.jwtSecret = 'MY_SECRET';
        config.mjApiKey = "fbed4d843fdffa29f7112d9aa1f5b962";
        config.mjSecretKey = "2c1c78ee60e78c5efb01cb51d1d70b68";
        config.FACEBOOK_APP_ID = "281058132717490";
        config.FACEBOOK_APP_SECRET = "981e1451f06e24772fe5f59851ecc80e";
        // config.sequelize = sequelizeConfig.development;
        config.s3KeyId = "AKIAIH7K7RNUNH7FKIJQ";
        config.s3KeySecret = "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";
        config.s3Region = "ca-central-1";
        config.s3BucketName = "mvmtribe";
        config.s3ProfilePicturesFolder = "proProfilePictures";
        config.s3ProductPicturesFolder = "productPictures";
        config.s3ArticleFolder = 'articlesFolder';
        config.host = "localhost";
        config.port = 3000;
        break;
}

module.exports = config;