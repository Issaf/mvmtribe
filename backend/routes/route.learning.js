const express = require('express');
const asyncHandler = require('express-async-handler');
const models = require('../models');
const config = require('../config/global.conf');
var fs = require('fs');
const AWS = require('aws-sdk');
const learningRoute = express.Router();

// ================
// Categories CRUD
// ================

learningRoute.get('/categories', asyncHandler(async (req, res, next) => {
  // listFromDB = ['back pain', 'strength training', 'movement', 'mobility', 'posture', 'rehab', 'aerobics', 'pain release'];
  rawData = await models.LearningCategory.findAll({attributes: ['id', 'name', 'description']});
  res.json({list: rawData});
}));


learningRoute.post('/categories', asyncHandler(async (req, res, next) => {
  await models.LearningCategory.create({name: req.body.name, description: req.body.description});
  res.json({msg: 'Successfully added'});
}));


learningRoute.patch('/categories', asyncHandler(async (req, res, next) => {
  category = await models.LearningCategory.find({where: {id: req.body.id }});
  oldName = category.name;
  await category.update({name: req.body.name, description: req.body.description} , { fields: ['name', 'description'] });
  await models.Article.update({category: req.body.name}, {where: {category: oldName}});
  res.json({msg: 'Successfully updated'});
}));


learningRoute.delete('/categories/:name', asyncHandler(async (req, res, next) => {
  // once articles table is set up and populated, make sure to check it's not used before deleting
  category = await models.LearningCategory.find({where: {name: req.params.name }});
  await category.destroy();
  res.json({msg: 'Successfully deleted'});
}));


// =============
// Articles CRUD
// =============

learningRoute.get('/:article', asyncHandler(async (req, res, next) => {
  rawData = await models.Article.find({ where: {title: req.params.article}});
  
  nameParts = rawData.contentFileUrl.split('/');
  var bucketName = config.s3BucketName 
  var pathToFile =   config.s3ArticleFolder + 
    '/' + nameParts[nameParts.length -2] +
    '/' + nameParts[nameParts.length -1];
  AWS.config.credentials = {accessKeyId: config.s3KeyId, secretAccessKey: config.s3KeySecret};
  AWS.config.region = config.s3Region;
  var s3Bucket = new AWS.S3();
  
  s3Bucket.getObject({Bucket: bucketName, Key: pathToFile}, async(err, data) => {
    if (err) {
      res.json({type: 'error'});
    } else {
      let objectData = data.Body.toString('utf-8');
      res.json({type: 'success', infos: rawData, content: objectData});
    }
  });
  

}));

// Fetches random  2 articles
learningRoute.get('/articles/not/:id', asyncHandler(async (req, res, next) => {
  rawData = await models.Article.findAll({
    limit: 2,
    where: {id: {[models.Sequelize.Op.not]: req.params.id}},
    order: models.sequelize.random()
  });
  res.json({list: rawData});
}));


learningRoute.get('/articles/:category', asyncHandler(async (req, res, next) => {
  rawData = await models.Article.findAll({ where: {category: req.params.category}});
  res.json({list: rawData});
}));


learningRoute.post('/articles', asyncHandler(async (req, res, next) => {
  await models.Article.create(req.body);
  res.json({msg: 'Successfully added'});
}));


learningRoute.patch('/articles', asyncHandler(async (req, res, next) => {
  article = await models.Article.find({where: {id: req.body.id }});
  await article.update(req.body.newVersion);
  res.json({msg: 'Successfully updated'});
}));


learningRoute.delete('/articles/:id', asyncHandler(async (req, res, next) => {
  article = await models.Article.find({where: {id: req.params.id }});
  await article.destroy();
  res.json({msg: 'Successfully deleted'});
}));


// ===========
// Middlewares
// ===========

module.exports = learningRoute;