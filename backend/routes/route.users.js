const express = require('express');
const asyncHandler = require('express-async-handler');
const passport = require('passport');
const models = require('../models');
const jwt = require('jsonwebtoken');
const config = require('../config/global.conf');
const mailjet = require ('node-mailjet').connect(config.mjApiKey, config.mjSecretKey);
const usersRoute = express.Router();

usersRoute.get('/', (req, res) => {
  res.send('users api works');
})

usersRoute.post('/signup', asyncHandler(async (req, res, next) => {
    var fields = ['firstName', 'email', 'userType'];
    const existingUser = await models.User.find({ where: {email: req.body.email} })
    if (existingUser != null) {
      res.json({type:'error', msg: 'Email already used'})
    } else {
      const user = await models.User.build(req.body, { fields: fields });
      user.setPassword(req.body.password);
      user.save();
      
      await models.SpecialistDetails.create({firstName:req.body.firstName, email: req.body.email});
      
      linkToken = user.generateJwt(1);
      const request = mailjet
      .post("send")
      .request({
          "FromEmail":"donotreply@mvmtribe.com",
          "FromName":"MVMTRIBE",
          "Subject":"Email Verification",
          "Mj-TemplateID": "524292",
          "Mj-TemplateLanguage":"true",
          "Vars": {"url": "http://localhost:4200/completion-step/" + linkToken },
          "Recipients":[{"Email": req.body.email}]
      })
      request
      .then(result => {
        res.json({type: 'success', msg: "Your account has been created successfully"});
      })
      .catch(err => {
        res.json({type: 'error', msg: "Error while trying to send the email confirmation. Please login to receive a new one."});
      })
    }
}))


usersRoute.post('/login', async(req, res, next) => {
  passport.authenticate('local', { session: false }, async(err, user, info) => {
    // If Passport throws/catches an error
    if (err) {
      res.json({type:"error", msg: "Error logging in. Please try again"});
    }
    else if (user) {
      if (user.verified) {
      models.User.updateLastLogin(user.id, models);
      res.json({type: "success", msg: "Successfully logged in", token: user.generateJwt(7)});

    } else {
      linkToken = user.generateJwt(1);
      const request = mailjet
      .post("send")
      .request({
          "FromEmail":"donotreply@mvmtribe.com",
          "FromName":"MVMTRIBE",
          "Subject":"Email Verification",
          "Mj-TemplateID": "524292",
          "Mj-TemplateLanguage":"true",
          "Vars": {"url": "http://localhost:4200/completion-step/" + linkToken },
          "Recipients":[{"Email": user.email}]
      })
      request
      .then(result => {
        res.json({type: 'needConfirmation', msg: "Confirmation Email Re-sent"});
      })
      .catch(err => {
        res.json({type: 'error', msg: "Error while trying to send the email confirmation. Please login to receive a new one."});
      })
    }
    } else {
      // If user is not found
      res.json({type:"error", msg: "Was not able to login with given email and password"});
    }
  })(req, res);

});


usersRoute.post('/decodeToken', async(req, res, next) => {
  const token = req.body.token;
  const decodedJwt = jwt.verify(token, config.jwtSecret, function(err, decodedJwt) {
    if (err) {
      res.json({msg: 'error'});
    } else {
      res.json(decodedJwt);
    }
  });
})

usersRoute.post('/reset-password', async(req, res, next) => {
  user = await models.User.find({ where: {email: req.body.email}});
  linkToken = user.generateJwt(1);
  const request = mailjet
  .post("send")
  .request({
      "FromEmail":"donotreply@mvmtribe.com",
      "FromName":"MVMTRIBE",
      "Subject":"Password Reset",
      "Mj-TemplateID": "524354",
      "Mj-TemplateLanguage":"true",
      "Vars": {"url": "http://localhost:4200/forgot-password;token=" + linkToken },
      "Recipients":[{"Email": req.body.email}]
  })
  request
  .then(result => {
    res.json({type: 'success', msg: "Reset email sent"});
  })
  .catch(err => {
    res.json({type: 'error', msg: "Reset email not sent"});
  })
})


usersRoute.patch('/updatePassword', async(req, res, next) => {
  var user = await models.User.find({where: {email: req.body.email}});
  user.setPassword(req.body.password);
  user.save();
  res.json({msg: 'success'});
})


usersRoute.patch('/updateVerification', async(req, res, next) => {
  var verifiedUser = await models.User.find({where: {email: req.body.email}});
  await verifiedUser.update({verified: true} , { fields: ['verified'] });
  res.json({msg: 'Successfully verified'});
})


// =========
// A d m i n
// =========

// // ONE TIME ONLY TO CREATE ADMIN ACCOUNT, DELETE CODE AND PASSWORD VARIABLE AFTER ITS DONE 
// usersRoute.get('/other/237097858480934839834902828293', async(req, res, next) => {
//   const existingUsers = await models.User.findAll({ where: {userType: 'admin'} })
//   if (existingUsers && existingUsers.length >= 1) {
//     res.status(454).json({msg: 'ignored'});
//   } else {
//     const user = await models.User.build({email: 'boss@inhere.com', userType: 'admin'});
//     user.setPassword(config.adminPassword);
//     user.save();
//     res.status(454).json({msg: 'created'});
//   }
// });


usersRoute.post('/other/3070818948930944333033120', async(req, res, next) => {
  const existingUsers = await models.User.findAll({ where: {userType: 'admin'} })
  if (existingUsers && existingUsers.length > 1) {
    let token = user.shortJwt(10);
    const request = mailjet
    .post("send")
    .request({
        "FromEmail":"donotreply@mvmtribe.com",
        "FromName":"MVMTRIBE",
        "Subject":"Admin access",
        "Mj-TemplateID": "534060",
        "Mj-TemplateLanguage":"true",
        "Vars": {"time": new Date().toString(), "status": 'denied - more than one admin' },
        "Recipients":[{"Email": "isafsaf@mvmtribe.com"}]
    })
  request
  .then(result => {
    res.json({msg: 'ignored'});
  })
  .catch(err => {
    res.json({msg: 'error email'});
  })

  } else {

      user = existingUsers[0];
      if (user.validPassword(req.body.value)) {
        let token = user.shortJwt(10);
        const request = mailjet
        .post("send")
        .request({
            "FromEmail":"donotreply@mvmtribe.com",
            "FromName":"MVMTRIBE",
            "Subject":"Admin access",
            "Mj-TemplateID": "534060",
            "Mj-TemplateLanguage":"true",
            "Vars": {"time": new Date().toString(), "status": 'accessed' },
            "Recipients":[{"Email": "isafsaf@mvmtribe.com"}]
        })
      request
      .then(result => {
        res.json({msg: 'passed', token: token});
      })
      .catch(err => {
        res.json({msg: 'error email'});
      })
      } else {
        const request = mailjet
        .post("send")
        .request({
            "FromEmail":"donotreply@mvmtribe.com",
            "FromName":"MVMTRIBE",
            "Subject":"Admin access",
            "Mj-TemplateID": "534060",
            "Mj-TemplateLanguage":"true",
            "Vars": {"time": new Date().toString(), "status": 'denied' },
            "Recipients":[{"Email": "isafsaf@mvmtribe.com"}]
        })
      request
      .then(result => {
        res.json({msg: 'denied'});
      })
      .catch(err => {
        res.json({msg: 'error email'});
      })
  }
}
})




module.exports = usersRoute;