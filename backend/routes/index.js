var express = require('express');
const models = require('../models');
const asyncHandler = require('express-async-handler');

const users = require('./route.users');
const specialists = require('./route.specialists');
const learning = require('./route.learning');
const tools = require('./route.tools');
const socialAuth = require('./route.social-auth');

var router = express.Router();
router.use('/users', users);
router.use('/specialists', specialists);
router.use('/learning', learning);
router.use('/tools', tools);
router.use('/social-auth', socialAuth);

// ===================================================================================

router.post('/saveEmail', asyncHandler(async (req, res, next) => {
  existsAlready = await models.EmailList.find({where: {email: req.body.email}});
  if (!existsAlready) {
    await models.EmailList.create({email: req.body.email, articleId: req.body.articleId});
  }
  res.json({msg: 'Email successfully added'});
}));


module.exports = router;