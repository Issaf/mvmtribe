const express = require('express');
const passport = require('passport');
const socialAuthRoute = express.Router();

socialAuthRoute.get('/facebook', passport.authenticate('facebook'));


socialAuthRoute.get('/facebook/callback', passport.authenticate('facebook', { session: false }),async(req,res, next) => {
  let token = null;
  const user = req.user;
  token = user.generateJwt(7);
  res.redirect('/login-with-token/' + token);
});


// socialAuthRoute.get('/google', (req, res) => {
//   passport.authenticate('google', { 
//     scope : ['profile','email'],
//     state: req.query.agent
//    })(req,res);
// });


// socialAuthRoute.get('/google/callback', passport.authenticate('google', { session: false }), (req,res) => {
//   let token = null;
//   const user = req.user;
//   token = user.generateJwt();
//   res.redirect('/login-with-token/'+token);
// });


module.exports = socialAuthRoute;


