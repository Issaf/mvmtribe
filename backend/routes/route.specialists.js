const express = require('express');
const asyncHandler = require('express-async-handler');
const models = require('../models');
const config = require('../config/global.conf');
const AWS = require('aws-sdk');
const multer = require('multer');
var storage = multer.memoryStorage();
const upload = multer({ storage: storage });
const specialistsRoute = express.Router();

// ================
// Specialties CRUD
// ================

specialistsRoute.get('/specialties', asyncHandler(async (req, res, next) => {
  // listFromDB = ['back pain', 'strength training', 'movement', 'mobility', 'posture', 'rehab', 'aerobics', 'pain release'];
  rawData = await models.Specialty.findAll({attributes: ['name']});
  listFromDB = [];
  for (let i=0 ; i < Object.keys(rawData).length ; i++ ) {
    listFromDB.push(rawData[i]['name']);
  }
  res.json({list: listFromDB});
}));


specialistsRoute.post('/specialties', asyncHandler(async (req, res, next) => {
  await models.Specialty.findOrCreate({where: {name: req.body.name }});
  res.json({msg: 'Successfully added'});
}));


specialistsRoute.patch('/specialties', asyncHandler(async (req, res, next) => {
  category = await models.Specialty.find({where: {name: req.body.old }});
  await category.update({name: req.body.new} , { fields: ['name'] });
  
  await models.SpecialistDetails.update(
    {mainSpecialty: req.body.new}, {where: {mainSpecialty: req.body.old}});
  await models.SpecialistDetails.update(
    {secondSpecialty: req.body.new}, {where: {secondSpecialty: req.body.old}});

  res.json({msg: 'Successfully updated'});
}));


specialistsRoute.delete('/specialties/:name', asyncHandler(async (req, res, next) => {
  affectedRows = await models.SpecialistDetails.findAll({where: {
      [models.Sequelize.Op.or]: [{mainSpecialty: req.params.name}, {secondSpecialty: req.params.name}]
    }});
  if (affectedRows.length === 0) {
    category = await models.Specialty.find({where: {name: req.params.name }});
    await category.destroy();
    res.json({msg: 'Successfully deleted'});
  } else {
    res.json({msg: 'Cannot delete used specialty'});
  }
}));


// ==============
// Job Titles GET
// ==============

specialistsRoute.get('/job-titles', asyncHandler(async (req, res, next) => {
  var defaultList = ["personal trainer", "movement coach", "physiotherapist", "chiropractor"];
  rawData = await models.SpecialistDetails.findAll({ attributes: ['jobTitle'] });
  listFromDB = [];
  rawData.forEach(element => {
    listFromDB.push(element.jobTitle);
  });
  res.json({list: listFromDB});
}))

// ===================
// Specialists Details
// ===================

specialistsRoute.post('', asyncHandler(async (req, res, next) => {
  fields = ["id", "firstName", "lastName", "city", "jobTitle", "profilePicURL"];
  var filters = {completeProfile: true};
  if (req.body.bySpecialty == 'true') {
    if (req.body.specialty) { filters['mainSpecialty'] = req.body.specialty; }
    if (req.body.city) { filters['city'] = req.body.city; }
  } else {
    if (req.body.jobTitle) { filters['jobTitle'] = req.body.jobTitle; }
    if (req.body.city) { filters['city'] = req.body.city; }
  }
  rawData = await models.SpecialistDetails.findAll({attributes: fields, where: filters});
  res.json({list: rawData});
}));


specialistsRoute.get('/:email', asyncHandler(async (req, res, next) => {
  fields = ["id", "firstName", "lastName", "email", "phone", "website",
            "city", "languages", "jobTitle", "institution", "flexible","bio", 
            "mainSpecialty", "secondSpecialty", "profilePicURL", "completeProfile"];
  rawData = await models.SpecialistDetails.find({attributes: fields, where: {email: req.params.email }});
  res.json({list: rawData});
}));

specialistsRoute.get('/byId/:id', asyncHandler(async (req, res, next) => {
  fields = ["firstName", "lastName", "email", "phone", "website",
            "city", "languages", "jobTitle", "institution", "flexible","bio", 
            "mainSpecialty", "secondSpecialty", "profilePicURL", "completeProfile"];
  rawData = await models.SpecialistDetails.find({attributes: fields, where: {id: req.params.id }});
  res.json({list: rawData});
}));


specialistsRoute.put('/:email', asyncHandler(async (req, res, next) => {
  fields = ["id", "firstName", "lastName", "phone", "website",
            "city", "languages", "jobTitle", "institution", "flexible","bio", 
            "mainSpecialty", "secondSpecialty", "profilePicURL", "completeProfile"];
  currentUser = await models.SpecialistDetails.find({ where: {email: req.params.email}});
  await currentUser.update(req.body, {fields: fields});
  res.json({msg: "Profile successfully updated"});
}));


specialistsRoute.delete('/:email', asyncHandler(async (req, res, next) => {
  proDetailsRow = await models.SpecialistDetails.find({ where: {email: req.params.email }});
  userRow = await models.User.find({ where: {email: req.params.email } });
  await proDetailsRow.destroy();
  await userRow.destroy();
  res.json({msg: 'successfully deleted'});
}));


// ==================
// Profile Pic Upload
// ==================

specialistsRoute.post('/img/:userId', upload.any(), asyncHandler(async (req,res, next) => {
  if (req.files[0].mimetype && req.files[0].buffer) {
    var bucketName = config.s3BucketName +  "/" + config.s3ProfilePicturesFolder;
    var pictureUrlBase = "https://s3." + config.s3Region + ".amazonaws.com/" + bucketName + "/";
    var pictureName = "picture_" + req.params.userId + "_" + Date.now() + "." + req.files[0].mimetype.split("/")[1];
    var picture = { Key: pictureName, Body: req.files[0].buffer }; 
    if (req.files[0].mimetype.split("/")[0] !== 'image') {
      res.json({type: "error", msg: 'Wrong Format. Please upload an image'})
    } else if (req.files[0].size > 6000000) {
      res.json({type: "error", msg: 'Image must be less than 6MB'})
    } else {
      AWS.config.credentials = {accessKeyId: config.s3KeyId, secretAccessKey: config.s3KeySecret};
      AWS.config.region = config.s3Region;
      var s3Bucket = new AWS.S3({ params: {Bucket: bucketName }});
      s3Bucket.putObject(picture, async(err, data) => {
        if (err) { 
          res.json({type: "error", msg: 'Internal error. Please try again [bucket]'});
        } else {
          await models.SpecialistDetails.update({profilePicURL: pictureUrlBase+pictureName}, {where: {id: req.params.userId}});
          res.json({type: "success", msg: 'Successfully uploaded', url: pictureUrlBase+pictureName})
        }
      })
    }
    } else {
      res.json({type: "error", msg: 'Internal error. Please try again [no idea]'});
    }
}))

// ===========
// Middlewares
// ===========

module.exports = specialistsRoute;