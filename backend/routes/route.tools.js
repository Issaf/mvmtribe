const express = require('express');
const asyncHandler = require('express-async-handler');
const models = require('../models');
const config = require('../config/global.conf');
const AWS = require('aws-sdk');
const multer = require('multer');
var storage = multer.memoryStorage();
const upload = multer({ storage: storage });
const toolsRoute = express.Router();

// ==========
// Tools CRUD
// ==========

toolsRoute.get('', asyncHandler(async (req, res, next) => {
  rawData = await models.Tool.findAll();
  res.json({list: rawData});
}));


toolsRoute.post('', asyncHandler(async (req, res, next) => {
  await models.Tool.create(req.body);
  res.json({msg: 'Successfully added'});
}));


toolsRoute.put('', asyncHandler(async (req, res, next) => {
  category = await models.Tool.find({where: {id: req.body.id }});
  await category.update(req.body.dict);
  res.json({msg: 'Successfully updated'});
}));


toolsRoute.delete('/:id', asyncHandler(async (req, res, next) => {
  category = await models.Tool.find({where: {id: req.params.id }});
  await category.destroy();
  res.json({msg: 'Successfully deleted'});
}));


// ==================
// Tool Pic Upload
// ==================

toolsRoute.post('/img/:name', upload.any(), asyncHandler(async (req,res, next) => {
  if (req.files[0].mimetype && req.files[0].buffer) {
    var bucketName = config.s3BucketName +  "/" + config.s3ProductPicturesFolder;
    var pictureUrlBase = "https://s3." + config.s3Region + ".amazonaws.com/" + bucketName + "/";
    var pictureName = "product_picture_" + Date.now() + "." + req.files[0].mimetype.split("/")[1];
    var picture = { Key: pictureName, Body: req.files[0].buffer }; 
    if (req.files[0].mimetype.split("/")[0] !== 'image') {
      res.json({type: "error", msg: 'Wrong Format. Please upload an image'})
    } else if (req.files[0].size > 15000000) {
      res.json({type: "error", msg: 'Image must be less than 6MB'})
    } else {
      AWS.config.credentials = {accessKeyId: config.s3KeyId, secretAccessKey: config.s3KeySecret};
      AWS.config.region = config.s3Region;
      var s3Bucket = new AWS.S3({ params: {Bucket: bucketName }});
      s3Bucket.putObject(picture, async(err, data) => {
        if (err) { 
          res.json({type: "error", msg: 'Internal error. Please try again [bucket]'});
        } else {
          res.json({type: "success", msg: 'Successfully uploaded', url: pictureUrlBase+pictureName})
        }
      })
    }
    } else {
      res.json({type: "error", msg: 'Internal error. Please try again [no idea]'});
    }
}))



// ===========
// Middlewares
// ===========

module.exports = toolsRoute;