'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      firstName: Sequelize.STRING,
      hash: Sequelize.STRING,
      salt: Sequelize.STRING,
      verified: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      lastLogin: Sequelize.DATE,
      provider: {
        type: Sequelize.ENUM('local', 'facebook', 'google'), 
        allowNull:false, 
        defaultValue:'local'
      },
      userType: {
        type: Sequelize.STRING, 
        allowNull:false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: Sequelize.DATE,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};