'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SpecialistDetails', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique:true,
      },
      phone: {
        type: Sequelize.STRING
      },
      website: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      languages: {
        type: Sequelize.ARRAY(Sequelize.TEXT)
      },
      jobTitle: {
        type: Sequelize.STRING
      },
      mainSpecialty: {
        type: Sequelize.STRING,
        allowNull: true,
        unique:false,
      },
      secondSpecialty: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: false,
      },
      institution: {
        type: Sequelize.STRING
      },
      flexible: {
        type: Sequelize.BOOLEAN
      },
      bio: {
        type: Sequelize.TEXT
      },
      profilePicURL: {
        type: Sequelize.STRING
      },
      completeProfile: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: Sequelize.DATE,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SpecialistDetails');
  }
};