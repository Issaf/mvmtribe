var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var config = require('./config/global.conf');
var passport = require('passport');
var router = require('./routes');
require('./models');
require('./auth/auth.local');
require('./auth/auth.facebook');
// require('./auth/auth.google');


var app = express();
app.use(passport.initialize()); 


global.backendDir = __dirname;


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(global.backendDir, './../frontend/dist/frontend')));

// APIs
app.use('/api', router);
app.get('*', (req, res) => {
  res.sendFile(path.join(global.backendDir, './../frontend/dist/frontend/index.html'));
});


// error handler
app.use(function(err, req, res, next) {
  if (err && err.status >= 400) {
    res.status(404).json({message: "Oops! Something went wrong"});
  }
})


module.exports = app;
