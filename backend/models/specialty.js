'use strict';
module.exports = (sequelize, DataTypes) => {
  var Specialty = sequelize.define('Specialty', {
    name: DataTypes.STRING
  },
  {
    paranoid: true
  });

  return Specialty;
};