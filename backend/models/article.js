'use strict';
module.exports = (sequelize, DataTypes) => {
  const Article = sequelize.define('Article', {
    title: {
      type: DataTypes.STRING
    },
    category: {
      type: DataTypes.STRING
    },
    bgImgUrl: {
      type: DataTypes.STRING
    },
    contentFileUrl: {
      type: DataTypes.STRING
    },
    readTime: {
      type: DataTypes.INTEGER
    },
    difficultyLevel: {
      type: DataTypes.ENUM('beginner', 'intermediate', 'advanced')
    },
    keywords: {
      type: DataTypes.ARRAY(DataTypes.TEXT)
    },
  },   {
    paranoid: true
  });
  
  Article.associate = function(models) {
    // associations can be defined here
  };
  return Article;
};