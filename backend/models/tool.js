'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tool = sequelize.define('Tool', {
    name: {
      type: DataTypes.STRING
    },
    price: {
      type: DataTypes.FLOAT
    },
    pictureURL: {
      type: DataTypes.STRING
    },
    amazonURL: {
      type: DataTypes.STRING
    },
    isOwnBrand: {
      type: DataTypes.BOOLEAN
    },
    isNew: {
      type: DataTypes.BOOLEAN
    },
    isOnSale: {
      type: DataTypes.BOOLEAN
    },
  },
  {
    paranoid: true
  });
  Tool.associate = function(models) {
    // associations can be defined here
  };
  return Tool;
};