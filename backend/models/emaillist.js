'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmailList = sequelize.define('EmailList', {
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    articleId: DataTypes.INTEGER
  }, 
  {
    paranoid: true
  });

  EmailList.associate = function(models) {
    // associations can be defined here
  };
  return EmailList;
};