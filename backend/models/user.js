'use strict';
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    email: DataTypes.STRING,
    firstName: DataTypes.STRING,
    hash: DataTypes.STRING,
    salt: DataTypes.STRING,
    verified: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    lastLogin: DataTypes.DATE,
    provider: {
      type: DataTypes.ENUM('local', 'facebook', 'google'), 
      allowNull:false, 
      defaultValue:'local'
    },
    userType: {
      type: DataTypes.STRING, 
      allowNull:false,
    },
  },
  {
    paranoid: true
  });

  User.updateLastLogin = (id,models) => {
    const lastLogin = new Date();
    models.User.update(
      {
        lastLogin: lastLogin
      },
      {
        where: {
          id: id
        }
      }
    );
  }

  
  User.prototype.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
  };

  User.prototype.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
  };

  User.prototype.generateJwt = function(t) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + t);
  
    return jwt.sign({
      email: this.email,
      firstName: this.firstName
    }, config.jwtSecret);
  };

  User.prototype.shortJwt = function(t) {
    var expiry = new Date();
    expiry.setMinutes(expiry.getMinutes() + t);
  
    return jwt.sign({
      email: this.email,
      firstName: this.firstName,
      exp: expiry.valueOf()
    }, config.jwtSecret);
  };

  return User;
};