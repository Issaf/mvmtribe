'use strict';
module.exports = (sequelize, DataTypes) => {
  const LearningCategory = sequelize.define('LearningCategory', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT
  },
  {
    paranoid: true
  });
  LearningCategory.associate = function(models) {
    // associations can be defined here
  };
  return LearningCategory;
};