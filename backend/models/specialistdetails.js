'use strict';
module.exports = (sequelize, DataTypes) => {
  const SpecialistDetails = sequelize.define('SpecialistDetails', {
    firstName: {
      type: DataTypes.STRING
    },
    lastName: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique:true,
    },
    phone: {
      type: DataTypes.STRING
    },
    website: {
      type: DataTypes.STRING
    },
    city: {
      type: DataTypes.STRING
    },
    languages: {
      type: DataTypes.ARRAY(DataTypes.TEXT)
    },
    jobTitle: {
      type: DataTypes.STRING
    },
    mainSpecialty: {
      type: DataTypes.STRING,
      allowNull: true,
      unique:false,
    },
    secondSpecialty: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false,
    },
    institution: {
      type: DataTypes.STRING
    },
    flexible: {
      type: DataTypes.BOOLEAN
    },
    bio: {
      type: DataTypes.TEXT
    },
    profilePicURL: {
      type: DataTypes.STRING
    },
    completeProfile: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  },
  {
    paranoid: true
  });
  
  return SpecialistDetails;
};