(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-core-homepage-homepage-module"],{

/***/ "./node_modules/angular-particle/index.js":
/*!************************************************!*\
  !*** ./node_modules/angular-particle/index.js ***!
  \************************************************/
/*! exports provided: ParticlesModule, ParticlesComponent, ParticlesDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParticlesModule", function() { return ParticlesModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParticlesComponent", function() { return ParticlesComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParticlesDirective", function() { return ParticlesDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");



var ParticlesComponent = (function () {
    function ParticlesComponent() {
        this.width = 100;
        this.height = 100;
        this.style = {};
    }
    return ParticlesComponent;
}());
ParticlesComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: 'particles',
                template: "\n        <div [ngStyle]=\"style\" class=\"particles-container\">\n            <canvas d-particles [params]=\"params\" [style.width.%]=\"width\" [style.height.%]=\"height\"></canvas>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
ParticlesComponent.ctorParameters = function () { return []; };
ParticlesComponent.propDecorators = {
    'width': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'height': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'params': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'style': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};

var hexToRgb = function (hex) {
    var /** @type {?} */ shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
    });
    var /** @type {?} */ result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
};
var clamp = function (number, min, max) {
    return Math.min(Math.max(number, min), max);
};
var isInArray = function (value, array) {
    return array.indexOf(value) > -1;
};
var deepExtend = function (destination, source) {
    for (var /** @type {?} */ property in source) {
        if (source[property] &&
            source[property].constructor &&
            source[property].constructor === Object) {
            destination[property] = destination[property] || {};
            deepExtend(destination[property], source[property]);
        }
        else {
            destination[property] = source[property];
        }
    }
    return destination;
};
var getColor = function (colorObject) {
    var /** @type {?} */ color = {};
    if (typeof (colorObject) == 'object') {
        if (colorObject instanceof Array) {
            var /** @type {?} */ selectedColor = colorObject[Math.floor(Math.random() * colorObject.length)];
            color.rgb = hexToRgb(selectedColor);
        }
        else {
            var r = colorObject.r, g = colorObject.g, b = colorObject.b;
            if (r !== undefined && g !== undefined && b !== undefined) {
                color.rgb = { r: r, g: g, b: b };
            }
            else {
                var h = colorObject.h, s = colorObject.s, l = colorObject.l;
                if (h !== undefined && g !== undefined && b !== undefined) {
                    color.hsl = { h: h, s: s, l: l };
                }
            }
        }
    }
    else if (colorObject == 'random') {
        color.rgb = {
            r: (Math.floor(Math.random() * 255) + 1),
            g: (Math.floor(Math.random() * 255) + 1),
            b: (Math.floor(Math.random() * 255) + 1)
        };
    }
    else if (typeof (colorObject) == 'string') {
        color.rgb = hexToRgb(colorObject);
    }
    return color;
};
var getDefaultParams = function () {
    return {
        particles: {
            number: {
                value: 100,
                density: {
                    enable: true,
                    value_area: 800
                }
            },
            color: {
                value: '#FFF'
            },
            shape: {
                type: 'circle',
                stroke: {
                    width: 0,
                    color: '#000000'
                },
                polygon: {
                    nb_sides: 5
                },
                image: {
                    src: '',
                    width: 100,
                    height: 100
                }
            },
            opacity: {
                value: 0.5,
                random: false,
                anim: {
                    enable: true,
                    speed: 1,
                    opacity_min: 0.1,
                    sync: false
                }
            },
            size: {
                value: 3,
                random: true,
                anim: {
                    enable: false,
                    speed: 40,
                    size_min: 0,
                    sync: false
                }
            },
            line_linked: {
                enable: true,
                distance: 150,
                color: '#FFF',
                opacity: 0.6,
                width: 1,
                shadow: {
                    enable: false,
                    blur: 5,
                    color: 'lime'
                }
            },
            move: {
                enable: true,
                speed: 3,
                direction: 'none',
                random: false,
                straight: false,
                out_mode: 'out',
                bounce: true,
                attract: {
                    enable: false,
                    rotateX: 3000,
                    rotateY: 3000
                }
            },
            array: []
        },
        interactivity: {
            detect_on: 'canvas',
            events: {
                onhover: {
                    enable: true,
                    mode: 'grab'
                },
                onclick: {
                    enable: true,
                    mode: 'push'
                },
                resize: true
            },
            modes: {
                grab: {
                    distance: 200,
                    line_linked: {
                        opacity: 1
                    }
                },
                bubble: {
                    distance: 200,
                    size: 80,
                    duration: 0.4
                },
                repulse: {
                    distance: 200,
                    duration: 0.4
                },
                push: {
                    particles_nb: 4
                },
                remove: {
                    particles_nb: 2
                }
            },
            mouse: {}
        },
        retina_detect: true
    };
};
/**
 * @param {?} params
 * @param {?} tmp
 * @return {?}
 */
function loadImg(params, tmp) {
    var particles = this.params.particles;
    tmp.img_error = undefined;
    if (particles.shape.type == 'image' && particles.shape.image.src != '') {
        if (tmp.img_type == 'svg') {
            var /** @type {?} */ xhr_1 = new XMLHttpRequest();
            xhr_1.open('GET', particles.shape.image.src);
            xhr_1.onreadystatechange = function (data) {
                if (xhr_1.readyState == 4) {
                    if (xhr_1.status == 200) {
                        tmp.source_svg = data.currentTarget.response;
                        if (tmp.source_svg == undefined) {
                            var /** @type {?} */ check = void 0;
                            tmp.checkAnimFrame = requestAnimationFrame(check);
                        }
                    }
                    else {
                        tmp.img_error = true;
                        throw "Error : image not found";
                    }
                }
            };
            xhr_1.send();
        }
        else {
            var /** @type {?} */ img_1 = new Image();
            img_1.addEventListener('load', function () {
                tmp.img_obj = img_1;
                cancelAnimationFrame(tmp.checkAnimFrame);
            });
            img_1.src = particles.shape.image.src;
        }
    }
    else {
        tmp.img_error = true;
        throw "Error : no image.src";
    }
}
/**
 * @param {?} particle
 * @param {?} tmp
 * @return {?}
 */
function createSvgImg(particle, tmp) {
    var /** @type {?} */ svgXml = tmp.source_svg;
    var /** @type {?} */ rgbHex = /#([0-9A-F]{3,6})/gi;
    var /** @type {?} */ coloredSvgXml = svgXml.replace(rgbHex, function (m, r, g, b) {
        var /** @type {?} */ color_value;
        if (particle.color.rgb) {
            var _a = particle.color.rgb, r_1 = _a.r, g_1 = _a.g, b_1 = _a.b;
            color_value = "rgba( " + r_1 + ", " + g_1 + ", " + b_1 + ", " + particle.opacity + " )";
        }
        else {
            var _b = particle.color.hsl, h = _b.h, s = _b.s, l = _b.l;
            color_value = "rgba( " + h + ", " + s + ", " + l + ", " + particle.opacity + " )";
        }
        return color_value;
    });
    var /** @type {?} */ svg = new Blob([coloredSvgXml], {
        type: 'image/svg+xml;charset=utf-8'
    });
    var /** @type {?} */ DOMURL = window.URL || window;
    var /** @type {?} */ url = DOMURL.createObjectURL(svg);
    var /** @type {?} */ img = new Image();
    img.addEventListener('load', function () {
        particle.img.obj = img;
        particle.img.loaded = true;
        DOMURL.revokeObjectURL(url);
        tmp.count_svg++;
    });
    img.src = url;
}

var CanvasManager$$1 = (function () {
    /**
     * @param {?} _canvasParams
     * @param {?} _params
     * @param {?} _tmpParams
     */
    function CanvasManager$$1(_canvasParams, _params, _tmpParams) {
        this._canvasParams = _canvasParams;
        this._params = _params;
        this._tmpParams = _tmpParams;
        this._onWindowResize = this._onWindowResize.bind(this);
        this._retinaInit();
        this._canvasSize();
        this.particlesManager = new ParticlesManager$$1(this._canvasParams, this._params, this._tmpParams);
        this.particlesManager.particlesCreate();
        this._densityAutoParticles();
        var particles = this._params.particles;
        particles.line_linked.color_rgb_line = hexToRgb(particles.line_linked.color);
    }
    /**
     * @return {?}
     */
    CanvasManager$$1.prototype.draw = function () {
        var particles = this._params.particles;
        if (particles.shape.type == 'image') {
            if (this._tmpParams.img_type == 'svg') {
                if (this._tmpParams.count_svg >= particles.number.value) {
                    this.particlesManager.particlesDraw();
                    if (!particles.move.enable) {
                        cancelAnimationFrame(this._tmpParams.drawAnimFrame);
                    }
                    else {
                        this._tmpParams.drawAnimFrame = requestAnimationFrame(this.draw.bind(this));
                    }
                }
                else {
                    if (!this._tmpParams.img_error) {
                        this._tmpParams.drawAnimFrame = requestAnimationFrame(this.draw.bind(this));
                    }
                }
            }
            else {
                if (this._tmpParams.img_obj != undefined) {
                    this.particlesManager.particlesDraw();
                    if (!particles.move.enable) {
                        cancelAnimationFrame(this._tmpParams.drawAnimFrame);
                    }
                    else {
                        this._tmpParams.drawAnimFrame = requestAnimationFrame(this.draw.bind(this));
                    }
                }
                else {
                    if (!this._tmpParams.img_error) {
                        this._tmpParams.drawAnimFrame = requestAnimationFrame(this.draw.bind(this));
                    }
                }
            }
        }
        else {
            this.particlesManager.particlesDraw();
            if (!particles.move.enable) {
                cancelAnimationFrame(this._tmpParams.drawAnimFrame);
            }
            else {
                this._tmpParams.drawAnimFrame = requestAnimationFrame(this.draw.bind(this));
            }
        }
    };
    /**
     * @return {?}
     */
    CanvasManager$$1.prototype._densityAutoParticles = function () {
        var particles = this._params.particles;
        if (particles.number.density.enable) {
            var /** @type {?} */ area = this._canvasParams.el.width * this._canvasParams.el.height / 1000;
            if (this._tmpParams.retina) {
                area = area / (this._canvasParams.pxratio * 2);
            }
            var /** @type {?} */ nb_particles = area * particles.number.value / particles.number.density.value_area;
            var /** @type {?} */ missing_particles = particles.array.length - nb_particles;
            if (missing_particles < 0) {
                this.particlesManager.pushParticles(Math.abs(missing_particles));
            }
            else {
                this.particlesManager.removeParticles(missing_particles);
            }
        }
    };
    /**
     * @return {?}
     */
    CanvasManager$$1.prototype._retinaInit = function () {
        if (this._params.retina_detect && window.devicePixelRatio > 1) {
            this._canvasParams.pxratio = window.devicePixelRatio;
            this._tmpParams.retina = true;
            this._canvasParams.width = this._canvasParams.el.offsetWidth * this._canvasParams.pxratio;
            this._canvasParams.height = this._canvasParams.el.offsetHeight * this._canvasParams.pxratio;
            this._params.particles.size.value = this._tmpParams.obj.size_value * this._canvasParams.pxratio;
            this._params.particles.size.anim.speed = this._tmpParams.obj.size_anim_speed * this._canvasParams.pxratio;
            this._params.particles.move.speed = this._tmpParams.obj.move_speed * this._canvasParams.pxratio;
            this._params.particles.line_linked.distance = this._tmpParams.obj.line_linked_distance * this._canvasParams.pxratio;
            this._params.interactivity.modes.grab.distance = this._tmpParams.obj.mode_grab_distance * this._canvasParams.pxratio;
            this._params.interactivity.modes.bubble.distance = this._tmpParams.obj.mode_bubble_distance * this._canvasParams.pxratio;
            this._params.particles.line_linked.width = this._tmpParams.obj.line_linked_width * this._canvasParams.pxratio;
            this._params.interactivity.modes.bubble.size = this._tmpParams.obj.mode_bubble_size * this._canvasParams.pxratio;
            this._params.interactivity.modes.repulse.distance = this._tmpParams.obj.mode_repulse_distance * this._canvasParams.pxratio;
        }
        else {
            this._canvasParams.pxratio = 1;
            this._tmpParams.retina = false;
        }
    };
    /**
     * @return {?}
     */
    CanvasManager$$1.prototype._canvasClear = function () {
        this._canvasParams.ctx.clearRect(0, 0, this._canvasParams.width, this._canvasParams.height);
    };
    /**
     * @return {?}
     */
    CanvasManager$$1.prototype._canvasPaint = function () {
        this._canvasParams.ctx.fillRect(0, 0, this._canvasParams.width, this._canvasParams.height);
    };
    /**
     * @return {?}
     */
    CanvasManager$$1.prototype._canvasSize = function () {
        this._canvasParams.el.width = this._canvasParams.width;
        this._canvasParams.el.height = this._canvasParams.height;
        if (this._params && this._params.interactivity.events.resize) {
            window.addEventListener('resize', this._onWindowResize);
        }
    };
    /**
     * @return {?}
     */
    CanvasManager$$1.prototype._onWindowResize = function () {
        this._canvasParams.width = this._canvasParams.el.offsetWidth;
        this._canvasParams.height = this._canvasParams.el.offsetHeight;
        if (this._tmpParams.retina) {
            this._canvasParams.width *= this._canvasParams.pxratio;
            this._canvasParams.height *= this._canvasParams.pxratio;
        }
        this._canvasParams.el.width = this._canvasParams.width;
        this._canvasParams.el.height = this._canvasParams.height;
        if (!this._params.particles.move.enable) {
            this.particlesManager.particlesEmpty();
            this.particlesManager.particlesCreate();
            this.particlesManager.particlesDraw();
            this._densityAutoParticles();
        }
        this._densityAutoParticles();
    };
    return CanvasManager$$1;
}());

var ParticlesManager$$1 = (function () {
    /**
     * @param {?} _canvasParams
     * @param {?} _params
     * @param {?} _tmpParams
     */
    function ParticlesManager$$1(_canvasParams, _params, _tmpParams) {
        this._canvasParams = _canvasParams;
        this._params = _params;
        this._tmpParams = _tmpParams;
        this._interaction = new ParticleInteraction();
    }
    /**
     * @return {?}
     */
    ParticlesManager$$1.prototype.particlesCreate = function () {
        var _a = this._params.particles, color = _a.color, opacity = _a.opacity;
        for (var /** @type {?} */ i = 0; i < this._params.particles.number.value; i++) {
            this._params.particles.array.push(new Particle$$1(this._canvasParams, this._params, this._tmpParams, color, opacity.value));
        }
    };
    /**
     * @return {?}
     */
    ParticlesManager$$1.prototype._particlesUpdate = function () {
        var _this = this;
        this._params.particles.array.forEach(function (particle, i) {
            if (_this._params.particles.move.enable) {
                var /** @type {?} */ ms = _this._params.particles.move.speed / 2;
                particle.x += particle.vx * ms;
                particle.y += particle.vy * ms;
            }
            if (_this._params.particles.opacity.anim.enable) {
                if (particle.opacity_status == true) {
                    if (particle.opacity >= _this._params.particles.opacity.value)
                        particle.opacity_status = false;
                    particle.opacity += particle.vo;
                }
                else {
                    if (particle.opacity <= _this._params.particles.opacity.anim.opacity_min)
                        particle.opacity_status = true;
                    particle.opacity -= particle.vo;
                }
                if (particle.opacity < 0)
                    particle.opacity = 0;
            }
            if (_this._params.particles.size.anim.enable) {
                if (particle.size_status == true) {
                    if (particle.radius >= _this._params.particles.size.value)
                        particle.size_status = false;
                    particle.radius += particle.vs;
                }
                else {
                    if (particle.radius <= _this._params.particles.size.anim.size_min)
                        particle.size_status = true;
                    particle.radius -= particle.vs;
                }
                if (particle.radius < 0)
                    particle.radius = 0;
            }
            var /** @type {?} */ new_pos;
            if (_this._params.particles.move.out_mode == 'bounce') {
                new_pos = {
                    x_left: particle.radius,
                    x_right: _this._canvasParams.width,
                    y_top: particle.radius,
                    y_bottom: _this._canvasParams.height
                };
            }
            else {
                new_pos = {
                    x_left: -particle.radius,
                    x_right: _this._canvasParams.width + particle.radius,
                    y_top: -particle.radius,
                    y_bottom: _this._canvasParams.height + particle.radius
                };
            }
            if (particle.x - particle.radius > _this._canvasParams.width) {
                particle.x = new_pos.x_left;
                particle.y = Math.random() * _this._canvasParams.height;
            }
            else if (particle.x + particle.radius < 0) {
                particle.x = new_pos.x_right;
                particle.y = Math.random() * _this._canvasParams.height;
            }
            if (particle.y - particle.radius > _this._canvasParams.height) {
                particle.y = new_pos.y_top;
                particle.x = Math.random() * _this._canvasParams.width;
            }
            else if (particle.y + particle.radius < 0) {
                particle.y = new_pos.y_bottom;
                particle.x = Math.random() * _this._canvasParams.width;
            }
            switch (_this._params.particles.move.out_mode) {
                case 'bounce':
                    if (particle.x + particle.radius > _this._canvasParams.width)
                        particle.vx = -particle.vx;
                    else if (particle.x - particle.radius < 0)
                        particle.vx = -particle.vx;
                    if (particle.y + particle.radius > _this._canvasParams.height)
                        particle.vy = -particle.vy;
                    else if (particle.y - particle.radius < 0)
                        particle.vy = -particle.vy;
                    break;
            }
            if (isInArray('grab', _this._params.interactivity.events.onhover.mode)) {
                _this._grabParticle(particle);
            }
            if (isInArray('bubble', _this._params.interactivity.events.onhover.mode) ||
                isInArray('bubble', _this._params.interactivity.events.onclick.mode)) {
                _this._bubbleParticle(particle);
            }
            if (isInArray('repulse', _this._params.interactivity.events.onhover.mode) ||
                isInArray('repulse', _this._params.interactivity.events.onclick.mode)) {
                _this._repulseParticle(particle);
            }
            if (_this._params.particles.line_linked.enable || _this._params.particles.move.attract.enable) {
                for (var /** @type {?} */ j = i + 1; j < _this._params.particles.array.length; j++) {
                    var /** @type {?} */ link = _this._params.particles.array[j];
                    if (_this._params.particles.line_linked.enable)
                        _this._interaction.linkParticles(particle, link, _this._params, _this._canvasParams);
                    if (_this._params.particles.move.attract.enable)
                        _this._interaction.attractParticles(particle, link, _this._params);
                    if (_this._params.particles.move.bounce)
                        _this._interaction.bounceParticles(particle, link);
                }
            }
        });
    };
    /**
     * @return {?}
     */
    ParticlesManager$$1.prototype.particlesDraw = function () {
        this._canvasParams.ctx.clearRect(0, 0, this._canvasParams.width, this._canvasParams.height);
        this._particlesUpdate();
        this._params.particles.array.forEach(function (particle) {
            particle.draw();
        });
    };
    /**
     * @return {?}
     */
    ParticlesManager$$1.prototype.particlesEmpty = function () {
        this._params.particles.array = [];
    };
    /**
     * @param {?} nb
     * @return {?}
     */
    ParticlesManager$$1.prototype.removeParticles = function (nb) {
        this._params.particles.array.splice(0, nb);
        if (!this._params.particles.move.enable) {
            this.particlesDraw();
        }
    };
    /**
     * @param {?} nb
     * @param {?=} pos
     * @return {?}
     */
    ParticlesManager$$1.prototype.pushParticles = function (nb, pos) {
        this._tmpParams.pushing = true;
        for (var /** @type {?} */ i = 0; i < nb; i++) {
            this._params.particles.array.push(new Particle$$1(this._canvasParams, this._params, this._tmpParams, this._params.particles.color, this._params.particles.opacity.value, {
                x: pos ? pos.pos_x : Math.random() * this._canvasParams.width,
                y: pos ? pos.pos_y : Math.random() * this._canvasParams.height
            }));
            if (i == nb - 1) {
                if (!this._params.particles.move.enable) {
                    this.particlesDraw();
                }
                this._tmpParams.pushing = false;
            }
        }
    };
    /**
     * @param {?} particle
     * @return {?}
     */
    ParticlesManager$$1.prototype._bubbleParticle = function (particle) {
        var _this = this;
        if (this._params.interactivity.events.onhover.enable &&
            isInArray('bubble', this._params.interactivity.events.onhover.mode)) {
            var /** @type {?} */ dx_mouse = particle.x - this._params.interactivity.mouse.pos_x;
            var /** @type {?} */ dy_mouse = particle.y - this._params.interactivity.mouse.pos_y;
            var /** @type {?} */ dist_mouse = Math.sqrt(dx_mouse * dx_mouse + dy_mouse * dy_mouse);
            var /** @type {?} */ ratio = 1 - dist_mouse / this._params.interactivity.modes.bubble.distance;
            var /** @type {?} */ init = function () {
                particle.opacity_bubble = particle.opacity;
                particle.radius_bubble = particle.radius;
            };
            if (dist_mouse <= this._params.interactivity.modes.bubble.distance) {
                if (ratio >= 0 && this._params.interactivity.status == 'mousemove') {
                    if (this._params.interactivity.modes.bubble.size != this._params.particles.size.value) {
                        if (this._params.interactivity.modes.bubble.size > this._params.particles.size.value) {
                            var /** @type {?} */ size = particle.radius + (this._params.interactivity.modes.bubble.size * ratio);
                            if (size >= 0) {
                                particle.radius_bubble = size;
                            }
                        }
                        else {
                            var /** @type {?} */ dif = particle.radius - this._params.interactivity.modes.bubble.size;
                            var /** @type {?} */ size = particle.radius - (dif * ratio);
                            if (size > 0) {
                                particle.radius_bubble = size;
                            }
                            else {
                                particle.radius_bubble = 0;
                            }
                        }
                    }
                    if (this._params.interactivity.modes.bubble.opacity != this._params.particles.opacity.value) {
                        if (this._params.interactivity.modes.bubble.opacity > this._params.particles.opacity.value) {
                            var /** @type {?} */ opacity = this._params.interactivity.modes.bubble.opacity * ratio;
                            if (opacity > particle.opacity && opacity <= this._params.interactivity.modes.bubble.opacity) {
                                particle.opacity_bubble = opacity;
                            }
                        }
                        else {
                            var /** @type {?} */ opacity = particle.opacity - (this._params.particles.opacity.value - this._params.interactivity.modes.bubble.opacity) * ratio;
                            if (opacity < particle.opacity && opacity >= this._params.interactivity.modes.bubble.opacity) {
                                particle.opacity_bubble = opacity;
                            }
                        }
                    }
                }
            }
            else {
                init();
            }
            if (this._params.interactivity.status == 'mouseleave') {
                init();
            }
        }
        else if (this._params.interactivity.events.onclick.enable &&
            isInArray('bubble', this._params.interactivity.events.onclick.mode)) {
            if (this._tmpParams.bubble_clicking) {
                var /** @type {?} */ dx_mouse = particle.x - this._params.interactivity.mouse.click_pos_x;
                var /** @type {?} */ dy_mouse = particle.y - this._params.interactivity.mouse.click_pos_y;
                var /** @type {?} */ dist_mouse_1 = Math.sqrt(dx_mouse * dx_mouse + dy_mouse * dy_mouse);
                var /** @type {?} */ time_spent_1 = (new Date().getTime() - this._params.interactivity.mouse.click_time) / 1000;
                if (time_spent_1 > this._params.interactivity.modes.bubble.duration) {
                    this._tmpParams.bubble_duration_end = true;
                }
                if (time_spent_1 > this._params.interactivity.modes.bubble.duration * 2) {
                    this._tmpParams.bubble_clicking = false;
                    this._tmpParams.bubble_duration_end = false;
                }
                var /** @type {?} */ process = function (bubble_param, particles_param, p_obj_bubble, p_obj, id) {
                    if (bubble_param != particles_param) {
                        if (!_this._tmpParams.bubble_duration_end) {
                            if (dist_mouse_1 <= _this._params.interactivity.modes.bubble.distance) {
                                var /** @type {?} */ obj = void 0;
                                if (p_obj_bubble != undefined) {
                                    obj = p_obj_bubble;
                                }
                                else {
                                    obj = p_obj;
                                }
                                if (obj != bubble_param) {
                                    var /** @type {?} */ value = p_obj - (time_spent_1 * (p_obj - bubble_param) / _this._params.interactivity.modes.bubble.duration);
                                    if (id == 'size')
                                        particle.radius_bubble = value;
                                    if (id == 'opacity')
                                        particle.opacity_bubble = value;
                                }
                            }
                            else {
                                if (id == 'size')
                                    particle.radius_bubble = undefined;
                                if (id == 'opacity')
                                    particle.opacity_bubble = undefined;
                            }
                        }
                        else {
                            if (p_obj_bubble != undefined) {
                                var /** @type {?} */ value_tmp = p_obj - (time_spent_1 * (p_obj - bubble_param) / _this._params.interactivity.modes.bubble.duration);
                                var /** @type {?} */ dif = bubble_param - value_tmp;
                                var /** @type {?} */ value = bubble_param + dif;
                                if (id == 'size')
                                    particle.radius_bubble = value;
                                if (id == 'opacity')
                                    particle.opacity_bubble = value;
                            }
                        }
                    }
                };
                if (this._tmpParams.bubble_clicking) {
                    process(this._params.interactivity.modes.bubble.size, this._params.particles.size.value, particle.radius_bubble, particle.radius, 'size');
                    process(this._params.interactivity.modes.bubble.opacity, this._params.particles.opacity.value, particle.opacity_bubble, particle.opacity, 'opacity');
                }
            }
        }
    };
    /**
     * @param {?} particle
     * @return {?}
     */
    ParticlesManager$$1.prototype._repulseParticle = function (particle) {
        var _this = this;
        if (this._params.interactivity.events.onhover.enable &&
            isInArray('repulse', this._params.interactivity.events.onhover.mode) &&
            this._params.interactivity.status == 'mousemove') {
            var /** @type {?} */ dx_mouse = particle.x - this._params.interactivity.mouse.pos_x;
            var /** @type {?} */ dy_mouse = particle.y - this._params.interactivity.mouse.pos_y;
            var /** @type {?} */ dist_mouse = Math.sqrt(dx_mouse * dx_mouse + dy_mouse * dy_mouse);
            var /** @type {?} */ normVec = { x: dx_mouse / dist_mouse, y: dy_mouse / dist_mouse };
            var /** @type {?} */ repulseRadius = this._params.interactivity.modes.repulse.distance;
            var /** @type {?} */ velocity = 100;
            var /** @type {?} */ repulseFactor = clamp((1 / repulseRadius) * (-1 * Math.pow(dist_mouse / repulseRadius, 2) + 1) * repulseRadius * velocity, 0, 50);
            var /** @type {?} */ pos = {
                x: particle.x + normVec.x * repulseFactor,
                y: particle.y + normVec.y * repulseFactor
            };
            if (this._params.particles.move.out_mode == 'bounce') {
                if (pos.x - particle.radius > 0 && pos.x + particle.radius < this._canvasParams.width)
                    particle.x = pos.x;
                if (pos.y - particle.radius > 0 && pos.y + particle.radius < this._canvasParams.height)
                    particle.y = pos.y;
            }
            else {
                particle.x = pos.x;
                particle.y = pos.y;
            }
        }
        else if (this._params.interactivity.events.onclick.enable &&
            isInArray('repulse', this._params.interactivity.events.onclick.mode)) {
            if (!this._tmpParams.repulse_finish) {
                this._tmpParams.repulse_count++;
                if (this._tmpParams.repulse_count == this._params.particles.array.length)
                    this._tmpParams.repulse_finish = true;
            }
            if (this._tmpParams.repulse_clicking) {
                var /** @type {?} */ repulseRadius = Math.pow(this._params.interactivity.modes.repulse.distance / 6, 3);
                var /** @type {?} */ dx_1 = this._params.interactivity.mouse.click_pos_x - particle.x;
                var /** @type {?} */ dy_1 = this._params.interactivity.mouse.click_pos_y - particle.y;
                var /** @type {?} */ d = dx_1 * dx_1 + dy_1 * dy_1;
                var /** @type {?} */ force_1 = -repulseRadius / d * 1;
                var /** @type {?} */ process = function () {
                    var /** @type {?} */ f = Math.atan2(dy_1, dx_1);
                    particle.vx = force_1 * Math.cos(f);
                    particle.vy = force_1 * Math.sin(f);
                    if (_this._params.particles.move.out_mode == 'bounce') {
                        var /** @type {?} */ pos = {
                            x: particle.x + particle.vx,
                            y: particle.y + particle.vy
                        };
                        if (pos.x + particle.radius > _this._canvasParams.width)
                            particle.vx = -particle.vx;
                        else if (pos.x - particle.radius < 0)
                            particle.vx = -particle.vx;
                        if (pos.y + particle.radius > _this._canvasParams.height)
                            particle.vy = -particle.vy;
                        else if (pos.y - particle.radius < 0)
                            particle.vy = -particle.vy;
                    }
                };
                if (d <= repulseRadius) {
                    process();
                }
            }
            else {
                if (this._tmpParams.repulse_clicking == false) {
                    particle.vx = particle.vx_i;
                    particle.vy = particle.vy_i;
                }
            }
        }
    };
    /**
     * @param {?} particle
     * @return {?}
     */
    ParticlesManager$$1.prototype._grabParticle = function (particle) {
        var _a = this._params, interactivity = _a.interactivity, particles = _a.particles;
        if (interactivity.events.onhover.enable &&
            interactivity.status == 'mousemove') {
            var /** @type {?} */ dx_mouse = particle.x - interactivity.mouse.pos_x;
            var /** @type {?} */ dy_mouse = particle.y - interactivity.mouse.pos_y;
            var /** @type {?} */ dist_mouse = Math.sqrt(dx_mouse * dx_mouse + dy_mouse * dy_mouse);
            if (dist_mouse <= interactivity.modes.grab.distance) {
                var grab = interactivity.modes.grab;
                var /** @type {?} */ opacity_line = grab.line_linked.opacity - (dist_mouse / (1 / grab.line_linked.opacity)) / grab.distance;
                if (opacity_line > 0) {
                    var /** @type {?} */ color_line = particles.line_linked.color_rgb_line;
                    var r = color_line.r, g = color_line.g, b = color_line.b;
                    this._canvasParams.ctx.strokeStyle = "rgba( " + r + ", " + g + ", " + b + ", " + opacity_line + " )";
                    this._canvasParams.ctx.lineWidth = particles.line_linked.width;
                    this._canvasParams.ctx.beginPath();
                    this._canvasParams.ctx.moveTo(particle.x, particle.y);
                    this._canvasParams.ctx.lineTo(interactivity.mouse.pos_x, interactivity.mouse.pos_y);
                    this._canvasParams.ctx.stroke();
                    this._canvasParams.ctx.closePath();
                }
            }
        }
    };
    return ParticlesManager$$1;
}());

var Particle$$1 = (function () {
    /**
     * @param {?} _canvasParams
     * @param {?} _params
     * @param {?} _tmpParams
     * @param {?=} color
     * @param {?=} opacity
     * @param {?=} position
     */
    function Particle$$1(_canvasParams, _params, _tmpParams, color, opacity, position) {
        this._canvasParams = _canvasParams;
        this._params = _params;
        this._tmpParams = _tmpParams;
        this._setupSize();
        this._setupPosition(position);
        this._setupColor(color);
        this._setupOpacity();
        this._setupAnimation();
    }
    /**
     * @return {?}
     */
    Particle$$1.prototype._setupSize = function () {
        this.radius = (this._params.particles.size.random ? Math.random() : 1) * this._params.particles.size.value;
        if (this._params.particles.size.anim.enable) {
            this.size_status = false;
            this.vs = this._params.particles.size.anim.speed / 100;
            if (!this._params.particles.size.anim.sync)
                this.vs = this.vs * Math.random();
        }
    };
    /**
     * @param {?=} position
     * @return {?}
     */
    Particle$$1.prototype._setupPosition = function (position) {
        this.x = position ? position.x : Math.random() * this._canvasParams.width;
        this.y = position ? position.y : Math.random() * this._canvasParams.height;
        if (this.x > this._canvasParams.width - this.radius * 2) {
            this.x = this.x - this.radius;
        }
        else if (this.x < this.radius * 2) {
            this.x = this.x + this.radius;
        }
        if (this.y > this._canvasParams.height - this.radius * 2) {
            this.y = this.y - this.radius;
        }
        else if (this.y < this.radius * 2) {
            this.y = this.y + this.radius;
        }
        if (this._params.particles.move.bounce) {
            this._checkOverlap(this, position);
        }
    };
    /**
     * @param {?} p1
     * @param {?=} position
     * @return {?}
     */
    Particle$$1.prototype._checkOverlap = function (p1, position) {
        var _this = this;
        var particles = this._params.particles;
        particles.array.forEach(function (particle) {
            var /** @type {?} */ p2 = particle;
            var /** @type {?} */ dx = p1.x - p2.x;
            var /** @type {?} */ dy = p1.y - p2.y;
            var /** @type {?} */ dist = Math.sqrt(dx * dx + dy * dy);
            if (dist <= p1.radius + p2.radius) {
                p1.x = position ? position.x : Math.random() * _this._canvasParams.width;
                p1.y = position ? position.y : Math.random() * _this._canvasParams.height;
                _this._checkOverlap(p1);
            }
        });
    };
    /**
     * @param {?=} color
     * @return {?}
     */
    Particle$$1.prototype._setupColor = function (color) {
        this.color = getColor(color.value);
    };
    /**
     * @return {?}
     */
    Particle$$1.prototype._setupOpacity = function () {
        this.opacity = (this._params.particles.opacity.random ? Math.random() : 1) * this._params.particles.opacity.value;
        if (this._params.particles.opacity.anim.enable) {
            this.opacity_status = false;
            this.vo = this._params.particles.opacity.anim.speed / 100;
            if (!this._params.particles.opacity.anim.sync) {
                this.vo = this.vo * Math.random();
            }
        }
    };
    /**
     * @return {?}
     */
    Particle$$1.prototype._setupAnimation = function () {
        var /** @type {?} */ velbase = null;
        switch (this._params.particles.move.direction) {
            case 'top':
                velbase = { x: 0, y: -1 };
                break;
            case 'top-right':
                velbase = { x: 0.5, y: -0.5 };
                break;
            case 'right':
                velbase = { x: 1, y: 0 };
                break;
            case 'bottom-right':
                velbase = { x: 0.5, y: 0.5 };
                break;
            case 'bottom':
                velbase = { x: 0, y: 1 };
                break;
            case 'bottom-left':
                velbase = { x: -0.5, y: 1 };
                break;
            case 'left':
                velbase = { x: -1, y: 0 };
                break;
            case 'top-left':
                velbase = { x: -0.5, y: -0.5 };
                break;
            default:
                velbase = { x: 0, y: 0 };
                break;
        }
        if (this._params.particles.move.straight) {
            this.vx = velbase.x;
            this.vy = velbase.y;
            if (this._params.particles.move.random) {
                this.vx = this.vx * (Math.random());
                this.vy = this.vy * (Math.random());
            }
        }
        else {
            this.vx = velbase.x + Math.random() - 0.5;
            this.vy = velbase.y + Math.random() - 0.5;
        }
        this.vx_i = this.vx;
        this.vy_i = this.vy;
        var /** @type {?} */ shape_type = this._params.particles.shape.type;
        if (typeof (shape_type) == 'object') {
            if (shape_type instanceof Array) {
                var /** @type {?} */ shape_selected = shape_type[Math.floor(Math.random() * shape_type.length)];
                this.shape = shape_selected;
            }
        }
        else {
            this.shape = shape_type;
        }
        if (this.shape == 'image') {
            var /** @type {?} */ sh = this._params.particles.shape;
            this.img = {
                src: sh.image.src,
                ratio: sh.image.width / sh.image.height
            };
            if (!this.img.ratio)
                this.img.ratio = 1;
            if (this._tmpParams.img_type == 'svg' && this._tmpParams.source_svg != undefined) {
                createSvgImg(this, this._tmpParams);
                if (this._tmpParams.pushing) {
                    this.img.loaded = false;
                }
            }
        }
    };
    /**
     * @param {?} c
     * @param {?} startX
     * @param {?} startY
     * @param {?} sideLength
     * @param {?} sideCountNumerator
     * @param {?} sideCountDenominator
     * @return {?}
     */
    Particle$$1.prototype._drawShape = function (c, startX, startY, sideLength, sideCountNumerator, sideCountDenominator) {
        var /** @type {?} */ sideCount = sideCountNumerator * sideCountDenominator;
        var /** @type {?} */ decimalSides = sideCountNumerator / sideCountDenominator;
        var /** @type {?} */ interiorAngleDegrees = (180 * (decimalSides - 2)) / decimalSides;
        var /** @type {?} */ interiorAngle = Math.PI - Math.PI * interiorAngleDegrees / 180;
        c.save();
        c.beginPath();
        c.translate(startX, startY);
        c.moveTo(0, 0);
        for (var /** @type {?} */ i = 0; i < sideCount; i++) {
            c.lineTo(sideLength, 0);
            c.translate(sideLength, 0);
            c.rotate(interiorAngle);
        }
        c.fill();
        c.restore();
    };
    /**
     * @return {?}
     */
    Particle$$1.prototype.draw = function () {
        var _this = this;
        var particles = this._params.particles;
        var /** @type {?} */ radius;
        if (this.radius_bubble != undefined) {
            radius = this.radius_bubble;
        }
        else {
            radius = this.radius;
        }
        var /** @type {?} */ opacity;
        if (this.opacity_bubble != undefined) {
            opacity = this.opacity_bubble;
        }
        else {
            opacity = this.opacity;
        }
        var /** @type {?} */ color_value;
        if (this.color.rgb) {
            var _a = this.color.rgb, r = _a.r, g = _a.g, b = _a.b;
            color_value = "rgba( " + r + ", " + g + ", " + b + ", " + opacity + " )";
        }
        else {
            var _b = this.color.hsl, h = _b.h, s = _b.s, l = _b.l;
            color_value = "hsla( " + h + ", " + s + ", " + l + ", " + opacity + " )";
        }
        this._canvasParams.ctx.fillStyle = color_value;
        this._canvasParams.ctx.beginPath();
        switch (this.shape) {
            case 'circle':
                this._canvasParams.ctx.arc(this.x, this.y, radius, 0, Math.PI * 2, false);
                break;
            case 'edge':
                this._canvasParams.ctx.rect(this.x - radius, this.y - radius, radius * 2, radius * 2);
                break;
            case 'triangle':
                this._drawShape(this._canvasParams.ctx, this.x - radius, this.y + radius / 1.66, radius * 2, 3, 2);
                break;
            case 'polygon':
                this._drawShape(this._canvasParams.ctx, this.x - radius / (this._params.particles.shape.polygon.nb_sides / 3.5), this.y - radius / (2.66 / 3.5), radius * 2.66 / (this._params.particles.shape.polygon.nb_sides / 3), this._params.particles.shape.polygon.nb_sides, 1);
                break;
            case 'star':
                this._drawShape(this._canvasParams.ctx, this.x - radius * 2 / (this._params.particles.shape.polygon.nb_sides / 4), this.y - radius / (2 * 2.66 / 3.5), radius * 2 * 2.66 / (this._params.particles.shape.polygon.nb_sides / 3), this._params.particles.shape.polygon.nb_sides, 2);
                break;
            case 'image':
                var /** @type {?} */ draw = function (img_obj) {
                    _this._canvasParams.ctx.drawImage(img_obj, _this.x - radius, _this.y - radius, radius * 2, radius * 2 / _this.img.ratio);
                };
                var /** @type {?} */ img_obj = void 0;
                if (this._tmpParams.img_type == 'svg') {
                    img_obj = this.img.obj;
                }
                else {
                    img_obj = this._tmpParams.img_obj;
                }
                if (img_obj)
                    draw(img_obj);
                break;
        }
        this._canvasParams.ctx.closePath();
        if (this._params.particles.shape.stroke.width > 0) {
            this._canvasParams.ctx.strokeStyle = this._params.particles.shape.stroke.color;
            this._canvasParams.ctx.lineWidth = this._params.particles.shape.stroke.width;
            this._canvasParams.ctx.stroke();
        }
        this._canvasParams.ctx.fill();
    };
    return Particle$$1;
}());

var ParticleInteraction = (function () {
    function ParticleInteraction() {
    }
    /**
     * @param {?} p1
     * @param {?} p2
     * @param {?} params
     * @param {?} canvasParams
     * @return {?}
     */
    ParticleInteraction.prototype.linkParticles = function (p1, p2, params, canvasParams) {
        var /** @type {?} */ dx = p1.x - p2.x;
        var /** @type {?} */ dy = p1.y - p2.y;
        var /** @type {?} */ dist = Math.sqrt(dx * dx + dy * dy);
        var line_linked = params.particles.line_linked;
        if (dist <= params.particles.line_linked.distance) {
            var /** @type {?} */ opacity_line = params.particles.line_linked.opacity - (dist / (1 / params.particles.line_linked.opacity)) / params.particles.line_linked.distance;
            if (opacity_line > 0) {
                var /** @type {?} */ color_line = params.particles.line_linked.color_rgb_line;
                var r = color_line.r, g = color_line.g, b = color_line.b;
                canvasParams.ctx.save();
                canvasParams.ctx.strokeStyle = "rgba( " + r + ", " + g + ", " + b + ", " + opacity_line + " )";
                canvasParams.ctx.lineWidth = params.particles.line_linked.width;
                canvasParams.ctx.beginPath();
                if (line_linked.shadow.enable) {
                    canvasParams.ctx.shadowBlur = line_linked.shadow.blur;
                    canvasParams.ctx.shadowColor = line_linked.shadow.color;
                }
                canvasParams.ctx.moveTo(p1.x, p1.y);
                canvasParams.ctx.lineTo(p2.x, p2.y);
                canvasParams.ctx.stroke();
                canvasParams.ctx.closePath();
                canvasParams.ctx.restore();
            }
        }
    };
    /**
     * @param {?} p1
     * @param {?} p2
     * @param {?} params
     * @return {?}
     */
    ParticleInteraction.prototype.attractParticles = function (p1, p2, params) {
        var /** @type {?} */ dx = p1.x - p2.x;
        var /** @type {?} */ dy = p1.y - p2.y;
        var /** @type {?} */ dist = Math.sqrt(dx * dx + dy * dy);
        if (dist <= params.particles.line_linked.distance) {
            var /** @type {?} */ ax = dx / (params.particles.move.attract.rotateX * 1000);
            var /** @type {?} */ ay = dy / (params.particles.move.attract.rotateY * 1000);
            p1.vx -= ax;
            p1.vy -= ay;
            p2.vx += ax;
            p2.vy += ay;
        }
    };
    /**
     * @param {?} p1
     * @param {?} p2
     * @return {?}
     */
    ParticleInteraction.prototype.bounceParticles = function (p1, p2) {
        var /** @type {?} */ dx = p1.x - p2.x;
        var /** @type {?} */ dy = p1.y - p2.y;
        var /** @type {?} */ dist = Math.sqrt(dx * dx + dy * dy);
        var /** @type {?} */ dist_p = p1.radius + p2.radius;
        if (dist <= dist_p) {
            p1.vx = -p1.vx;
            p1.vy = -p1.vy;
            p2.vx = -p2.vx;
            p2.vy = -p2.vy;
        }
    };
    return ParticleInteraction;
}());

var ParticlesDirective = (function () {
    /**
     * @param {?} el
     */
    function ParticlesDirective(el) {
        this.el = el;
        this._tmpParams = {};
    }
    Object.defineProperty(ParticlesDirective.prototype, "params", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            var /** @type {?} */ defaultParams = getDefaultParams();
            this._params = deepExtend(defaultParams, value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    ParticlesDirective.prototype.ngAfterViewInit = function () {
        this._canvasParams = {
            el: this.el.nativeElement,
            ctx: this.el.nativeElement.getContext('2d'),
            width: this.el.nativeElement.offsetWidth,
            height: this.el.nativeElement.offsetHeight
        };
        this._tmpParams.obj = {
            size_value: this._params.particles.size.value,
            size_anim_speed: this._params.particles.size.anim.speed,
            move_speed: this._params.particles.move.speed,
            line_linked_distance: this._params.particles.line_linked.distance,
            line_linked_width: this._params.particles.line_linked.width,
            mode_grab_distance: this._params.interactivity.modes.grab.distance,
            mode_bubble_distance: this._params.interactivity.modes.bubble.distance,
            mode_bubble_size: this._params.interactivity.modes.bubble.size,
            mode_repulse_distance: this._params.interactivity.modes.repulse.distance
        };
        this._params.interactivity.el = (this._params.interactivity.detect_on == 'window') ? window : this._canvasParams.el;
        if (isInArray('image', this._params.particles.shape.type)) {
            this._tmpParams.img_type = this._params.particles.shape.image.src.substr(this._params.particles.shape.image.src.length - 3);
            loadImg(this._params, this._tmpParams);
        }
        this._canvasManager = new CanvasManager$$1(this._canvasParams, this._params, this._tmpParams);
        this._canvasManager.draw();
    };
    /**
     * Mouse move event
    \@param event
     * @param {?} event
     * @return {?}
     */
    ParticlesDirective.prototype.onMouseMove = function (event) {
        var interactivity = this._params.interactivity;
        if (interactivity.events.onhover.enable ||
            interactivity.events.onclick.enable) {
            var /** @type {?} */ pos = void 0;
            if (interactivity.el == window) {
                pos = {
                    x: event.clientX,
                    y: event.clientY
                };
            }
            else {
                pos = {
                    x: event.offsetX || event.clientX,
                    y: event.offsetY || event.clientY
                };
            }
            interactivity.mouse.pos_x = pos.x;
            interactivity.mouse.pos_y = pos.y;
            if (this._tmpParams.retina) {
                interactivity.mouse.pos_x *= this._canvasParams.pxratio;
                interactivity.mouse.pos_y *= this._canvasParams.pxratio;
            }
            interactivity.status = 'mousemove';
        }
    };
    /**
     * Mouse leave event
     * @return {?}
     */
    ParticlesDirective.prototype.onMouseLeave = function () {
        var interactivity = this._params.interactivity;
        if (interactivity.events.onhover.enable ||
            interactivity.events.onclick.enable) {
            interactivity.mouse.pos_x = null;
            interactivity.mouse.pos_y = null;
            interactivity.status = 'mouseleave';
        }
    };
    /**
     * Click event
     * @return {?}
     */
    ParticlesDirective.prototype.onClick = function () {
        var _this = this;
        var _a = this._params, interactivity = _a.interactivity, particles = _a.particles;
        if (interactivity.events.onclick.enable) {
            interactivity.mouse.click_pos_x = interactivity.mouse.pos_x;
            interactivity.mouse.click_pos_y = interactivity.mouse.pos_y;
            interactivity.mouse.click_time = new Date().getTime();
            switch (interactivity.events.onclick.mode) {
                case 'push':
                    if (particles.move.enable) {
                        this._canvasManager.particlesManager.pushParticles(interactivity.modes.push.particles_nb, interactivity.mouse);
                    }
                    else {
                        if (interactivity.modes.push.particles_nb == 1) {
                            this._canvasManager.particlesManager.pushParticles(interactivity.modes.push.particles_nb, interactivity.mouse);
                        }
                        else if (interactivity.modes.push.particles_nb > 1) {
                            this._canvasManager.particlesManager.pushParticles(interactivity.modes.push.particles_nb);
                        }
                    }
                    break;
                case 'remove':
                    this._canvasManager.particlesManager.removeParticles(interactivity.modes.remove.particles_nb);
                    break;
                case 'bubble':
                    this._tmpParams.bubble_clicking = true;
                    break;
                case 'repulse':
                    this._tmpParams.repulse_clicking = true;
                    this._tmpParams.repulse_count = 0;
                    this._tmpParams.repulse_finish = false;
                    setTimeout(function () {
                        _this._tmpParams.repulse_clicking = false;
                    }, interactivity.modes.repulse.duration * 1000);
                    break;
            }
        }
    };
    return ParticlesDirective;
}());
ParticlesDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[d-particles]'
            },] },
];
/**
 * @nocollapse
 */
ParticlesDirective.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
]; };
ParticlesDirective.propDecorators = {
    'params': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'onMouseMove': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['mousemove', ['$event'],] },],
    'onMouseLeave': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['mouseleave',] },],
    'onClick': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['click',] },],
};

var ParticlesModule = (function () {
    function ParticlesModule() {
    }
    return ParticlesModule;
}());
ParticlesModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
                ],
                declarations: [
                    ParticlesComponent,
                    ParticlesDirective
                ],
                exports: [
                    ParticlesComponent,
                    ParticlesDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
ParticlesModule.ctorParameters = function () { return []; };




/***/ }),

/***/ "./src/app/core/homepage/homepage.component.html":
/*!*******************************************************!*\
  !*** ./src/app/core/homepage/homepage.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-topnavbar></app-topnavbar>\n<app-mobile-navbar></app-mobile-navbar>\n\n\n<!-- A B O V E  T H E  F O L D -->\n<div class=\"nb-above-fold w-100\">\n  <div class=\"nb-color-filter w-100 h-100\">\n\n    <div class=\"row mx-0 w-100 h-100 align-items-center justify-content-center\">\n      <div class=\"d-lg-block d-md-none d-sm-none d-none col-12 align-self-center\">\n        <div class=\"nb-header\"> The resources you need to do fitness,</div>\n        <div class=\"nb-header\"> get rid of pain, and live a healthy life.</div>\n      </div>\n      <div class=\"d-lg-none d-md-block d-sm-block d-none col-12 align-self-center\">\n        <div class=\"nb-header\"> The resources you need to</div>\n        <div class=\"nb-header\"> do fitness, get rid of pain,</div>\n        <div class=\"nb-header\"> and live a healthy life.</div>\n      </div>\n      <div class=\"d-sm-none d-block col-12 align-self-center\">\n        <div class=\"nb-header\"> The resources you need to</div>\n        <div class=\"nb-header\"> do fitness, get rid of pain,</div>\n        <div class=\"nb-header\"> and live a healthy life.</div>\n      </div>\n    </div>\n\n    <div class=\"nb-atf-cta\" pageScroll [pageScrollOffset]=\"scrollOffset\" [pageScrollDuration]=\"1000\" [pageScrollEasing]=\"easeInOutExpo\" href=\"#nb-how-it-works\">\n      <h6>Learn more</h6>\n      <svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\"\n        y=\"0px\" width=\"960px\" height=\"560px\" viewBox=\"0 0 960 560\" enable-background=\"new 0 0 960 560\" xml:space=\"preserve\">\n        <g id=\"Rounded_Rectangle_33_copy_4_1_\">\n          <path d=\"M480,344.181L268.869,131.889c-15.756-15.859-41.3-15.859-57.054,0c-15.754,15.857-15.754,41.57,0,57.431l237.632,238.937\n\t\t          c8.395,8.451,19.562,12.254,30.553,11.698c10.993,0.556,22.159-3.247,30.555-11.698l237.631-238.937\n\t\t          c15.756-15.86,15.756-41.571,0-57.431s-41.299-15.859-57.051,0L480,344.181z\"\n            fill=\"white\" />\n        </g>\n      </svg>\n    </div>\n\n  </div>\n</div>\n\n\n<!-- H O W  I T  W O R K S -->\n<div id=\"nb-how-it-works\" class=\"w-100\">\n  <div class=\"row mx-0 w-100 align-items-center justify-content-center\">\n    <div class=\"nb-intro col-12\">\n      <h3 class=\"mb-3 font-weight-bold\">Our reality today</h3>\n      <p>It is a long established fact that a reader will be distracted by the readable content of a\n        page when looking at its\n        layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed\n        to\n        using 'Content here, content here, making it look like readable English. Many desktop publishing packages and\n        web\n        page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many\n        web\n        sites still in their infancy.</p>\n    </div>\n\n    <div class=\"nb-solution-text\">\n      <h3 class=\"font-weight-bold\">\n        Our solution\n      </h3>\n      <p>\n        their default model text, and a search for 'lorem ipsum' will uncover many\n        web sites still in their infancy.\n        their default model text, and a search for 'lorem ipsum' will uncover many\n        web sites still in their infancy.\n        their default model text, and a search for 'lorem ipsum' will uncover many\n        web sites still in their infancy.\n      </p>\n    </div>\n\n    <div class=\"nb-services w-100\">\n      <hr class=\"w-25 mb-5\">\n\n      <!-- Learning -->\n      <div class=\"row mx-0 w-100 align-items-center justify-content-center\">\n        <div class=\"d-sm-block d-md-none col-lg-4 col-md-5 col-sm-12 align-self-center text-center\">\n          <img class=\"nb-from-left\" src=\"../../../assets/images/temp-icons/light-bulb.svg\">\n          <hr>\n        </div>\n        <div class=\"col-lg-4 col-md-5 col-sm-12 align-self-center\">\n          <div class=\"nb-explanation\">\n            <h3 class=\"mb-3\">Usable knowledge</h3>\n            <p>\n              Discover and learn about a movement based approach to health and fitness. Find a variety of articles and\n              courses to take your health into your own hands and thrive.\n              Discover and learn about a movement based approach to health and fitness. Find a variety of articles and\n              courses to take your health into your own hands and thrive.\n            </p>\n          </div>\n        </div>\n        <div class=\"d-none d-sm-none d-md-block col-lg-4 col-md-5 col-sm-12 align-self-center text-center\">\n          <img class=\"nb-from-left\" src=\"../../../assets/images/temp-icons/light-bulb.svg\">\n          <hr>\n        </div>\n      </div>\n\n      <!-- Tools -->\n      <div class=\"row mx-0 w-100 align-items-center justify-content-center\">\n        <div class=\"col-lg-4 col-md-5 col-sm-12 align-self-center text-center\">\n          <img class=\"nb-from-right\" src=\"../../../assets/images/temp-icons/kettlebell.svg\">\n          <hr>\n        </div>\n        <div class=\"col-lg-4 col-md-5 col-sm-12 align-self-center\">\n          <div class=\"nb-explanation\">\n            <h3 class=\"mb-3\">Essential tools</h3>\n            <p>\n              You've learned a lot, had a few realizations, and have decided to take your health seriously. Great! Time\n              to\n              get a few tools to help you achieve your goals.\n              You've learned a lot, had a few realizations, and have decided to take your health seriously. Great! Time\n              to\n              get a few tools to help you achieve your goals.\n            </p>\n          </div>\n        </div>\n      </div>\n\n      <!-- Specialists -->\n      <div class=\"row mx-0 w-100 align-items-center justify-content-center\">\n        <div class=\"d-sm-block d-md-none col-lg-4 col-md-5 col-sm-12 align-self-center text-center\">\n          <img class=\"nb-from-left\" src=\"../../../assets/images/temp-icons/group.svg\">\n          <hr>\n        </div>\n        <div class=\"col-lg-4 col-md-5 col-sm-12 align-self-center\">\n          <div class=\"nb-explanation\">\n            <h3 class=\"mb-3\">A community</h3>\n            <p>\n              Find movement specialists in your area who share our philosophy. Get the right assistance to get to the\n              root\n              of the problem faster, not just quick fixes and pain killers.\n              Find movement specialists in your area who share our philosophy. Get the right assistance to get to the\n              root\n              of the problem faster, not just quick fixes and pain killers.\n            </p>\n          </div>\n        </div>\n        <div class=\"d-none d-sm-none d-md-block col-lg-4 col-md-5 col-sm-12 align-self-center text-center\">\n          <img class=\"nb-from-left\" src=\"../../../assets/images/temp-icons/group.svg\">\n          <hr>\n        </div>\n      </div>\n\n    </div>\n  </div>\n</div>\n\n\n<!-- C A L L  T O  A C T I O N -->\n<div class=\"nb-cta2-section\">\n  <div class=\"row mx-0 w-100 h-100 align-items-center justify-content-center text-center\">\n    <div class=\"col col-lg-8 col-md-9 col-sm-10 col-11 align-self-center\">\n      <h4 class=\"mb-3 w-100\">\n        Access the platform for free.\n        <br class=\"d-block d-sm-block d-md-none\">\n        Learn, connect, and move!\n      </h4>\n      <div class=\"nb-btn-cta2 mt-4\" (click)=\"routerLink.navigate(['learning'])\">Start my Journey</div>\n    </div>\n  </div>\n</div>\n\n\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/core/homepage/homepage.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/core/homepage/homepage.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*!\n * Bootstrap Grid v4.1.1 (https://getbootstrap.com/)\n * Copyright 2011-2018 The Bootstrap Authors\n * Copyright 2011-2018 Twitter, Inc.\n * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)\n */\n@-ms-viewport {\n  width: device-width; }\nhtml {\n  box-sizing: border-box;\n  -ms-overflow-style: scrollbar; }\n*,\n*::before,\n*::after {\n  box-sizing: inherit; }\n.container {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto; }\n@media (min-width: 576px) {\n    .container {\n      max-width: 540px; } }\n@media (min-width: 768px) {\n    .container {\n      max-width: 720px; } }\n@media (min-width: 992px) {\n    .container {\n      max-width: 960px; } }\n@media (min-width: 1200px) {\n    .container {\n      max-width: 1140px; } }\n.container-fluid {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto; }\n.row {\n  display: flex;\n  flex-wrap: wrap;\n  margin-right: -15px;\n  margin-left: -15px; }\n.no-gutters {\n  margin-right: 0;\n  margin-left: 0; }\n.no-gutters > .col,\n  .no-gutters > [class*=\"col-\"] {\n    padding-right: 0;\n    padding-left: 0; }\n.col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,\n.col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,\n.col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,\n.col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,\n.col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,\n.col-xl-auto {\n  position: relative;\n  width: 100%;\n  min-height: 1px;\n  padding-right: 15px;\n  padding-left: 15px; }\n.col {\n  flex-basis: 0;\n  flex-grow: 1;\n  max-width: 100%; }\n.col-auto {\n  flex: 0 0 auto;\n  width: auto;\n  max-width: none; }\n.col-1 {\n  flex: 0 0 8.33333333%;\n  max-width: 8.33333333%; }\n.col-2 {\n  flex: 0 0 16.66666667%;\n  max-width: 16.66666667%; }\n.col-3 {\n  flex: 0 0 25%;\n  max-width: 25%; }\n.col-4 {\n  flex: 0 0 33.33333333%;\n  max-width: 33.33333333%; }\n.col-5 {\n  flex: 0 0 41.66666667%;\n  max-width: 41.66666667%; }\n.col-6 {\n  flex: 0 0 50%;\n  max-width: 50%; }\n.col-7 {\n  flex: 0 0 58.33333333%;\n  max-width: 58.33333333%; }\n.col-8 {\n  flex: 0 0 66.66666667%;\n  max-width: 66.66666667%; }\n.col-9 {\n  flex: 0 0 75%;\n  max-width: 75%; }\n.col-10 {\n  flex: 0 0 83.33333333%;\n  max-width: 83.33333333%; }\n.col-11 {\n  flex: 0 0 91.66666667%;\n  max-width: 91.66666667%; }\n.col-12 {\n  flex: 0 0 100%;\n  max-width: 100%; }\n.order-first {\n  order: -1; }\n.order-last {\n  order: 13; }\n.order-0 {\n  order: 0; }\n.order-1 {\n  order: 1; }\n.order-2 {\n  order: 2; }\n.order-3 {\n  order: 3; }\n.order-4 {\n  order: 4; }\n.order-5 {\n  order: 5; }\n.order-6 {\n  order: 6; }\n.order-7 {\n  order: 7; }\n.order-8 {\n  order: 8; }\n.order-9 {\n  order: 9; }\n.order-10 {\n  order: 10; }\n.order-11 {\n  order: 11; }\n.order-12 {\n  order: 12; }\n.offset-1 {\n  margin-left: 8.33333333%; }\n.offset-2 {\n  margin-left: 16.66666667%; }\n.offset-3 {\n  margin-left: 25%; }\n.offset-4 {\n  margin-left: 33.33333333%; }\n.offset-5 {\n  margin-left: 41.66666667%; }\n.offset-6 {\n  margin-left: 50%; }\n.offset-7 {\n  margin-left: 58.33333333%; }\n.offset-8 {\n  margin-left: 66.66666667%; }\n.offset-9 {\n  margin-left: 75%; }\n.offset-10 {\n  margin-left: 83.33333333%; }\n.offset-11 {\n  margin-left: 91.66666667%; }\n@media (min-width: 576px) {\n  .col-sm {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-sm-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-sm-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-sm-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-sm-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-sm-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-sm-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-sm-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-sm-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-sm-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-sm-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-sm-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-sm-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-sm-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-sm-first {\n    order: -1; }\n  .order-sm-last {\n    order: 13; }\n  .order-sm-0 {\n    order: 0; }\n  .order-sm-1 {\n    order: 1; }\n  .order-sm-2 {\n    order: 2; }\n  .order-sm-3 {\n    order: 3; }\n  .order-sm-4 {\n    order: 4; }\n  .order-sm-5 {\n    order: 5; }\n  .order-sm-6 {\n    order: 6; }\n  .order-sm-7 {\n    order: 7; }\n  .order-sm-8 {\n    order: 8; }\n  .order-sm-9 {\n    order: 9; }\n  .order-sm-10 {\n    order: 10; }\n  .order-sm-11 {\n    order: 11; }\n  .order-sm-12 {\n    order: 12; }\n  .offset-sm-0 {\n    margin-left: 0; }\n  .offset-sm-1 {\n    margin-left: 8.33333333%; }\n  .offset-sm-2 {\n    margin-left: 16.66666667%; }\n  .offset-sm-3 {\n    margin-left: 25%; }\n  .offset-sm-4 {\n    margin-left: 33.33333333%; }\n  .offset-sm-5 {\n    margin-left: 41.66666667%; }\n  .offset-sm-6 {\n    margin-left: 50%; }\n  .offset-sm-7 {\n    margin-left: 58.33333333%; }\n  .offset-sm-8 {\n    margin-left: 66.66666667%; }\n  .offset-sm-9 {\n    margin-left: 75%; }\n  .offset-sm-10 {\n    margin-left: 83.33333333%; }\n  .offset-sm-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 768px) {\n  .col-md {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-md-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-md-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-md-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-md-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-md-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-md-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-md-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-md-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-md-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-md-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-md-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-md-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-md-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-md-first {\n    order: -1; }\n  .order-md-last {\n    order: 13; }\n  .order-md-0 {\n    order: 0; }\n  .order-md-1 {\n    order: 1; }\n  .order-md-2 {\n    order: 2; }\n  .order-md-3 {\n    order: 3; }\n  .order-md-4 {\n    order: 4; }\n  .order-md-5 {\n    order: 5; }\n  .order-md-6 {\n    order: 6; }\n  .order-md-7 {\n    order: 7; }\n  .order-md-8 {\n    order: 8; }\n  .order-md-9 {\n    order: 9; }\n  .order-md-10 {\n    order: 10; }\n  .order-md-11 {\n    order: 11; }\n  .order-md-12 {\n    order: 12; }\n  .offset-md-0 {\n    margin-left: 0; }\n  .offset-md-1 {\n    margin-left: 8.33333333%; }\n  .offset-md-2 {\n    margin-left: 16.66666667%; }\n  .offset-md-3 {\n    margin-left: 25%; }\n  .offset-md-4 {\n    margin-left: 33.33333333%; }\n  .offset-md-5 {\n    margin-left: 41.66666667%; }\n  .offset-md-6 {\n    margin-left: 50%; }\n  .offset-md-7 {\n    margin-left: 58.33333333%; }\n  .offset-md-8 {\n    margin-left: 66.66666667%; }\n  .offset-md-9 {\n    margin-left: 75%; }\n  .offset-md-10 {\n    margin-left: 83.33333333%; }\n  .offset-md-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 992px) {\n  .col-lg {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-lg-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-lg-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-lg-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-lg-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-lg-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-lg-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-lg-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-lg-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-lg-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-lg-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-lg-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-lg-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-lg-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-lg-first {\n    order: -1; }\n  .order-lg-last {\n    order: 13; }\n  .order-lg-0 {\n    order: 0; }\n  .order-lg-1 {\n    order: 1; }\n  .order-lg-2 {\n    order: 2; }\n  .order-lg-3 {\n    order: 3; }\n  .order-lg-4 {\n    order: 4; }\n  .order-lg-5 {\n    order: 5; }\n  .order-lg-6 {\n    order: 6; }\n  .order-lg-7 {\n    order: 7; }\n  .order-lg-8 {\n    order: 8; }\n  .order-lg-9 {\n    order: 9; }\n  .order-lg-10 {\n    order: 10; }\n  .order-lg-11 {\n    order: 11; }\n  .order-lg-12 {\n    order: 12; }\n  .offset-lg-0 {\n    margin-left: 0; }\n  .offset-lg-1 {\n    margin-left: 8.33333333%; }\n  .offset-lg-2 {\n    margin-left: 16.66666667%; }\n  .offset-lg-3 {\n    margin-left: 25%; }\n  .offset-lg-4 {\n    margin-left: 33.33333333%; }\n  .offset-lg-5 {\n    margin-left: 41.66666667%; }\n  .offset-lg-6 {\n    margin-left: 50%; }\n  .offset-lg-7 {\n    margin-left: 58.33333333%; }\n  .offset-lg-8 {\n    margin-left: 66.66666667%; }\n  .offset-lg-9 {\n    margin-left: 75%; }\n  .offset-lg-10 {\n    margin-left: 83.33333333%; }\n  .offset-lg-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 1200px) {\n  .col-xl {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-xl-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-xl-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-xl-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-xl-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-xl-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-xl-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-xl-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-xl-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-xl-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-xl-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-xl-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-xl-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-xl-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-xl-first {\n    order: -1; }\n  .order-xl-last {\n    order: 13; }\n  .order-xl-0 {\n    order: 0; }\n  .order-xl-1 {\n    order: 1; }\n  .order-xl-2 {\n    order: 2; }\n  .order-xl-3 {\n    order: 3; }\n  .order-xl-4 {\n    order: 4; }\n  .order-xl-5 {\n    order: 5; }\n  .order-xl-6 {\n    order: 6; }\n  .order-xl-7 {\n    order: 7; }\n  .order-xl-8 {\n    order: 8; }\n  .order-xl-9 {\n    order: 9; }\n  .order-xl-10 {\n    order: 10; }\n  .order-xl-11 {\n    order: 11; }\n  .order-xl-12 {\n    order: 12; }\n  .offset-xl-0 {\n    margin-left: 0; }\n  .offset-xl-1 {\n    margin-left: 8.33333333%; }\n  .offset-xl-2 {\n    margin-left: 16.66666667%; }\n  .offset-xl-3 {\n    margin-left: 25%; }\n  .offset-xl-4 {\n    margin-left: 33.33333333%; }\n  .offset-xl-5 {\n    margin-left: 41.66666667%; }\n  .offset-xl-6 {\n    margin-left: 50%; }\n  .offset-xl-7 {\n    margin-left: 58.33333333%; }\n  .offset-xl-8 {\n    margin-left: 66.66666667%; }\n  .offset-xl-9 {\n    margin-left: 75%; }\n  .offset-xl-10 {\n    margin-left: 83.33333333%; }\n  .offset-xl-11 {\n    margin-left: 91.66666667%; } }\n.d-none {\n  display: none !important; }\n.d-inline {\n  display: inline !important; }\n.d-inline-block {\n  display: inline-block !important; }\n.d-block {\n  display: block !important; }\n.d-table {\n  display: table !important; }\n.d-table-row {\n  display: table-row !important; }\n.d-table-cell {\n  display: table-cell !important; }\n.d-flex {\n  display: flex !important; }\n.d-inline-flex {\n  display: inline-flex !important; }\n@media (min-width: 576px) {\n  .d-sm-none {\n    display: none !important; }\n  .d-sm-inline {\n    display: inline !important; }\n  .d-sm-inline-block {\n    display: inline-block !important; }\n  .d-sm-block {\n    display: block !important; }\n  .d-sm-table {\n    display: table !important; }\n  .d-sm-table-row {\n    display: table-row !important; }\n  .d-sm-table-cell {\n    display: table-cell !important; }\n  .d-sm-flex {\n    display: flex !important; }\n  .d-sm-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 768px) {\n  .d-md-none {\n    display: none !important; }\n  .d-md-inline {\n    display: inline !important; }\n  .d-md-inline-block {\n    display: inline-block !important; }\n  .d-md-block {\n    display: block !important; }\n  .d-md-table {\n    display: table !important; }\n  .d-md-table-row {\n    display: table-row !important; }\n  .d-md-table-cell {\n    display: table-cell !important; }\n  .d-md-flex {\n    display: flex !important; }\n  .d-md-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 992px) {\n  .d-lg-none {\n    display: none !important; }\n  .d-lg-inline {\n    display: inline !important; }\n  .d-lg-inline-block {\n    display: inline-block !important; }\n  .d-lg-block {\n    display: block !important; }\n  .d-lg-table {\n    display: table !important; }\n  .d-lg-table-row {\n    display: table-row !important; }\n  .d-lg-table-cell {\n    display: table-cell !important; }\n  .d-lg-flex {\n    display: flex !important; }\n  .d-lg-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 1200px) {\n  .d-xl-none {\n    display: none !important; }\n  .d-xl-inline {\n    display: inline !important; }\n  .d-xl-inline-block {\n    display: inline-block !important; }\n  .d-xl-block {\n    display: block !important; }\n  .d-xl-table {\n    display: table !important; }\n  .d-xl-table-row {\n    display: table-row !important; }\n  .d-xl-table-cell {\n    display: table-cell !important; }\n  .d-xl-flex {\n    display: flex !important; }\n  .d-xl-inline-flex {\n    display: inline-flex !important; } }\n@media print {\n  .d-print-none {\n    display: none !important; }\n  .d-print-inline {\n    display: inline !important; }\n  .d-print-inline-block {\n    display: inline-block !important; }\n  .d-print-block {\n    display: block !important; }\n  .d-print-table {\n    display: table !important; }\n  .d-print-table-row {\n    display: table-row !important; }\n  .d-print-table-cell {\n    display: table-cell !important; }\n  .d-print-flex {\n    display: flex !important; }\n  .d-print-inline-flex {\n    display: inline-flex !important; } }\n.flex-row {\n  flex-direction: row !important; }\n.flex-column {\n  flex-direction: column !important; }\n.flex-row-reverse {\n  flex-direction: row-reverse !important; }\n.flex-column-reverse {\n  flex-direction: column-reverse !important; }\n.flex-wrap {\n  flex-wrap: wrap !important; }\n.flex-nowrap {\n  flex-wrap: nowrap !important; }\n.flex-wrap-reverse {\n  flex-wrap: wrap-reverse !important; }\n.flex-fill {\n  flex: 1 1 auto !important; }\n.flex-grow-0 {\n  flex-grow: 0 !important; }\n.flex-grow-1 {\n  flex-grow: 1 !important; }\n.flex-shrink-0 {\n  flex-shrink: 0 !important; }\n.flex-shrink-1 {\n  flex-shrink: 1 !important; }\n.justify-content-start {\n  justify-content: flex-start !important; }\n.justify-content-end {\n  justify-content: flex-end !important; }\n.justify-content-center {\n  justify-content: center !important; }\n.justify-content-between {\n  justify-content: space-between !important; }\n.justify-content-around {\n  justify-content: space-around !important; }\n.align-items-start {\n  align-items: flex-start !important; }\n.align-items-end {\n  align-items: flex-end !important; }\n.align-items-center {\n  align-items: center !important; }\n.align-items-baseline {\n  align-items: baseline !important; }\n.align-items-stretch {\n  align-items: stretch !important; }\n.align-content-start {\n  align-content: flex-start !important; }\n.align-content-end {\n  align-content: flex-end !important; }\n.align-content-center {\n  align-content: center !important; }\n.align-content-between {\n  align-content: space-between !important; }\n.align-content-around {\n  align-content: space-around !important; }\n.align-content-stretch {\n  align-content: stretch !important; }\n.align-self-auto {\n  -ms-grid-row-align: auto !important;\n      align-self: auto !important; }\n.align-self-start {\n  align-self: flex-start !important; }\n.align-self-end {\n  align-self: flex-end !important; }\n.align-self-center {\n  -ms-grid-row-align: center !important;\n      align-self: center !important; }\n.align-self-baseline {\n  align-self: baseline !important; }\n.align-self-stretch {\n  -ms-grid-row-align: stretch !important;\n      align-self: stretch !important; }\n@media (min-width: 576px) {\n  .flex-sm-row {\n    flex-direction: row !important; }\n  .flex-sm-column {\n    flex-direction: column !important; }\n  .flex-sm-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-sm-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-sm-wrap {\n    flex-wrap: wrap !important; }\n  .flex-sm-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-sm-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-sm-fill {\n    flex: 1 1 auto !important; }\n  .flex-sm-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-sm-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-sm-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-sm-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-sm-start {\n    justify-content: flex-start !important; }\n  .justify-content-sm-end {\n    justify-content: flex-end !important; }\n  .justify-content-sm-center {\n    justify-content: center !important; }\n  .justify-content-sm-between {\n    justify-content: space-between !important; }\n  .justify-content-sm-around {\n    justify-content: space-around !important; }\n  .align-items-sm-start {\n    align-items: flex-start !important; }\n  .align-items-sm-end {\n    align-items: flex-end !important; }\n  .align-items-sm-center {\n    align-items: center !important; }\n  .align-items-sm-baseline {\n    align-items: baseline !important; }\n  .align-items-sm-stretch {\n    align-items: stretch !important; }\n  .align-content-sm-start {\n    align-content: flex-start !important; }\n  .align-content-sm-end {\n    align-content: flex-end !important; }\n  .align-content-sm-center {\n    align-content: center !important; }\n  .align-content-sm-between {\n    align-content: space-between !important; }\n  .align-content-sm-around {\n    align-content: space-around !important; }\n  .align-content-sm-stretch {\n    align-content: stretch !important; }\n  .align-self-sm-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-sm-start {\n    align-self: flex-start !important; }\n  .align-self-sm-end {\n    align-self: flex-end !important; }\n  .align-self-sm-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-sm-baseline {\n    align-self: baseline !important; }\n  .align-self-sm-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 768px) {\n  .flex-md-row {\n    flex-direction: row !important; }\n  .flex-md-column {\n    flex-direction: column !important; }\n  .flex-md-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-md-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-md-wrap {\n    flex-wrap: wrap !important; }\n  .flex-md-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-md-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-md-fill {\n    flex: 1 1 auto !important; }\n  .flex-md-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-md-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-md-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-md-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-md-start {\n    justify-content: flex-start !important; }\n  .justify-content-md-end {\n    justify-content: flex-end !important; }\n  .justify-content-md-center {\n    justify-content: center !important; }\n  .justify-content-md-between {\n    justify-content: space-between !important; }\n  .justify-content-md-around {\n    justify-content: space-around !important; }\n  .align-items-md-start {\n    align-items: flex-start !important; }\n  .align-items-md-end {\n    align-items: flex-end !important; }\n  .align-items-md-center {\n    align-items: center !important; }\n  .align-items-md-baseline {\n    align-items: baseline !important; }\n  .align-items-md-stretch {\n    align-items: stretch !important; }\n  .align-content-md-start {\n    align-content: flex-start !important; }\n  .align-content-md-end {\n    align-content: flex-end !important; }\n  .align-content-md-center {\n    align-content: center !important; }\n  .align-content-md-between {\n    align-content: space-between !important; }\n  .align-content-md-around {\n    align-content: space-around !important; }\n  .align-content-md-stretch {\n    align-content: stretch !important; }\n  .align-self-md-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-md-start {\n    align-self: flex-start !important; }\n  .align-self-md-end {\n    align-self: flex-end !important; }\n  .align-self-md-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-md-baseline {\n    align-self: baseline !important; }\n  .align-self-md-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 992px) {\n  .flex-lg-row {\n    flex-direction: row !important; }\n  .flex-lg-column {\n    flex-direction: column !important; }\n  .flex-lg-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-lg-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-lg-wrap {\n    flex-wrap: wrap !important; }\n  .flex-lg-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-lg-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-lg-fill {\n    flex: 1 1 auto !important; }\n  .flex-lg-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-lg-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-lg-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-lg-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-lg-start {\n    justify-content: flex-start !important; }\n  .justify-content-lg-end {\n    justify-content: flex-end !important; }\n  .justify-content-lg-center {\n    justify-content: center !important; }\n  .justify-content-lg-between {\n    justify-content: space-between !important; }\n  .justify-content-lg-around {\n    justify-content: space-around !important; }\n  .align-items-lg-start {\n    align-items: flex-start !important; }\n  .align-items-lg-end {\n    align-items: flex-end !important; }\n  .align-items-lg-center {\n    align-items: center !important; }\n  .align-items-lg-baseline {\n    align-items: baseline !important; }\n  .align-items-lg-stretch {\n    align-items: stretch !important; }\n  .align-content-lg-start {\n    align-content: flex-start !important; }\n  .align-content-lg-end {\n    align-content: flex-end !important; }\n  .align-content-lg-center {\n    align-content: center !important; }\n  .align-content-lg-between {\n    align-content: space-between !important; }\n  .align-content-lg-around {\n    align-content: space-around !important; }\n  .align-content-lg-stretch {\n    align-content: stretch !important; }\n  .align-self-lg-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-lg-start {\n    align-self: flex-start !important; }\n  .align-self-lg-end {\n    align-self: flex-end !important; }\n  .align-self-lg-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-lg-baseline {\n    align-self: baseline !important; }\n  .align-self-lg-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 1200px) {\n  .flex-xl-row {\n    flex-direction: row !important; }\n  .flex-xl-column {\n    flex-direction: column !important; }\n  .flex-xl-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-xl-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-xl-wrap {\n    flex-wrap: wrap !important; }\n  .flex-xl-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-xl-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-xl-fill {\n    flex: 1 1 auto !important; }\n  .flex-xl-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-xl-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-xl-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-xl-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-xl-start {\n    justify-content: flex-start !important; }\n  .justify-content-xl-end {\n    justify-content: flex-end !important; }\n  .justify-content-xl-center {\n    justify-content: center !important; }\n  .justify-content-xl-between {\n    justify-content: space-between !important; }\n  .justify-content-xl-around {\n    justify-content: space-around !important; }\n  .align-items-xl-start {\n    align-items: flex-start !important; }\n  .align-items-xl-end {\n    align-items: flex-end !important; }\n  .align-items-xl-center {\n    align-items: center !important; }\n  .align-items-xl-baseline {\n    align-items: baseline !important; }\n  .align-items-xl-stretch {\n    align-items: stretch !important; }\n  .align-content-xl-start {\n    align-content: flex-start !important; }\n  .align-content-xl-end {\n    align-content: flex-end !important; }\n  .align-content-xl-center {\n    align-content: center !important; }\n  .align-content-xl-between {\n    align-content: space-between !important; }\n  .align-content-xl-around {\n    align-content: space-around !important; }\n  .align-content-xl-stretch {\n    align-content: stretch !important; }\n  .align-self-xl-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-xl-start {\n    align-self: flex-start !important; }\n  .align-self-xl-end {\n    align-self: flex-end !important; }\n  .align-self-xl-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-xl-baseline {\n    align-self: baseline !important; }\n  .align-self-xl-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n.validation-animation {\n  display: inline-block !important; }\n.validation-animation svg {\n    width: 40px; }\n.validation-animation .path {\n    stroke-dasharray: 1000;\n    stroke-dashoffset: 0; }\n.validation-animation .path.circle {\n      -webkit-animation: dash 1.2s ease-in-out;\n      animation: dash 1.2s ease-in-out; }\n.validation-animation .path.line {\n      stroke-dashoffset: 1000;\n      -webkit-animation: dash 1.2s .4s ease-in-out forwards;\n      animation: dash .9s .4s ease-in-out forwards; }\n.validation-animation .path.check {\n      stroke-dashoffset: -100;\n      -webkit-animation: dash-check 1.2s .4s ease-in-out forwards;\n      animation: dash-check 1.2s .4s ease-in-out forwards; }\n@-webkit-keyframes dash {\n  0% {\n    stroke-dashoffset: 1000; }\n  100% {\n    stroke-dashoffset: 0; } }\n@keyframes dash {\n  0% {\n    stroke-dashoffset: 1000; }\n  100% {\n    stroke-dashoffset: 0; } }\n@-webkit-keyframes dash-check {\n  0% {\n    stroke-dashoffset: -100; }\n  100% {\n    stroke-dashoffset: 900; } }\n@keyframes dash-check {\n  0% {\n    stroke-dashoffset: -100; }\n  100% {\n    stroke-dashoffset: 900; } }\n/*!\n * Bootstrap Grid v4.1.1 (https://getbootstrap.com/)\n * Copyright 2011-2018 The Bootstrap Authors\n * Copyright 2011-2018 Twitter, Inc.\n * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)\n */\n@-ms-viewport {\n  width: device-width; }\nhtml {\n  box-sizing: border-box;\n  -ms-overflow-style: scrollbar; }\n*,\n*::before,\n*::after {\n  box-sizing: inherit; }\n.container {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto; }\n@media (min-width: 576px) {\n    .container {\n      max-width: 540px; } }\n@media (min-width: 768px) {\n    .container {\n      max-width: 720px; } }\n@media (min-width: 992px) {\n    .container {\n      max-width: 960px; } }\n@media (min-width: 1200px) {\n    .container {\n      max-width: 1140px; } }\n.container-fluid {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto; }\n.row {\n  display: flex;\n  flex-wrap: wrap;\n  margin-right: -15px;\n  margin-left: -15px; }\n.no-gutters {\n  margin-right: 0;\n  margin-left: 0; }\n.no-gutters > .col,\n  .no-gutters > [class*=\"col-\"] {\n    padding-right: 0;\n    padding-left: 0; }\n.col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,\n.col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,\n.col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,\n.col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,\n.col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,\n.col-xl-auto {\n  position: relative;\n  width: 100%;\n  min-height: 1px;\n  padding-right: 15px;\n  padding-left: 15px; }\n.col {\n  flex-basis: 0;\n  flex-grow: 1;\n  max-width: 100%; }\n.col-auto {\n  flex: 0 0 auto;\n  width: auto;\n  max-width: none; }\n.col-1 {\n  flex: 0 0 8.33333333%;\n  max-width: 8.33333333%; }\n.col-2 {\n  flex: 0 0 16.66666667%;\n  max-width: 16.66666667%; }\n.col-3 {\n  flex: 0 0 25%;\n  max-width: 25%; }\n.col-4 {\n  flex: 0 0 33.33333333%;\n  max-width: 33.33333333%; }\n.col-5 {\n  flex: 0 0 41.66666667%;\n  max-width: 41.66666667%; }\n.col-6 {\n  flex: 0 0 50%;\n  max-width: 50%; }\n.col-7 {\n  flex: 0 0 58.33333333%;\n  max-width: 58.33333333%; }\n.col-8 {\n  flex: 0 0 66.66666667%;\n  max-width: 66.66666667%; }\n.col-9 {\n  flex: 0 0 75%;\n  max-width: 75%; }\n.col-10 {\n  flex: 0 0 83.33333333%;\n  max-width: 83.33333333%; }\n.col-11 {\n  flex: 0 0 91.66666667%;\n  max-width: 91.66666667%; }\n.col-12 {\n  flex: 0 0 100%;\n  max-width: 100%; }\n.order-first {\n  order: -1; }\n.order-last {\n  order: 13; }\n.order-0 {\n  order: 0; }\n.order-1 {\n  order: 1; }\n.order-2 {\n  order: 2; }\n.order-3 {\n  order: 3; }\n.order-4 {\n  order: 4; }\n.order-5 {\n  order: 5; }\n.order-6 {\n  order: 6; }\n.order-7 {\n  order: 7; }\n.order-8 {\n  order: 8; }\n.order-9 {\n  order: 9; }\n.order-10 {\n  order: 10; }\n.order-11 {\n  order: 11; }\n.order-12 {\n  order: 12; }\n.offset-1 {\n  margin-left: 8.33333333%; }\n.offset-2 {\n  margin-left: 16.66666667%; }\n.offset-3 {\n  margin-left: 25%; }\n.offset-4 {\n  margin-left: 33.33333333%; }\n.offset-5 {\n  margin-left: 41.66666667%; }\n.offset-6 {\n  margin-left: 50%; }\n.offset-7 {\n  margin-left: 58.33333333%; }\n.offset-8 {\n  margin-left: 66.66666667%; }\n.offset-9 {\n  margin-left: 75%; }\n.offset-10 {\n  margin-left: 83.33333333%; }\n.offset-11 {\n  margin-left: 91.66666667%; }\n@media (min-width: 576px) {\n  .col-sm {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-sm-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-sm-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-sm-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-sm-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-sm-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-sm-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-sm-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-sm-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-sm-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-sm-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-sm-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-sm-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-sm-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-sm-first {\n    order: -1; }\n  .order-sm-last {\n    order: 13; }\n  .order-sm-0 {\n    order: 0; }\n  .order-sm-1 {\n    order: 1; }\n  .order-sm-2 {\n    order: 2; }\n  .order-sm-3 {\n    order: 3; }\n  .order-sm-4 {\n    order: 4; }\n  .order-sm-5 {\n    order: 5; }\n  .order-sm-6 {\n    order: 6; }\n  .order-sm-7 {\n    order: 7; }\n  .order-sm-8 {\n    order: 8; }\n  .order-sm-9 {\n    order: 9; }\n  .order-sm-10 {\n    order: 10; }\n  .order-sm-11 {\n    order: 11; }\n  .order-sm-12 {\n    order: 12; }\n  .offset-sm-0 {\n    margin-left: 0; }\n  .offset-sm-1 {\n    margin-left: 8.33333333%; }\n  .offset-sm-2 {\n    margin-left: 16.66666667%; }\n  .offset-sm-3 {\n    margin-left: 25%; }\n  .offset-sm-4 {\n    margin-left: 33.33333333%; }\n  .offset-sm-5 {\n    margin-left: 41.66666667%; }\n  .offset-sm-6 {\n    margin-left: 50%; }\n  .offset-sm-7 {\n    margin-left: 58.33333333%; }\n  .offset-sm-8 {\n    margin-left: 66.66666667%; }\n  .offset-sm-9 {\n    margin-left: 75%; }\n  .offset-sm-10 {\n    margin-left: 83.33333333%; }\n  .offset-sm-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 768px) {\n  .col-md {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-md-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-md-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-md-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-md-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-md-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-md-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-md-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-md-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-md-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-md-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-md-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-md-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-md-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-md-first {\n    order: -1; }\n  .order-md-last {\n    order: 13; }\n  .order-md-0 {\n    order: 0; }\n  .order-md-1 {\n    order: 1; }\n  .order-md-2 {\n    order: 2; }\n  .order-md-3 {\n    order: 3; }\n  .order-md-4 {\n    order: 4; }\n  .order-md-5 {\n    order: 5; }\n  .order-md-6 {\n    order: 6; }\n  .order-md-7 {\n    order: 7; }\n  .order-md-8 {\n    order: 8; }\n  .order-md-9 {\n    order: 9; }\n  .order-md-10 {\n    order: 10; }\n  .order-md-11 {\n    order: 11; }\n  .order-md-12 {\n    order: 12; }\n  .offset-md-0 {\n    margin-left: 0; }\n  .offset-md-1 {\n    margin-left: 8.33333333%; }\n  .offset-md-2 {\n    margin-left: 16.66666667%; }\n  .offset-md-3 {\n    margin-left: 25%; }\n  .offset-md-4 {\n    margin-left: 33.33333333%; }\n  .offset-md-5 {\n    margin-left: 41.66666667%; }\n  .offset-md-6 {\n    margin-left: 50%; }\n  .offset-md-7 {\n    margin-left: 58.33333333%; }\n  .offset-md-8 {\n    margin-left: 66.66666667%; }\n  .offset-md-9 {\n    margin-left: 75%; }\n  .offset-md-10 {\n    margin-left: 83.33333333%; }\n  .offset-md-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 992px) {\n  .col-lg {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-lg-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-lg-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-lg-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-lg-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-lg-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-lg-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-lg-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-lg-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-lg-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-lg-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-lg-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-lg-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-lg-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-lg-first {\n    order: -1; }\n  .order-lg-last {\n    order: 13; }\n  .order-lg-0 {\n    order: 0; }\n  .order-lg-1 {\n    order: 1; }\n  .order-lg-2 {\n    order: 2; }\n  .order-lg-3 {\n    order: 3; }\n  .order-lg-4 {\n    order: 4; }\n  .order-lg-5 {\n    order: 5; }\n  .order-lg-6 {\n    order: 6; }\n  .order-lg-7 {\n    order: 7; }\n  .order-lg-8 {\n    order: 8; }\n  .order-lg-9 {\n    order: 9; }\n  .order-lg-10 {\n    order: 10; }\n  .order-lg-11 {\n    order: 11; }\n  .order-lg-12 {\n    order: 12; }\n  .offset-lg-0 {\n    margin-left: 0; }\n  .offset-lg-1 {\n    margin-left: 8.33333333%; }\n  .offset-lg-2 {\n    margin-left: 16.66666667%; }\n  .offset-lg-3 {\n    margin-left: 25%; }\n  .offset-lg-4 {\n    margin-left: 33.33333333%; }\n  .offset-lg-5 {\n    margin-left: 41.66666667%; }\n  .offset-lg-6 {\n    margin-left: 50%; }\n  .offset-lg-7 {\n    margin-left: 58.33333333%; }\n  .offset-lg-8 {\n    margin-left: 66.66666667%; }\n  .offset-lg-9 {\n    margin-left: 75%; }\n  .offset-lg-10 {\n    margin-left: 83.33333333%; }\n  .offset-lg-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 1200px) {\n  .col-xl {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-xl-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-xl-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-xl-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-xl-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-xl-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-xl-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-xl-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-xl-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-xl-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-xl-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-xl-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-xl-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-xl-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-xl-first {\n    order: -1; }\n  .order-xl-last {\n    order: 13; }\n  .order-xl-0 {\n    order: 0; }\n  .order-xl-1 {\n    order: 1; }\n  .order-xl-2 {\n    order: 2; }\n  .order-xl-3 {\n    order: 3; }\n  .order-xl-4 {\n    order: 4; }\n  .order-xl-5 {\n    order: 5; }\n  .order-xl-6 {\n    order: 6; }\n  .order-xl-7 {\n    order: 7; }\n  .order-xl-8 {\n    order: 8; }\n  .order-xl-9 {\n    order: 9; }\n  .order-xl-10 {\n    order: 10; }\n  .order-xl-11 {\n    order: 11; }\n  .order-xl-12 {\n    order: 12; }\n  .offset-xl-0 {\n    margin-left: 0; }\n  .offset-xl-1 {\n    margin-left: 8.33333333%; }\n  .offset-xl-2 {\n    margin-left: 16.66666667%; }\n  .offset-xl-3 {\n    margin-left: 25%; }\n  .offset-xl-4 {\n    margin-left: 33.33333333%; }\n  .offset-xl-5 {\n    margin-left: 41.66666667%; }\n  .offset-xl-6 {\n    margin-left: 50%; }\n  .offset-xl-7 {\n    margin-left: 58.33333333%; }\n  .offset-xl-8 {\n    margin-left: 66.66666667%; }\n  .offset-xl-9 {\n    margin-left: 75%; }\n  .offset-xl-10 {\n    margin-left: 83.33333333%; }\n  .offset-xl-11 {\n    margin-left: 91.66666667%; } }\n.d-none {\n  display: none !important; }\n.d-inline {\n  display: inline !important; }\n.d-inline-block {\n  display: inline-block !important; }\n.d-block {\n  display: block !important; }\n.d-table {\n  display: table !important; }\n.d-table-row {\n  display: table-row !important; }\n.d-table-cell {\n  display: table-cell !important; }\n.d-flex {\n  display: flex !important; }\n.d-inline-flex {\n  display: inline-flex !important; }\n@media (min-width: 576px) {\n  .d-sm-none {\n    display: none !important; }\n  .d-sm-inline {\n    display: inline !important; }\n  .d-sm-inline-block {\n    display: inline-block !important; }\n  .d-sm-block {\n    display: block !important; }\n  .d-sm-table {\n    display: table !important; }\n  .d-sm-table-row {\n    display: table-row !important; }\n  .d-sm-table-cell {\n    display: table-cell !important; }\n  .d-sm-flex {\n    display: flex !important; }\n  .d-sm-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 768px) {\n  .d-md-none {\n    display: none !important; }\n  .d-md-inline {\n    display: inline !important; }\n  .d-md-inline-block {\n    display: inline-block !important; }\n  .d-md-block {\n    display: block !important; }\n  .d-md-table {\n    display: table !important; }\n  .d-md-table-row {\n    display: table-row !important; }\n  .d-md-table-cell {\n    display: table-cell !important; }\n  .d-md-flex {\n    display: flex !important; }\n  .d-md-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 992px) {\n  .d-lg-none {\n    display: none !important; }\n  .d-lg-inline {\n    display: inline !important; }\n  .d-lg-inline-block {\n    display: inline-block !important; }\n  .d-lg-block {\n    display: block !important; }\n  .d-lg-table {\n    display: table !important; }\n  .d-lg-table-row {\n    display: table-row !important; }\n  .d-lg-table-cell {\n    display: table-cell !important; }\n  .d-lg-flex {\n    display: flex !important; }\n  .d-lg-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 1200px) {\n  .d-xl-none {\n    display: none !important; }\n  .d-xl-inline {\n    display: inline !important; }\n  .d-xl-inline-block {\n    display: inline-block !important; }\n  .d-xl-block {\n    display: block !important; }\n  .d-xl-table {\n    display: table !important; }\n  .d-xl-table-row {\n    display: table-row !important; }\n  .d-xl-table-cell {\n    display: table-cell !important; }\n  .d-xl-flex {\n    display: flex !important; }\n  .d-xl-inline-flex {\n    display: inline-flex !important; } }\n@media print {\n  .d-print-none {\n    display: none !important; }\n  .d-print-inline {\n    display: inline !important; }\n  .d-print-inline-block {\n    display: inline-block !important; }\n  .d-print-block {\n    display: block !important; }\n  .d-print-table {\n    display: table !important; }\n  .d-print-table-row {\n    display: table-row !important; }\n  .d-print-table-cell {\n    display: table-cell !important; }\n  .d-print-flex {\n    display: flex !important; }\n  .d-print-inline-flex {\n    display: inline-flex !important; } }\n.flex-row {\n  flex-direction: row !important; }\n.flex-column {\n  flex-direction: column !important; }\n.flex-row-reverse {\n  flex-direction: row-reverse !important; }\n.flex-column-reverse {\n  flex-direction: column-reverse !important; }\n.flex-wrap {\n  flex-wrap: wrap !important; }\n.flex-nowrap {\n  flex-wrap: nowrap !important; }\n.flex-wrap-reverse {\n  flex-wrap: wrap-reverse !important; }\n.flex-fill {\n  flex: 1 1 auto !important; }\n.flex-grow-0 {\n  flex-grow: 0 !important; }\n.flex-grow-1 {\n  flex-grow: 1 !important; }\n.flex-shrink-0 {\n  flex-shrink: 0 !important; }\n.flex-shrink-1 {\n  flex-shrink: 1 !important; }\n.justify-content-start {\n  justify-content: flex-start !important; }\n.justify-content-end {\n  justify-content: flex-end !important; }\n.justify-content-center {\n  justify-content: center !important; }\n.justify-content-between {\n  justify-content: space-between !important; }\n.justify-content-around {\n  justify-content: space-around !important; }\n.align-items-start {\n  align-items: flex-start !important; }\n.align-items-end {\n  align-items: flex-end !important; }\n.align-items-center {\n  align-items: center !important; }\n.align-items-baseline {\n  align-items: baseline !important; }\n.align-items-stretch {\n  align-items: stretch !important; }\n.align-content-start {\n  align-content: flex-start !important; }\n.align-content-end {\n  align-content: flex-end !important; }\n.align-content-center {\n  align-content: center !important; }\n.align-content-between {\n  align-content: space-between !important; }\n.align-content-around {\n  align-content: space-around !important; }\n.align-content-stretch {\n  align-content: stretch !important; }\n.align-self-auto {\n  -ms-grid-row-align: auto !important;\n      align-self: auto !important; }\n.align-self-start {\n  align-self: flex-start !important; }\n.align-self-end {\n  align-self: flex-end !important; }\n.align-self-center {\n  -ms-grid-row-align: center !important;\n      align-self: center !important; }\n.align-self-baseline {\n  align-self: baseline !important; }\n.align-self-stretch {\n  -ms-grid-row-align: stretch !important;\n      align-self: stretch !important; }\n@media (min-width: 576px) {\n  .flex-sm-row {\n    flex-direction: row !important; }\n  .flex-sm-column {\n    flex-direction: column !important; }\n  .flex-sm-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-sm-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-sm-wrap {\n    flex-wrap: wrap !important; }\n  .flex-sm-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-sm-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-sm-fill {\n    flex: 1 1 auto !important; }\n  .flex-sm-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-sm-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-sm-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-sm-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-sm-start {\n    justify-content: flex-start !important; }\n  .justify-content-sm-end {\n    justify-content: flex-end !important; }\n  .justify-content-sm-center {\n    justify-content: center !important; }\n  .justify-content-sm-between {\n    justify-content: space-between !important; }\n  .justify-content-sm-around {\n    justify-content: space-around !important; }\n  .align-items-sm-start {\n    align-items: flex-start !important; }\n  .align-items-sm-end {\n    align-items: flex-end !important; }\n  .align-items-sm-center {\n    align-items: center !important; }\n  .align-items-sm-baseline {\n    align-items: baseline !important; }\n  .align-items-sm-stretch {\n    align-items: stretch !important; }\n  .align-content-sm-start {\n    align-content: flex-start !important; }\n  .align-content-sm-end {\n    align-content: flex-end !important; }\n  .align-content-sm-center {\n    align-content: center !important; }\n  .align-content-sm-between {\n    align-content: space-between !important; }\n  .align-content-sm-around {\n    align-content: space-around !important; }\n  .align-content-sm-stretch {\n    align-content: stretch !important; }\n  .align-self-sm-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-sm-start {\n    align-self: flex-start !important; }\n  .align-self-sm-end {\n    align-self: flex-end !important; }\n  .align-self-sm-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-sm-baseline {\n    align-self: baseline !important; }\n  .align-self-sm-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 768px) {\n  .flex-md-row {\n    flex-direction: row !important; }\n  .flex-md-column {\n    flex-direction: column !important; }\n  .flex-md-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-md-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-md-wrap {\n    flex-wrap: wrap !important; }\n  .flex-md-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-md-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-md-fill {\n    flex: 1 1 auto !important; }\n  .flex-md-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-md-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-md-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-md-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-md-start {\n    justify-content: flex-start !important; }\n  .justify-content-md-end {\n    justify-content: flex-end !important; }\n  .justify-content-md-center {\n    justify-content: center !important; }\n  .justify-content-md-between {\n    justify-content: space-between !important; }\n  .justify-content-md-around {\n    justify-content: space-around !important; }\n  .align-items-md-start {\n    align-items: flex-start !important; }\n  .align-items-md-end {\n    align-items: flex-end !important; }\n  .align-items-md-center {\n    align-items: center !important; }\n  .align-items-md-baseline {\n    align-items: baseline !important; }\n  .align-items-md-stretch {\n    align-items: stretch !important; }\n  .align-content-md-start {\n    align-content: flex-start !important; }\n  .align-content-md-end {\n    align-content: flex-end !important; }\n  .align-content-md-center {\n    align-content: center !important; }\n  .align-content-md-between {\n    align-content: space-between !important; }\n  .align-content-md-around {\n    align-content: space-around !important; }\n  .align-content-md-stretch {\n    align-content: stretch !important; }\n  .align-self-md-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-md-start {\n    align-self: flex-start !important; }\n  .align-self-md-end {\n    align-self: flex-end !important; }\n  .align-self-md-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-md-baseline {\n    align-self: baseline !important; }\n  .align-self-md-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 992px) {\n  .flex-lg-row {\n    flex-direction: row !important; }\n  .flex-lg-column {\n    flex-direction: column !important; }\n  .flex-lg-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-lg-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-lg-wrap {\n    flex-wrap: wrap !important; }\n  .flex-lg-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-lg-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-lg-fill {\n    flex: 1 1 auto !important; }\n  .flex-lg-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-lg-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-lg-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-lg-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-lg-start {\n    justify-content: flex-start !important; }\n  .justify-content-lg-end {\n    justify-content: flex-end !important; }\n  .justify-content-lg-center {\n    justify-content: center !important; }\n  .justify-content-lg-between {\n    justify-content: space-between !important; }\n  .justify-content-lg-around {\n    justify-content: space-around !important; }\n  .align-items-lg-start {\n    align-items: flex-start !important; }\n  .align-items-lg-end {\n    align-items: flex-end !important; }\n  .align-items-lg-center {\n    align-items: center !important; }\n  .align-items-lg-baseline {\n    align-items: baseline !important; }\n  .align-items-lg-stretch {\n    align-items: stretch !important; }\n  .align-content-lg-start {\n    align-content: flex-start !important; }\n  .align-content-lg-end {\n    align-content: flex-end !important; }\n  .align-content-lg-center {\n    align-content: center !important; }\n  .align-content-lg-between {\n    align-content: space-between !important; }\n  .align-content-lg-around {\n    align-content: space-around !important; }\n  .align-content-lg-stretch {\n    align-content: stretch !important; }\n  .align-self-lg-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-lg-start {\n    align-self: flex-start !important; }\n  .align-self-lg-end {\n    align-self: flex-end !important; }\n  .align-self-lg-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-lg-baseline {\n    align-self: baseline !important; }\n  .align-self-lg-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 1200px) {\n  .flex-xl-row {\n    flex-direction: row !important; }\n  .flex-xl-column {\n    flex-direction: column !important; }\n  .flex-xl-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-xl-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-xl-wrap {\n    flex-wrap: wrap !important; }\n  .flex-xl-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-xl-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-xl-fill {\n    flex: 1 1 auto !important; }\n  .flex-xl-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-xl-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-xl-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-xl-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-xl-start {\n    justify-content: flex-start !important; }\n  .justify-content-xl-end {\n    justify-content: flex-end !important; }\n  .justify-content-xl-center {\n    justify-content: center !important; }\n  .justify-content-xl-between {\n    justify-content: space-between !important; }\n  .justify-content-xl-around {\n    justify-content: space-around !important; }\n  .align-items-xl-start {\n    align-items: flex-start !important; }\n  .align-items-xl-end {\n    align-items: flex-end !important; }\n  .align-items-xl-center {\n    align-items: center !important; }\n  .align-items-xl-baseline {\n    align-items: baseline !important; }\n  .align-items-xl-stretch {\n    align-items: stretch !important; }\n  .align-content-xl-start {\n    align-content: flex-start !important; }\n  .align-content-xl-end {\n    align-content: flex-end !important; }\n  .align-content-xl-center {\n    align-content: center !important; }\n  .align-content-xl-between {\n    align-content: space-between !important; }\n  .align-content-xl-around {\n    align-content: space-around !important; }\n  .align-content-xl-stretch {\n    align-content: stretch !important; }\n  .align-self-xl-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-xl-start {\n    align-self: flex-start !important; }\n  .align-self-xl-end {\n    align-self: flex-end !important; }\n  .align-self-xl-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-xl-baseline {\n    align-self: baseline !important; }\n  .align-self-xl-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n.validation-animation {\n  display: inline-block !important; }\n.validation-animation svg {\n    width: 40px; }\n.validation-animation .path {\n    stroke-dasharray: 1000;\n    stroke-dashoffset: 0; }\n.validation-animation .path.circle {\n      -webkit-animation: dash 1.2s ease-in-out;\n      animation: dash 1.2s ease-in-out; }\n.validation-animation .path.line {\n      stroke-dashoffset: 1000;\n      -webkit-animation: dash 1.2s .4s ease-in-out forwards;\n      animation: dash .9s .4s ease-in-out forwards; }\n.validation-animation .path.check {\n      stroke-dashoffset: -100;\n      -webkit-animation: dash-check 1.2s .4s ease-in-out forwards;\n      animation: dash-check 1.2s .4s ease-in-out forwards; }\n@-webkit-keyframes dash {\n  0% {\n    stroke-dashoffset: 1000; }\n  100% {\n    stroke-dashoffset: 0; } }\n@keyframes dash {\n  0% {\n    stroke-dashoffset: 1000; }\n  100% {\n    stroke-dashoffset: 0; } }\n@-webkit-keyframes dash-check {\n  0% {\n    stroke-dashoffset: -100; }\n  100% {\n    stroke-dashoffset: 900; } }\n@keyframes dash-check {\n  0% {\n    stroke-dashoffset: -100; }\n  100% {\n    stroke-dashoffset: 900; } }\n.hover-on-menu-item-animation {\n  display: inline-block;\n  position: relative; }\n.hover-on-menu-item-animation::after {\n  content: '';\n  position: absolute;\n  width: 100%;\n  -webkit-transform: scaleX(0);\n  transform: scaleX(0);\n  height: 2px;\n  bottom: 0;\n  left: 0;\n  background-color: #3AAFA9;\n  -webkit-transform-origin: bottom right;\n  transform-origin: bottom right;\n  transition: -webkit-transform 0.25s ease-out;\n  transition: transform 0.25s ease-out;\n  transition: transform 0.25s ease-out, -webkit-transform 0.25s ease-out; }\n.hover-on-menu-item-animation:hover {\n  color: #3AAFA9;\n  cursor: pointer; }\n.hover-on-menu-item-animation:hover::after {\n  -webkit-transform: scaleX(1);\n  transform: scaleX(1);\n  -webkit-transform-origin: bottom left;\n  transform-origin: bottom left; }\n/*!\n * Bootstrap Grid v4.1.1 (https://getbootstrap.com/)\n * Copyright 2011-2018 The Bootstrap Authors\n * Copyright 2011-2018 Twitter, Inc.\n * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)\n */\n@-ms-viewport {\n  width: device-width; }\nhtml {\n  box-sizing: border-box;\n  -ms-overflow-style: scrollbar; }\n*,\n*::before,\n*::after {\n  box-sizing: inherit; }\n.container {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto; }\n@media (min-width: 576px) {\n    .container {\n      max-width: 540px; } }\n@media (min-width: 768px) {\n    .container {\n      max-width: 720px; } }\n@media (min-width: 992px) {\n    .container {\n      max-width: 960px; } }\n@media (min-width: 1200px) {\n    .container {\n      max-width: 1140px; } }\n.container-fluid {\n  width: 100%;\n  padding-right: 15px;\n  padding-left: 15px;\n  margin-right: auto;\n  margin-left: auto; }\n.row {\n  display: flex;\n  flex-wrap: wrap;\n  margin-right: -15px;\n  margin-left: -15px; }\n.no-gutters {\n  margin-right: 0;\n  margin-left: 0; }\n.no-gutters > .col,\n  .no-gutters > [class*=\"col-\"] {\n    padding-right: 0;\n    padding-left: 0; }\n.col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,\n.col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,\n.col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,\n.col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,\n.col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,\n.col-xl-auto {\n  position: relative;\n  width: 100%;\n  min-height: 1px;\n  padding-right: 15px;\n  padding-left: 15px; }\n.col {\n  flex-basis: 0;\n  flex-grow: 1;\n  max-width: 100%; }\n.col-auto {\n  flex: 0 0 auto;\n  width: auto;\n  max-width: none; }\n.col-1 {\n  flex: 0 0 8.33333333%;\n  max-width: 8.33333333%; }\n.col-2 {\n  flex: 0 0 16.66666667%;\n  max-width: 16.66666667%; }\n.col-3 {\n  flex: 0 0 25%;\n  max-width: 25%; }\n.col-4 {\n  flex: 0 0 33.33333333%;\n  max-width: 33.33333333%; }\n.col-5 {\n  flex: 0 0 41.66666667%;\n  max-width: 41.66666667%; }\n.col-6 {\n  flex: 0 0 50%;\n  max-width: 50%; }\n.col-7 {\n  flex: 0 0 58.33333333%;\n  max-width: 58.33333333%; }\n.col-8 {\n  flex: 0 0 66.66666667%;\n  max-width: 66.66666667%; }\n.col-9 {\n  flex: 0 0 75%;\n  max-width: 75%; }\n.col-10 {\n  flex: 0 0 83.33333333%;\n  max-width: 83.33333333%; }\n.col-11 {\n  flex: 0 0 91.66666667%;\n  max-width: 91.66666667%; }\n.col-12 {\n  flex: 0 0 100%;\n  max-width: 100%; }\n.order-first {\n  order: -1; }\n.order-last {\n  order: 13; }\n.order-0 {\n  order: 0; }\n.order-1 {\n  order: 1; }\n.order-2 {\n  order: 2; }\n.order-3 {\n  order: 3; }\n.order-4 {\n  order: 4; }\n.order-5 {\n  order: 5; }\n.order-6 {\n  order: 6; }\n.order-7 {\n  order: 7; }\n.order-8 {\n  order: 8; }\n.order-9 {\n  order: 9; }\n.order-10 {\n  order: 10; }\n.order-11 {\n  order: 11; }\n.order-12 {\n  order: 12; }\n.offset-1 {\n  margin-left: 8.33333333%; }\n.offset-2 {\n  margin-left: 16.66666667%; }\n.offset-3 {\n  margin-left: 25%; }\n.offset-4 {\n  margin-left: 33.33333333%; }\n.offset-5 {\n  margin-left: 41.66666667%; }\n.offset-6 {\n  margin-left: 50%; }\n.offset-7 {\n  margin-left: 58.33333333%; }\n.offset-8 {\n  margin-left: 66.66666667%; }\n.offset-9 {\n  margin-left: 75%; }\n.offset-10 {\n  margin-left: 83.33333333%; }\n.offset-11 {\n  margin-left: 91.66666667%; }\n@media (min-width: 576px) {\n  .col-sm {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-sm-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-sm-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-sm-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-sm-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-sm-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-sm-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-sm-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-sm-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-sm-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-sm-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-sm-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-sm-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-sm-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-sm-first {\n    order: -1; }\n  .order-sm-last {\n    order: 13; }\n  .order-sm-0 {\n    order: 0; }\n  .order-sm-1 {\n    order: 1; }\n  .order-sm-2 {\n    order: 2; }\n  .order-sm-3 {\n    order: 3; }\n  .order-sm-4 {\n    order: 4; }\n  .order-sm-5 {\n    order: 5; }\n  .order-sm-6 {\n    order: 6; }\n  .order-sm-7 {\n    order: 7; }\n  .order-sm-8 {\n    order: 8; }\n  .order-sm-9 {\n    order: 9; }\n  .order-sm-10 {\n    order: 10; }\n  .order-sm-11 {\n    order: 11; }\n  .order-sm-12 {\n    order: 12; }\n  .offset-sm-0 {\n    margin-left: 0; }\n  .offset-sm-1 {\n    margin-left: 8.33333333%; }\n  .offset-sm-2 {\n    margin-left: 16.66666667%; }\n  .offset-sm-3 {\n    margin-left: 25%; }\n  .offset-sm-4 {\n    margin-left: 33.33333333%; }\n  .offset-sm-5 {\n    margin-left: 41.66666667%; }\n  .offset-sm-6 {\n    margin-left: 50%; }\n  .offset-sm-7 {\n    margin-left: 58.33333333%; }\n  .offset-sm-8 {\n    margin-left: 66.66666667%; }\n  .offset-sm-9 {\n    margin-left: 75%; }\n  .offset-sm-10 {\n    margin-left: 83.33333333%; }\n  .offset-sm-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 768px) {\n  .col-md {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-md-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-md-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-md-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-md-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-md-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-md-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-md-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-md-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-md-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-md-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-md-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-md-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-md-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-md-first {\n    order: -1; }\n  .order-md-last {\n    order: 13; }\n  .order-md-0 {\n    order: 0; }\n  .order-md-1 {\n    order: 1; }\n  .order-md-2 {\n    order: 2; }\n  .order-md-3 {\n    order: 3; }\n  .order-md-4 {\n    order: 4; }\n  .order-md-5 {\n    order: 5; }\n  .order-md-6 {\n    order: 6; }\n  .order-md-7 {\n    order: 7; }\n  .order-md-8 {\n    order: 8; }\n  .order-md-9 {\n    order: 9; }\n  .order-md-10 {\n    order: 10; }\n  .order-md-11 {\n    order: 11; }\n  .order-md-12 {\n    order: 12; }\n  .offset-md-0 {\n    margin-left: 0; }\n  .offset-md-1 {\n    margin-left: 8.33333333%; }\n  .offset-md-2 {\n    margin-left: 16.66666667%; }\n  .offset-md-3 {\n    margin-left: 25%; }\n  .offset-md-4 {\n    margin-left: 33.33333333%; }\n  .offset-md-5 {\n    margin-left: 41.66666667%; }\n  .offset-md-6 {\n    margin-left: 50%; }\n  .offset-md-7 {\n    margin-left: 58.33333333%; }\n  .offset-md-8 {\n    margin-left: 66.66666667%; }\n  .offset-md-9 {\n    margin-left: 75%; }\n  .offset-md-10 {\n    margin-left: 83.33333333%; }\n  .offset-md-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 992px) {\n  .col-lg {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-lg-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-lg-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-lg-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-lg-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-lg-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-lg-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-lg-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-lg-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-lg-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-lg-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-lg-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-lg-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-lg-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-lg-first {\n    order: -1; }\n  .order-lg-last {\n    order: 13; }\n  .order-lg-0 {\n    order: 0; }\n  .order-lg-1 {\n    order: 1; }\n  .order-lg-2 {\n    order: 2; }\n  .order-lg-3 {\n    order: 3; }\n  .order-lg-4 {\n    order: 4; }\n  .order-lg-5 {\n    order: 5; }\n  .order-lg-6 {\n    order: 6; }\n  .order-lg-7 {\n    order: 7; }\n  .order-lg-8 {\n    order: 8; }\n  .order-lg-9 {\n    order: 9; }\n  .order-lg-10 {\n    order: 10; }\n  .order-lg-11 {\n    order: 11; }\n  .order-lg-12 {\n    order: 12; }\n  .offset-lg-0 {\n    margin-left: 0; }\n  .offset-lg-1 {\n    margin-left: 8.33333333%; }\n  .offset-lg-2 {\n    margin-left: 16.66666667%; }\n  .offset-lg-3 {\n    margin-left: 25%; }\n  .offset-lg-4 {\n    margin-left: 33.33333333%; }\n  .offset-lg-5 {\n    margin-left: 41.66666667%; }\n  .offset-lg-6 {\n    margin-left: 50%; }\n  .offset-lg-7 {\n    margin-left: 58.33333333%; }\n  .offset-lg-8 {\n    margin-left: 66.66666667%; }\n  .offset-lg-9 {\n    margin-left: 75%; }\n  .offset-lg-10 {\n    margin-left: 83.33333333%; }\n  .offset-lg-11 {\n    margin-left: 91.66666667%; } }\n@media (min-width: 1200px) {\n  .col-xl {\n    flex-basis: 0;\n    flex-grow: 1;\n    max-width: 100%; }\n  .col-xl-auto {\n    flex: 0 0 auto;\n    width: auto;\n    max-width: none; }\n  .col-xl-1 {\n    flex: 0 0 8.33333333%;\n    max-width: 8.33333333%; }\n  .col-xl-2 {\n    flex: 0 0 16.66666667%;\n    max-width: 16.66666667%; }\n  .col-xl-3 {\n    flex: 0 0 25%;\n    max-width: 25%; }\n  .col-xl-4 {\n    flex: 0 0 33.33333333%;\n    max-width: 33.33333333%; }\n  .col-xl-5 {\n    flex: 0 0 41.66666667%;\n    max-width: 41.66666667%; }\n  .col-xl-6 {\n    flex: 0 0 50%;\n    max-width: 50%; }\n  .col-xl-7 {\n    flex: 0 0 58.33333333%;\n    max-width: 58.33333333%; }\n  .col-xl-8 {\n    flex: 0 0 66.66666667%;\n    max-width: 66.66666667%; }\n  .col-xl-9 {\n    flex: 0 0 75%;\n    max-width: 75%; }\n  .col-xl-10 {\n    flex: 0 0 83.33333333%;\n    max-width: 83.33333333%; }\n  .col-xl-11 {\n    flex: 0 0 91.66666667%;\n    max-width: 91.66666667%; }\n  .col-xl-12 {\n    flex: 0 0 100%;\n    max-width: 100%; }\n  .order-xl-first {\n    order: -1; }\n  .order-xl-last {\n    order: 13; }\n  .order-xl-0 {\n    order: 0; }\n  .order-xl-1 {\n    order: 1; }\n  .order-xl-2 {\n    order: 2; }\n  .order-xl-3 {\n    order: 3; }\n  .order-xl-4 {\n    order: 4; }\n  .order-xl-5 {\n    order: 5; }\n  .order-xl-6 {\n    order: 6; }\n  .order-xl-7 {\n    order: 7; }\n  .order-xl-8 {\n    order: 8; }\n  .order-xl-9 {\n    order: 9; }\n  .order-xl-10 {\n    order: 10; }\n  .order-xl-11 {\n    order: 11; }\n  .order-xl-12 {\n    order: 12; }\n  .offset-xl-0 {\n    margin-left: 0; }\n  .offset-xl-1 {\n    margin-left: 8.33333333%; }\n  .offset-xl-2 {\n    margin-left: 16.66666667%; }\n  .offset-xl-3 {\n    margin-left: 25%; }\n  .offset-xl-4 {\n    margin-left: 33.33333333%; }\n  .offset-xl-5 {\n    margin-left: 41.66666667%; }\n  .offset-xl-6 {\n    margin-left: 50%; }\n  .offset-xl-7 {\n    margin-left: 58.33333333%; }\n  .offset-xl-8 {\n    margin-left: 66.66666667%; }\n  .offset-xl-9 {\n    margin-left: 75%; }\n  .offset-xl-10 {\n    margin-left: 83.33333333%; }\n  .offset-xl-11 {\n    margin-left: 91.66666667%; } }\n.d-none {\n  display: none !important; }\n.d-inline {\n  display: inline !important; }\n.d-inline-block {\n  display: inline-block !important; }\n.d-block {\n  display: block !important; }\n.d-table {\n  display: table !important; }\n.d-table-row {\n  display: table-row !important; }\n.d-table-cell {\n  display: table-cell !important; }\n.d-flex {\n  display: flex !important; }\n.d-inline-flex {\n  display: inline-flex !important; }\n@media (min-width: 576px) {\n  .d-sm-none {\n    display: none !important; }\n  .d-sm-inline {\n    display: inline !important; }\n  .d-sm-inline-block {\n    display: inline-block !important; }\n  .d-sm-block {\n    display: block !important; }\n  .d-sm-table {\n    display: table !important; }\n  .d-sm-table-row {\n    display: table-row !important; }\n  .d-sm-table-cell {\n    display: table-cell !important; }\n  .d-sm-flex {\n    display: flex !important; }\n  .d-sm-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 768px) {\n  .d-md-none {\n    display: none !important; }\n  .d-md-inline {\n    display: inline !important; }\n  .d-md-inline-block {\n    display: inline-block !important; }\n  .d-md-block {\n    display: block !important; }\n  .d-md-table {\n    display: table !important; }\n  .d-md-table-row {\n    display: table-row !important; }\n  .d-md-table-cell {\n    display: table-cell !important; }\n  .d-md-flex {\n    display: flex !important; }\n  .d-md-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 992px) {\n  .d-lg-none {\n    display: none !important; }\n  .d-lg-inline {\n    display: inline !important; }\n  .d-lg-inline-block {\n    display: inline-block !important; }\n  .d-lg-block {\n    display: block !important; }\n  .d-lg-table {\n    display: table !important; }\n  .d-lg-table-row {\n    display: table-row !important; }\n  .d-lg-table-cell {\n    display: table-cell !important; }\n  .d-lg-flex {\n    display: flex !important; }\n  .d-lg-inline-flex {\n    display: inline-flex !important; } }\n@media (min-width: 1200px) {\n  .d-xl-none {\n    display: none !important; }\n  .d-xl-inline {\n    display: inline !important; }\n  .d-xl-inline-block {\n    display: inline-block !important; }\n  .d-xl-block {\n    display: block !important; }\n  .d-xl-table {\n    display: table !important; }\n  .d-xl-table-row {\n    display: table-row !important; }\n  .d-xl-table-cell {\n    display: table-cell !important; }\n  .d-xl-flex {\n    display: flex !important; }\n  .d-xl-inline-flex {\n    display: inline-flex !important; } }\n@media print {\n  .d-print-none {\n    display: none !important; }\n  .d-print-inline {\n    display: inline !important; }\n  .d-print-inline-block {\n    display: inline-block !important; }\n  .d-print-block {\n    display: block !important; }\n  .d-print-table {\n    display: table !important; }\n  .d-print-table-row {\n    display: table-row !important; }\n  .d-print-table-cell {\n    display: table-cell !important; }\n  .d-print-flex {\n    display: flex !important; }\n  .d-print-inline-flex {\n    display: inline-flex !important; } }\n.flex-row {\n  flex-direction: row !important; }\n.flex-column {\n  flex-direction: column !important; }\n.flex-row-reverse {\n  flex-direction: row-reverse !important; }\n.flex-column-reverse {\n  flex-direction: column-reverse !important; }\n.flex-wrap {\n  flex-wrap: wrap !important; }\n.flex-nowrap {\n  flex-wrap: nowrap !important; }\n.flex-wrap-reverse {\n  flex-wrap: wrap-reverse !important; }\n.flex-fill {\n  flex: 1 1 auto !important; }\n.flex-grow-0 {\n  flex-grow: 0 !important; }\n.flex-grow-1 {\n  flex-grow: 1 !important; }\n.flex-shrink-0 {\n  flex-shrink: 0 !important; }\n.flex-shrink-1 {\n  flex-shrink: 1 !important; }\n.justify-content-start {\n  justify-content: flex-start !important; }\n.justify-content-end {\n  justify-content: flex-end !important; }\n.justify-content-center {\n  justify-content: center !important; }\n.justify-content-between {\n  justify-content: space-between !important; }\n.justify-content-around {\n  justify-content: space-around !important; }\n.align-items-start {\n  align-items: flex-start !important; }\n.align-items-end {\n  align-items: flex-end !important; }\n.align-items-center {\n  align-items: center !important; }\n.align-items-baseline {\n  align-items: baseline !important; }\n.align-items-stretch {\n  align-items: stretch !important; }\n.align-content-start {\n  align-content: flex-start !important; }\n.align-content-end {\n  align-content: flex-end !important; }\n.align-content-center {\n  align-content: center !important; }\n.align-content-between {\n  align-content: space-between !important; }\n.align-content-around {\n  align-content: space-around !important; }\n.align-content-stretch {\n  align-content: stretch !important; }\n.align-self-auto {\n  -ms-grid-row-align: auto !important;\n      align-self: auto !important; }\n.align-self-start {\n  align-self: flex-start !important; }\n.align-self-end {\n  align-self: flex-end !important; }\n.align-self-center {\n  -ms-grid-row-align: center !important;\n      align-self: center !important; }\n.align-self-baseline {\n  align-self: baseline !important; }\n.align-self-stretch {\n  -ms-grid-row-align: stretch !important;\n      align-self: stretch !important; }\n@media (min-width: 576px) {\n  .flex-sm-row {\n    flex-direction: row !important; }\n  .flex-sm-column {\n    flex-direction: column !important; }\n  .flex-sm-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-sm-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-sm-wrap {\n    flex-wrap: wrap !important; }\n  .flex-sm-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-sm-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-sm-fill {\n    flex: 1 1 auto !important; }\n  .flex-sm-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-sm-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-sm-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-sm-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-sm-start {\n    justify-content: flex-start !important; }\n  .justify-content-sm-end {\n    justify-content: flex-end !important; }\n  .justify-content-sm-center {\n    justify-content: center !important; }\n  .justify-content-sm-between {\n    justify-content: space-between !important; }\n  .justify-content-sm-around {\n    justify-content: space-around !important; }\n  .align-items-sm-start {\n    align-items: flex-start !important; }\n  .align-items-sm-end {\n    align-items: flex-end !important; }\n  .align-items-sm-center {\n    align-items: center !important; }\n  .align-items-sm-baseline {\n    align-items: baseline !important; }\n  .align-items-sm-stretch {\n    align-items: stretch !important; }\n  .align-content-sm-start {\n    align-content: flex-start !important; }\n  .align-content-sm-end {\n    align-content: flex-end !important; }\n  .align-content-sm-center {\n    align-content: center !important; }\n  .align-content-sm-between {\n    align-content: space-between !important; }\n  .align-content-sm-around {\n    align-content: space-around !important; }\n  .align-content-sm-stretch {\n    align-content: stretch !important; }\n  .align-self-sm-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-sm-start {\n    align-self: flex-start !important; }\n  .align-self-sm-end {\n    align-self: flex-end !important; }\n  .align-self-sm-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-sm-baseline {\n    align-self: baseline !important; }\n  .align-self-sm-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 768px) {\n  .flex-md-row {\n    flex-direction: row !important; }\n  .flex-md-column {\n    flex-direction: column !important; }\n  .flex-md-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-md-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-md-wrap {\n    flex-wrap: wrap !important; }\n  .flex-md-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-md-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-md-fill {\n    flex: 1 1 auto !important; }\n  .flex-md-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-md-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-md-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-md-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-md-start {\n    justify-content: flex-start !important; }\n  .justify-content-md-end {\n    justify-content: flex-end !important; }\n  .justify-content-md-center {\n    justify-content: center !important; }\n  .justify-content-md-between {\n    justify-content: space-between !important; }\n  .justify-content-md-around {\n    justify-content: space-around !important; }\n  .align-items-md-start {\n    align-items: flex-start !important; }\n  .align-items-md-end {\n    align-items: flex-end !important; }\n  .align-items-md-center {\n    align-items: center !important; }\n  .align-items-md-baseline {\n    align-items: baseline !important; }\n  .align-items-md-stretch {\n    align-items: stretch !important; }\n  .align-content-md-start {\n    align-content: flex-start !important; }\n  .align-content-md-end {\n    align-content: flex-end !important; }\n  .align-content-md-center {\n    align-content: center !important; }\n  .align-content-md-between {\n    align-content: space-between !important; }\n  .align-content-md-around {\n    align-content: space-around !important; }\n  .align-content-md-stretch {\n    align-content: stretch !important; }\n  .align-self-md-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-md-start {\n    align-self: flex-start !important; }\n  .align-self-md-end {\n    align-self: flex-end !important; }\n  .align-self-md-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-md-baseline {\n    align-self: baseline !important; }\n  .align-self-md-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 992px) {\n  .flex-lg-row {\n    flex-direction: row !important; }\n  .flex-lg-column {\n    flex-direction: column !important; }\n  .flex-lg-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-lg-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-lg-wrap {\n    flex-wrap: wrap !important; }\n  .flex-lg-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-lg-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-lg-fill {\n    flex: 1 1 auto !important; }\n  .flex-lg-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-lg-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-lg-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-lg-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-lg-start {\n    justify-content: flex-start !important; }\n  .justify-content-lg-end {\n    justify-content: flex-end !important; }\n  .justify-content-lg-center {\n    justify-content: center !important; }\n  .justify-content-lg-between {\n    justify-content: space-between !important; }\n  .justify-content-lg-around {\n    justify-content: space-around !important; }\n  .align-items-lg-start {\n    align-items: flex-start !important; }\n  .align-items-lg-end {\n    align-items: flex-end !important; }\n  .align-items-lg-center {\n    align-items: center !important; }\n  .align-items-lg-baseline {\n    align-items: baseline !important; }\n  .align-items-lg-stretch {\n    align-items: stretch !important; }\n  .align-content-lg-start {\n    align-content: flex-start !important; }\n  .align-content-lg-end {\n    align-content: flex-end !important; }\n  .align-content-lg-center {\n    align-content: center !important; }\n  .align-content-lg-between {\n    align-content: space-between !important; }\n  .align-content-lg-around {\n    align-content: space-around !important; }\n  .align-content-lg-stretch {\n    align-content: stretch !important; }\n  .align-self-lg-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-lg-start {\n    align-self: flex-start !important; }\n  .align-self-lg-end {\n    align-self: flex-end !important; }\n  .align-self-lg-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-lg-baseline {\n    align-self: baseline !important; }\n  .align-self-lg-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n@media (min-width: 1200px) {\n  .flex-xl-row {\n    flex-direction: row !important; }\n  .flex-xl-column {\n    flex-direction: column !important; }\n  .flex-xl-row-reverse {\n    flex-direction: row-reverse !important; }\n  .flex-xl-column-reverse {\n    flex-direction: column-reverse !important; }\n  .flex-xl-wrap {\n    flex-wrap: wrap !important; }\n  .flex-xl-nowrap {\n    flex-wrap: nowrap !important; }\n  .flex-xl-wrap-reverse {\n    flex-wrap: wrap-reverse !important; }\n  .flex-xl-fill {\n    flex: 1 1 auto !important; }\n  .flex-xl-grow-0 {\n    flex-grow: 0 !important; }\n  .flex-xl-grow-1 {\n    flex-grow: 1 !important; }\n  .flex-xl-shrink-0 {\n    flex-shrink: 0 !important; }\n  .flex-xl-shrink-1 {\n    flex-shrink: 1 !important; }\n  .justify-content-xl-start {\n    justify-content: flex-start !important; }\n  .justify-content-xl-end {\n    justify-content: flex-end !important; }\n  .justify-content-xl-center {\n    justify-content: center !important; }\n  .justify-content-xl-between {\n    justify-content: space-between !important; }\n  .justify-content-xl-around {\n    justify-content: space-around !important; }\n  .align-items-xl-start {\n    align-items: flex-start !important; }\n  .align-items-xl-end {\n    align-items: flex-end !important; }\n  .align-items-xl-center {\n    align-items: center !important; }\n  .align-items-xl-baseline {\n    align-items: baseline !important; }\n  .align-items-xl-stretch {\n    align-items: stretch !important; }\n  .align-content-xl-start {\n    align-content: flex-start !important; }\n  .align-content-xl-end {\n    align-content: flex-end !important; }\n  .align-content-xl-center {\n    align-content: center !important; }\n  .align-content-xl-between {\n    align-content: space-between !important; }\n  .align-content-xl-around {\n    align-content: space-around !important; }\n  .align-content-xl-stretch {\n    align-content: stretch !important; }\n  .align-self-xl-auto {\n    -ms-grid-row-align: auto !important;\n        align-self: auto !important; }\n  .align-self-xl-start {\n    align-self: flex-start !important; }\n  .align-self-xl-end {\n    align-self: flex-end !important; }\n  .align-self-xl-center {\n    -ms-grid-row-align: center !important;\n        align-self: center !important; }\n  .align-self-xl-baseline {\n    align-self: baseline !important; }\n  .align-self-xl-stretch {\n    -ms-grid-row-align: stretch !important;\n        align-self: stretch !important; } }\n.nb-above-fold {\n  height: 100vh;\n  min-height: 650px;\n  -webkit-background-image: url('bg-homepage-2.jpg');\n  -ms-background-image: url('bg-homepage-2.jpg');\n  -moz-background-image: url('bg-homepage-2.jpg');\n  -o-background-image: url('bg-homepage-2.jpg');\n  background-image: url('bg-homepage-2.jpg');\n  -webkit-background-position: center;\n  -ms-background-position: center;\n  -moz-background-position: center;\n  -o-background-position: center;\n  background-position: center;\n  -ms-background-size: cover;\n  background-size: cover; }\n@media (max-width: 675px) {\n    .nb-above-fold {\n      height: calc(100vh - 50px);\n      min-height: 450px; } }\n.nb-above-fold .nb-color-filter {\n    background-color: rgba(33, 37, 41, 0.2); }\n.nb-above-fold .nb-header {\n    text-align: center;\n    background-color: white;\n    color: #212529;\n    font-size: 38px;\n    letter-spacing: 2px;\n    font-weight: bold;\n    text-transform: uppercase;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    margin: 8px auto;\n    padding: 0 8px; }\n.nb-above-fold .nb-header:first-of-type {\n      -webkit-animation-name: fadeIn;\n              animation-name: fadeIn;\n      -webkit-animation-iteration-count: 1;\n              animation-iteration-count: 1;\n      -webkit-animation-timing-function: ease-in-out;\n              animation-timing-function: ease-in-out;\n      -webkit-animation-fill-mode: backwards;\n              animation-fill-mode: backwards;\n      -webkit-animation-duration: 1s;\n              animation-duration: 1s;\n      -webkit-animation-delay: 0s;\n              animation-delay: 0s; }\n@-webkit-keyframes fadeIn {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n@keyframes fadeIn {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n.nb-above-fold .nb-header:last-of-type {\n      -webkit-animation-name: fadeIn;\n              animation-name: fadeIn;\n      -webkit-animation-iteration-count: 1;\n              animation-iteration-count: 1;\n      -webkit-animation-timing-function: ease-in-out;\n              animation-timing-function: ease-in-out;\n      -webkit-animation-fill-mode: backwards;\n              animation-fill-mode: backwards;\n      -webkit-animation-duration: 1s;\n              animation-duration: 1s;\n      -webkit-animation-delay: 0.4s;\n              animation-delay: 0.4s; }\n@keyframes fadeIn {\n  0% {\n    opacity: 0; }\n  100% {\n    opacity: 1; } }\n@media (max-width: 1199.98px) {\n      .nb-above-fold .nb-header {\n        font-size: 30px; } }\n@media (max-width: 575.98px) {\n      .nb-above-fold .nb-header {\n        font-size: 18px;\n        letter-spacing: 0; } }\n.nb-above-fold .nb-atf-cta {\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    right: 0;\n    margin: 0 auto;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    cursor: pointer;\n    padding-top: 2%;\n    padding-left: 3%;\n    padding-right: 3%;\n    border-top-left-radius: 140px;\n    border-top-right-radius: 140px;\n    background-color: rgba(255, 255, 255, 0);\n    transition: background-color .15s ease-in-out;\n    -webkit-transform: translateY(15px);\n    transform: translateY(15px); }\n.nb-above-fold .nb-atf-cta:hover h6 {\n      opacity: 1; }\n.nb-above-fold .nb-atf-cta:hover svg g path {\n      fill: #3AAFA9; }\n.nb-above-fold .nb-atf-cta h6 {\n      color: #3AAFA9;\n      text-align: center;\n      font-size: 19px;\n      margin-bottom: 30px;\n      opacity: 0;\n      transition: opacity .25s .1s ease-in-out; }\n.nb-above-fold .nb-atf-cta svg {\n      width: 90px;\n      height: 45px;\n      -webkit-transform: scale(2.5);\n              transform: scale(2.5);\n      margin: 0 auto;\n      -webkit-animation: bounce 2s infinite;\n      animation: bounce 2s infinite; }\n@-webkit-keyframes bounce {\n  0%, 20%, 50%, 80%, 100% {\n    -webkit-transform: translateY(0);\n    transform: translateY(0); }\n  40% {\n    -webkit-transform: translateY(-30px);\n    transform: translateY(-30px); }\n  60% {\n    -webkit-transform: translateY(-15px);\n    transform: translateY(-15px); } }\n@keyframes bounce {\n  0%, 20%, 50%, 80%, 100% {\n    -webkit-transform: translateY(0);\n    transform: translateY(0); }\n  40% {\n    -webkit-transform: translateY(-30px);\n    transform: translateY(-30px); }\n  60% {\n    -webkit-transform: translateY(-15px);\n    transform: translateY(-15px); } }\n.nb-above-fold .nb-atf-cta svg g path {\n        fill: rgba(255, 255, 255, 0.6); }\n@media (max-width: 675px) {\n      .nb-above-fold .nb-atf-cta {\n        padding-left: 1%;\n        padding-right: 1%;\n        bottom: 50px; }\n        .nb-above-fold .nb-atf-cta h6 {\n          opacity: 1; }\n        .nb-above-fold .nb-atf-cta svg g path {\n          fill: #3AAFA9; } }\n#nb-how-it-works {\n  background-color: #f6f6f6; }\n#nb-how-it-works .row .nb-intro {\n    position: relative;\n    padding-top: 55px;\n    width: 92%;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    min-height: -webkit-fit-content;\n    min-height: -moz-fit-content;\n    min-height: fit-content;\n    color: white;\n    font-size: 19px;\n    background-color: #f1c40f; }\n@media (max-width: 575.98px) {\n      #nb-how-it-works .row .nb-intro {\n        padding: 50px 0; } }\n#nb-how-it-works .row .nb-intro h3,\n    #nb-how-it-works .row .nb-intro p {\n      margin-left: 15%;\n      width: 45%;\n      text-align: justify; }\n#nb-how-it-works .row .nb-intro::after {\n    content: '';\n    position: absolute;\n    left: 0%;\n    top: 100%;\n    width: 0;\n    height: 0;\n    border-left: 27vw solid transparent;\n    border-right: 71vw solid transparent;\n    border-top: 20vh solid #f1c40f;\n    clear: both; }\n#nb-how-it-works .row .nb-solution-text {\n    width: 100%;\n    font-size: 19px;\n    margin-top: 23vh;\n    margin-bottom: 5%; }\n#nb-how-it-works .row .nb-solution-text h3 {\n      text-align: right; }\n#nb-how-it-works .row .nb-solution-text p {\n      text-align: justify; }\n#nb-how-it-works .row .nb-solution-text h3,\n    #nb-how-it-works .row .nb-solution-text p {\n      margin-left: 53%;\n      width: 35%; }\n@media (max-width: 1199.98px) {\n    #nb-how-it-works .row .nb-intro h3,\n    #nb-how-it-works .row .nb-intro p {\n      width: 55%; }\n    #nb-how-it-works .row .nb-solution-text h3,\n    #nb-how-it-works .row .nb-solution-text p {\n      width: 45%;\n      margin-left: 45%; } }\n@media (max-width: 991.98px) {\n    #nb-how-it-works .row .nb-intro h3,\n    #nb-how-it-works .row .nb-intro p {\n      width: 65%; }\n    #nb-how-it-works .row .nb-solution-text h3,\n    #nb-how-it-works .row .nb-solution-text p {\n      width: 50%;\n      margin-left: 40%; } }\n@media (max-width: 767.98px) {\n    #nb-how-it-works .row .nb-intro::after {\n      border-left: 49vw solid transparent;\n      border-right: 49vw solid transparent;\n      border-top: 7vh solid #f1c40f; }\n    #nb-how-it-works .row .nb-intro h3,\n    #nb-how-it-works .row .nb-intro p {\n      width: 70%;\n      margin-left: auto;\n      margin-right: auto; }\n    #nb-how-it-works .row .nb-solution-text h3,\n    #nb-how-it-works .row .nb-solution-text p {\n      width: 70%;\n      margin-left: auto;\n      margin-right: auto; }\n    #nb-how-it-works .row .nb-solution-text {\n      margin-top: 15vh; }\n      #nb-how-it-works .row .nb-solution-text h3 {\n        text-align: center; } }\n@media (max-width: 575.98px) {\n    #nb-how-it-works .row .nb-intro,\n    #nb-how-it-works .row .nb-solution-text {\n      font-size: 16px; }\n    #nb-how-it-works .row .nb-intro h3,\n    #nb-how-it-works .row .nb-intro p {\n      width: 85%; }\n    #nb-how-it-works .row .nb-solution-text h3,\n    #nb-how-it-works .row .nb-solution-text p {\n      width: 85%; } }\n#nb-how-it-works .row .nb-services .row {\n    margin-bottom: 150px;\n    position: relative; }\n#nb-how-it-works .row .nb-services .row img {\n      width: 155px;\n      -webkit-animation: float 6s ease-in-out infinite;\n      animation: float 6s ease-in-out infinite; }\n@keyframes float {\n  0% {\n    -webkit-transform: translatey(0px);\n            transform: translatey(0px); }\n  50% {\n    -webkit-transform: translatey(-20px);\n            transform: translatey(-20px); }\n  100% {\n    -webkit-transform: translatey(0px);\n            transform: translatey(0px); } }\n@-webkit-keyframes float {\n  0% {\n    -webkit-transform: translatey(0px);\n            transform: translatey(0px); }\n  50% {\n    -webkit-transform: translatey(-20px);\n            transform: translatey(-20px); }\n  100% {\n    -webkit-transform: translatey(0px);\n            transform: translatey(0px); } }\n#nb-how-it-works .row .nb-services .row hr {\n      border: 2px solid #3AAFA9;\n      width: 15%;\n      margin-top: 15px;\n      margin-bottom: 0; }\n#nb-how-it-works .row .nb-services .row .nb-explanation {\n      font-size: 19px;\n      width: 350px;\n      text-align: justify;\n      margin-left: auto;\n      margin-right: auto; }\n#nb-how-it-works .row .nb-services .row:last-of-type {\n      margin-bottom: 0; }\n#nb-how-it-works .row .nb-services .row:last-of-type::after {\n        content: '';\n        position: absolute;\n        width: 0;\n        height: 0;\n        right: 0;\n        bottom: -50%;\n        border-style: solid;\n        border-width: 145px 65px 80px 0;\n        border-color: transparent #f1c40f transparent transparent; }\n#nb-how-it-works .row .nb-services .row:nth-of-type(2)::after {\n      content: '';\n      position: absolute;\n      width: 0;\n      height: 0;\n      left: 0;\n      top: -35%;\n      border-style: solid;\n      border-width: 85px 0 200px 90px;\n      border-color: transparent transparent transparent #f1c40f; }\n@media (max-width: 767.98px) {\n    #nb-how-it-works .row .nb-services .row {\n      margin-bottom: 100px; }\n      #nb-how-it-works .row .nb-services .row:nth-of-type(2)::after {\n        top: 0;\n        border-width: 0 0 0 0; }\n      #nb-how-it-works .row .nb-services .row:last-of-type::after {\n        top: 0;\n        border-width: 0 0 0 0; }\n      #nb-how-it-works .row .nb-services .row img {\n        width: 135px; }\n      #nb-how-it-works .row .nb-services .row hr {\n        margin-bottom: 25px; }\n      #nb-how-it-works .row .nb-services .row .nb-explanation {\n        font-size: 17px;\n        max-width: 350px;\n        width: 85%;\n        text-align: justify; }\n        #nb-how-it-works .row .nb-services .row .nb-explanation h3 {\n          text-align: center; } }\n.nb-cta2-section {\n  width: 100%;\n  height: 80vh;\n  background-color: #f6f6f6;\n  color: white; }\n.nb-cta2-section .row div.col {\n    padding: 4% 0;\n    border-radius: 7px;\n    -webkit-background-image: linear-gradient(135deg, #606c88 0%, #3f4c6b 100%);\n    -ms-background-image: linear-gradient(135deg, #606c88 0%, #3f4c6b 100%);\n    -moz-background-image: linear-gradient(135deg, #606c88 0%, #3f4c6b 100%);\n    -o-background-image: linear-gradient(135deg, #606c88 0%, #3f4c6b 100%);\n    background-image: linear-gradient(135deg, #606c88 0%, #3f4c6b 100%);\n    box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.2), 0 0 0 1px rgba(0, 0, 0, 0.08); }\n.nb-cta2-section .row div.col .nb-btn-cta2 {\n      width: 210px;\n      border-radius: 7px;\n      height: 55px;\n      line-height: 55px;\n      text-align: center;\n      cursor: pointer;\n      box-shadow: 0 1px 5px rgba(0, 0, 0, 0.15);\n      background-color: #f1c40f;\n      color: white;\n      box-shadow: 0 1px 5px rgba(0, 0, 0, 0.15);\n      transition: box-shadow .6s ease-in-out, -webkit-transform .3s ease-in-out;\n      transition: box-shadow .6s ease-in-out, transform .3s ease-in-out;\n      transition: box-shadow .6s ease-in-out, transform .3s ease-in-out, -webkit-transform .3s ease-in-out;\n      margin: 0 auto;\n      text-transform: uppercase;\n      font-size: 18px;\n      font-weight: bold; }\n.nb-cta2-section .row div.col .nb-btn-cta2:hover {\n        box-shadow: -6px 10px 20px rgba(0, 0, 0, 0.2);\n        -webkit-transform: scale(1.05);\n        transform: scale(1.05); }\n@media (max-width: 767.98px) {\n      .nb-cta2-section .row div.col {\n        padding: 16% 0; }\n        .nb-cta2-section .row div.col h4 {\n          font-size: 18px; } }\n"

/***/ }),

/***/ "./src/app/core/homepage/homepage.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/core/homepage/homepage.component.ts ***!
  \*****************************************************/
/*! exports provided: HomepageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageComponent", function() { return HomepageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomepageComponent = /** @class */ (function () {
    function HomepageComponent(routerLink) {
        this.routerLink = routerLink;
        this.easeInOutExpo = {
            ease: function (t, b, c, d) {
                // easeInOutExpo easing
                if (t === 0)
                    return b;
                if (t === d)
                    return b + c;
                if ((t /= d / 2) < 1)
                    return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
                return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
            }
        };
        this.onResize();
    }
    HomepageComponent.prototype.onResize = function (event) {
        if (window.innerWidth <= 500) {
            this.scrollOffset = -15;
        }
        else {
            this.scrollOffset = 40;
        }
    };
    HomepageComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], HomepageComponent.prototype, "onResize", null);
    HomepageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-homepage',
            template: __webpack_require__(/*! ./homepage.component.html */ "./src/app/core/homepage/homepage.component.html"),
            styles: [__webpack_require__(/*! ./homepage.component.scss */ "./src/app/core/homepage/homepage.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HomepageComponent);
    return HomepageComponent;
}());



/***/ }),

/***/ "./src/app/core/homepage/homepage.module.ts":
/*!**************************************************!*\
  !*** ./src/app/core/homepage/homepage.module.ts ***!
  \**************************************************/
/*! exports provided: HomepageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageModule", function() { return HomepageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _homepage_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./homepage.component */ "./src/app/core/homepage/homepage.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var ngx_page_scroll__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-page-scroll */ "./node_modules/ngx-page-scroll/ngx-page-scroll.js");
/* harmony import */ var angular_particle__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-particle */ "./node_modules/angular-particle/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var HomepageModule = /** @class */ (function () {
    function HomepageModule() {
    }
    HomepageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild([{
                        path: '',
                        component: _homepage_component__WEBPACK_IMPORTED_MODULE_2__["HomepageComponent"]
                    }]),
                ngx_page_scroll__WEBPACK_IMPORTED_MODULE_5__["NgxPageScrollModule"],
                angular_particle__WEBPACK_IMPORTED_MODULE_6__["ParticlesModule"]
            ],
            declarations: [_homepage_component__WEBPACK_IMPORTED_MODULE_2__["HomepageComponent"]]
        })
    ], HomepageModule);
    return HomepageModule;
}());



/***/ })

}]);
//# sourceMappingURL=app-core-homepage-homepage-module.js.map