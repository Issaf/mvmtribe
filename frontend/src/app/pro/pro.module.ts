import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProNavbarComponent } from './pro-navbar/pro-navbar.component';
import { ProEditComponent } from './pro-edit/pro-edit.component';
import { ProMainComponent } from './pro-main/pro-main.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProHomepageComponent } from './pro-homepage/pro-homepage.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: 'homepage',
        component: ProHomepageComponent
      },
      // {
      //   path: 'dashboard',
      //   component: ProMainComponent
      //   },
      {
        path: 'edit',
        component: ProEditComponent
        },
    ]),
    FormsModule,
    ReactiveFormsModule,
    ProgressbarModule.forRoot(),
    TypeaheadModule.forRoot(),

  ],
  declarations: [ProNavbarComponent, ProEditComponent, ProMainComponent, ProHomepageComponent]
})
export class ProModule { }
