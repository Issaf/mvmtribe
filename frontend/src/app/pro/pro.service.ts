import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProService {

  constructor(private http: HttpClient) { }

  getSpecialties() {
    return this.http.get('/api/specialists/specialties');
  }
  
  getSpecialistDetails(email) {
    return this.http.get('/api/specialists/' + email);
  }

  updateProfile(email, dict) {
    return this.http.put('/api/specialists/' + email, dict)
  }

  imageApiCall(proId, picture) {
    var imageFormData = new FormData();
    imageFormData.append('picture', picture.files.item(0));
    return this.http.post('api/specialists/img/' + proId, imageFormData);
  }
}
