import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LoggingModalComponent } from '../../shared/logging-modal/logging-modal.component';
import { Router } from '@angular/router';
import { TokenServiceService } from '../../shared/services/token-service.service';

@Component({
  selector: 'app-pro-navbar',
  templateUrl: './pro-navbar.component.html',
  styleUrls: ['./pro-navbar.component.scss']
})
export class ProNavbarComponent implements OnInit {
  isCurrentUser: boolean = false;
  isHomepage: boolean;
  bsModalRef: BsModalRef;

  constructor(private bsModalService: BsModalService, private routerLink: Router, private tokenService: TokenServiceService) { }

  ngOnInit() {
    let urlParams = this.routerLink.url.split('/');
    if(urlParams[urlParams.length - 1] === 'homepage') { this.isHomepage = true; }
    if (this.tokenService.getToken()) {
      this.isCurrentUser = true;
    }
  }

  openLoginModal(userType, toSignupBool) {
    let initialState = {
      data: {
        userType: userType,
        toSignup: toSignupBool
      }
    } 
    this.bsModalRef = this.bsModalService.show(LoggingModalComponent, { initialState, class: 'modal-dialog-centered' });
  }

  logout() {
    this.tokenService.removeToken();
    this.isCurrentUser = false;
    this.routerLink.navigateByUrl('pro/homepage');
  }

}
