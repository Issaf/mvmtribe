import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TokenServiceService } from '../../shared/services/token-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pro-main',
  templateUrl: './pro-main.component.html',
  styleUrls: ['./pro-main.component.scss']
})
export class ProMainComponent implements OnInit, AfterViewInit {
  currentUser: any;
  fname = 'there';
  n1: number = 0;
  n2: number = 0;

  constructor(private tokenService: TokenServiceService, private routerLink: Router) { }

  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.tokenService.decodeToken(this.tokenService.getToken()).subscribe((details) => {
        this.currentUser = details;
        this.fname = this.currentUser.firstName;
      })
    } else {
      this.routerLink.navigateByUrl('pro/homepage');
    }
  }

  ngAfterViewInit() {
    this.counter(0, 221, 'n1'); 
    this.counter(0, 89, 'n2'); 
  }

  counter (i, x, string_n) {          
    let _self = this;
    setTimeout(() => {   
      _self[string_n] = i;
       if (i < x) {
         this.counter(i + 1, x, string_n);
       }
    }, 1200/x)
  }
}
