import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { TokenServiceService } from '../../shared/services/token-service.service';
import { Router } from '@angular/router';
import { ProService } from '../pro.service';
import { NotificationsService } from 'angular2-notifications';
import 'rxjs/Rx';
import { LocationAutocompleteService } from '../../shared/services/location-autocomplete.service';

@Component({
  selector: 'app-pro-edit',
  templateUrl: './pro-edit.component.html',
  styleUrls: ['./pro-edit.component.scss']
})
export class ProEditComponent implements OnInit {
  // Pro data
  currentUser= {};
  profile: FormGroup;

  // Initializing Profile picture Element
  @ViewChild('pictureFile') pictureFile: ElementRef;
  pictureUrl: string = "https://s3.ca-central-1.amazonaws.com/mvmtribe/proProfilePictures/default-user.png"; 
  
  // City field and list initialization
  cityFromAutocomplete = new FormControl();
  resultCities: any;

  // Utils initialization
  currentTab: number = 1;
  formSection: string = "Personal";
  langs = ['english', 'french', 'spanish', 'chinese', 'hindi', 'arabic']
  specialties: any;
  maxChars = 550;
  profileCompletionPercentage = {value: 33.33, text: '1 / 3'};

  validationState = { // initial forced validity to show normal fields
    firstName: false,
    lastName: false,
    email: false,
    phone: false,
    city: false,
    languages: false,
    jobTitle: false,
    mainSpecialty: false,
    institution: false
  };


  constructor(
    private tokenService: TokenServiceService,
    private proService: ProService, 
    private routerLink: Router, 
    private notifs: NotificationsService,
    private citiesService: LocationAutocompleteService
    ) { }


  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.createForm();
      this.tokenService.decodeToken(this.tokenService.getToken()).subscribe((details) => {
        this.currentUser['header'] = details;
        this.proService.getSpecialistDetails(this.currentUser['header']['email']).subscribe((data) => {
          this.currentUser['full'] = data['list'];
          this.fillFromDB();
        })
      })

      this.cityFromAutocomplete.valueChanges
      .debounceTime(400)
      .distinctUntilChanged() 
      .subscribe(term => {
        this.resultCities = this.citiesService.getCities(term);
      });

      this.proService.getSpecialties().subscribe((data) => {
        this.specialties = data['list'];
      });
    } else {
      this.routerLink.navigateByUrl('pro/homepage');
    }

    this.btnText();
  }


  createForm() {
    this.profile = new FormGroup({

      personal: new FormGroup({
        firstName: new FormControl("", Validators.required), 
        lastName: new FormControl("", Validators.required),
        email: new FormControl({value: "", disabled: true}),
        website: new FormControl(""),
        phone: new FormControl("", [Validators.required]),
        city: new FormControl("", [Validators.required]),
        languages: new FormArray([
          new FormControl(false),
          new FormControl(false),
          new FormControl(false),
          new FormControl(false),
          new FormControl(false),
          new FormControl(false)
        ])
      }),

      professional: new FormGroup({
        jobTitle: new FormControl("", Validators.required), 
        mainSpecialty: new FormControl("", Validators.required),
        secondSpecialty: new FormControl(""),
        institution: new FormControl(""),
        flexible: new FormControl("no")
      }),

      bio: new FormControl("")
    });
  }

  fillFromDB() {
    if (this.currentUser['full']) {
      this.profile.controls.personal.patchValue({
        firstName: this.currentUser['full']['firstName'],
        lastName: this.currentUser['full']['lastName'],
        email: this.currentUser['full']['email'],
        website: this.currentUser['full']['website'],
        phone: this.currentUser['full']['phone'],
        city: this.currentUser['full']['city'],
      });
      if (this.currentUser['full']['profilePicURL']) {
        this.pictureUrl = this.currentUser['full']['profilePicURL']
      }
      if (this.currentUser['full']['languages']) { 
        this.profile.controls.personal.patchValue({
          languages: this.langsFormatting(this.currentUser['full']['languages'])
        });
      }
      this.profile.controls.professional.patchValue({
        jobTitle: this.currentUser['full']['jobTitle'],
        mainSpecialty: this.currentUser['full']['mainSpecialty'],
        secondSpecialty: this.currentUser['full']['secondSpecialty'],
        institution: this.currentUser['full']['institution']
      });

      if (this.currentUser['full']['flexible']) {
        this.profile.controls.professional.patchValue({ flexible: 'yes' })
      }
      if (!this.currentUser['full']['flexible']) {
        this.profile.controls.professional.patchValue({ flexible: 'no' })
      }
      this.profile.patchValue({ bio: this.currentUser['full']['bio'] });
    }
  }


  validationCheck() {
    // Checks if INVALID
    var output = false;

    if (this.currentTab === 1) {
      this.validationState.firstName = this.profile.controls.personal['controls'].firstName.invalid;
      this.validationState.lastName = this.profile.controls.personal['controls'].lastName.invalid;
      this.validationState.email = this.profile.controls.personal['controls'].email.invalid;
      this.validationState.phone = this.profile.controls.personal['controls'].phone.invalid;
      this.validationState.city = this.profile.controls.personal['controls'].city.invalid;
      this.validationState.languages = this.langCheck();

      var fields = [
        this.validationState.firstName, 
        this.validationState.lastName, 
        this.validationState.email,
        this.validationState.phone,
        this.validationState.city,
        this.validationState.languages
      ];
      fields.forEach(function(field) {
        if (field === true) { output = true; }  // true means personal part of the form is invalid
      })

    } if (this.currentTab === 2) {
        this.validationState.jobTitle = this.profile.controls.professional['controls'].jobTitle.invalid;
        this.validationState.mainSpecialty = this.profile.controls.professional['controls'].mainSpecialty.invalid;
  
        var fields = [this.validationState.jobTitle, this.validationState.mainSpecialty];
        fields.forEach(function(field) {
          if (field === true) { output = true; }    
        })
      }
    return output
  }


  uploadPicture() {
    if (this.pictureFile.nativeElement.value) {
      this.proService.imageApiCall(this.currentUser['full']['id'], this.pictureFile.nativeElement).subscribe((output) => {
        if (output['type'] === 'error') {
          this.notifs.error(output['msg'], '', {timeOut: 4000});
        }
        else {
          this.notifs.success(output['msg'], '', {timeOut: 4000});
          this.pictureUrl = output['url'];
        }
      });
    }
  }

  save() {
    if (this.currentTab === 3) {
      if (this.fullValidityCheck()) {
        let dict = Object.assign({}, this.profile.value.personal, this.profile.value.professional, {bio: this.profile.controls.bio.value});
        if (dict.flexible === 'yes') { dict.flexible = true }
        if (dict.flexible === 'no') { dict.flexible = false }
        if (!dict.institution) { dict.institution = 'independant' }
        dict.jobTitle = dict.jobTitle.toLowerCase();
        dict['completeProfile'] = true;
        this.proService.updateProfile(this.currentUser['header']['email'], dict).subscribe((data) => {
          this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        });
      } else {
        this.notifs.error('Form is incomplete', 'Please fill out all the required fields', {timeOut: 4000});
      }
    } else {
      if (!this.validationCheck()) {
        this.tabControl('next');
      }
    } 
  }

  // =================
  // UTILITY FUNCTIONS
  // =================

  tabControl(to) {
    if (to === 'back' && this.currentTab !== 1) {
      this.currentTab = this.currentTab - 1;
    }
    if (to === 'next' && this.currentTab !== 3) {
      this.currentTab++;
    }
    window.scrollTo(0,0);
    if (this.currentTab === 1) { this.formSection = "Personal" }
    if (this.currentTab === 2) { this.formSection = "Professional" }
    if (this.currentTab === 3) { this.formSection = "Bio" }
    this.completionPercentage();
  }


  btnText() {
    // add later condition if profile is completed info from DB
    if (this.currentTab === 3) {
      return 'save';
    } else {
      return 'continue';
    }
  }


  langCheck() {
    var bool = false;
    this.profile.controls.personal['controls'].languages.value.forEach(function(element) {
      if (element === true) {
        bool = true;
      }
    })
    // After for loop bool answers the question: Is at least one language selected?
    this.validationState.languages = !bool;
    return this.validationState.languages;
  }

  langsFormatting(list) {
    let temp = []
    list.forEach(element => {
      if (element === 'true') { temp.push(true) }
      if (element === 'false') { temp.push(false) }
    });
    return temp;
  }

  selectedCity(name) {
    this.profile.controls.personal.patchValue({city: name});
    this.cityFromAutocomplete.patchValue('');
  }

  // cityCheck() {
  //   if (!this.cityFromMaps) { 
  //     return true;
  //   } 
  // }

  completionPercentage() {
    if (this.currentTab === 1) {
      this.profileCompletionPercentage.text = '1 / 3';
      this.profileCompletionPercentage.value = 100/3;
    }
    if (this.currentTab === 2) {
      this.profileCompletionPercentage.text = '2 / 3';
      this.profileCompletionPercentage.value = 200/3;
    }
    if (this.currentTab === 3) {
      this.profileCompletionPercentage.text = '3 / 3';
      this.profileCompletionPercentage.value = 300/3;
    }
  }

  fullValidityCheck() { // true => form IS valid
    let list = [
      this.profile.controls.personal['controls'].firstName.invalid,
      this.profile.controls.personal['controls'].lastName.invalid,
      this.profile.controls.personal['controls'].email.invalid,
      this.profile.controls.personal['controls'].phone.invalid,
      this.profile.controls.personal['controls'].city.invalid,
      this.langCheck(),
      this.profile.controls.professional['controls'].jobTitle.invalid,
      this.profile.controls.professional['controls'].mainSpecialty.invalid
    ]
    let bool = true;
    list.forEach(function(elem) {
      if (elem === true) {
        bool = false; 
      }
    })
    return bool;
  }

}