import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { LoggingModalComponent } from '../../shared/logging-modal/logging-modal.component';
import { TokenServiceService } from '../../shared/services/token-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pro-homepage',
  templateUrl: './pro-homepage.component.html',
  styleUrls: ['./pro-homepage.component.scss']
})
export class ProHomepageComponent implements OnInit {
  bsModalRef: BsModalRef;

  constructor(private bsModalService: BsModalService, private tokenService: TokenServiceService, private routerLink: Router) { }

  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.routerLink.navigateByUrl('pro/edit');
    }
  }

  openLoginModal(userType, toSignupBool) {
    let initialState = {
      data: {
        userType: userType,
        toSignup: toSignupBool
      }
    } 
    this.bsModalRef = this.bsModalService.show(LoggingModalComponent, { initialState, class: 'modal-dialog-centered' });
  }
}
