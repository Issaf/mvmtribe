import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { IntroAnimationComponent } from './core/intro-animation/intro-animation.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SpecialistComponent } from './core/specialists/specialist/specialist.component';
import { LoggingModalComponent } from './shared/logging-modal/logging-modal.component';
import { EmailConfirmationModalComponent } from './shared/email-confirmation-modal/email-confirmation-modal.component';
import { SpecialistsModule } from './core/specialists/specialists.module';
import { HttpClientModule } from '@angular/common/http';
import { ShareModalComponent } from './shared/share-modal/share-modal.component';
import { AdminModule } from './admin/admin.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleNotificationsModule } from 'angular2-notifications';

@NgModule({
  declarations: [
    AppComponent,
    IntroAnimationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot(),
    SharedModule,
    SpecialistsModule,
    AdminModule,
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot()
  ],
  exports: [
    ModalModule
  ],
  providers: [],
  entryComponents: [
    SpecialistComponent,
    LoggingModalComponent,
    EmailConfirmationModalComponent,
    ShareModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
