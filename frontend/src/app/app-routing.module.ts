import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroAnimationComponent } from './core/intro-animation/intro-animation.component';
import { CompletionPageComponent } from './shared/completion-page/completion-page.component';
import { ForgotPasswordPageComponent } from './shared/forgot-password-page/forgot-password-page.component';
import { TokenLoginComponent } from './shared/token-login/token-login.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/homepage'
  },
  // {
  //   path: '',
  //   pathMatch: 'full',
  //   component: IntroAnimationComponent
  // },
  {
    path: 'homepage',
    loadChildren: '../app/core/homepage/homepage.module#HomepageModule'
  },
  {
    path: 'about',
    loadChildren: '../app/core/about/about.module#AboutModule'
  },
  {
    path: 'learning',
    loadChildren: '../app/core/learning/learning.module#LearningModule'
  },
  {
    path: 'specialists',
    loadChildren: '../app/core/specialists/specialists.module#SpecialistsModule'
  },
  {
    path: 'tools',
    loadChildren: '../app/core/tools/tools.module#ToolsModule'
  },
  {
    path: 'favourites',
    loadChildren: '../app/core/favourites/favourites.module#FavouritesModule'
  },
  {
    path: 'pro',
    loadChildren: '../app/pro/pro.module#ProModule'
  },
  {
    path: 'admin-settings',
    loadChildren: '../app/admin/admin.module#AdminModule'
  },
  {
    path: 'privacy-policy',
    loadChildren: '../app/core/privacy-page/privacy-page.module#PrivacyPageModule'
  },
  {
    path: 'terms-of-use',
    loadChildren: '../app/core/terms-page/terms-page.module#TermsPageModule'
  },
  {
    path: '404',
    loadChildren: '../app/core/page404/page404.module#Page404Module'
  },
  {
    path: 'test',
    loadChildren: '../app/test/test.module#TestModule'
  },
  {
    path: 'completion-step/:token',
    component: CompletionPageComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordPageComponent
  },
  {
    path: 'login-with-token/:token',
    component: TokenLoginComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
