import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationAutocompleteService {

  constructor(private http: HttpClient) { }

  getCities(typedQuery) {
    if (typedQuery.length > 0) {
      let options = {
        apiKey: 'pk.eyJ1IjoibXZtdHJpYmUiLCJhIjoiY2psdzd1OHVqMTQxaDNxcXBld25rcHE1cCJ9.JarOtULpmObDeceE3DnyJw',
        types: 'place',
        limit: '5'
      }
      let url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' +
                typedQuery + '.json?' + 'access_token=' + options.apiKey +
                '&autocomplete=true&types=' + options.types + '&limit=' + options.limit; 
      let cities = [];
      this.http.get(url).subscribe((res) => {
        res['features'].forEach(element => {
          cities.push(element['place_name']);
        });
      })
      return cities;
    }
  }

}


//
// https://api.mapbox.com/geocoding/v5/mapbox.places/Chester.json?access_token=pk.eyJ1IjoibXZtdHJpYmUiLCJhIjoiY2psdzd1OHVqMTQxaDNxcXBld25rcHE1cCJ9.JarOtULpmObDeceE3DnyJw&types=place&autocomplete=true&limit=5