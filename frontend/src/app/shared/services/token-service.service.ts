import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenServiceService {

  constructor(private http: HttpClient) { }

  setToken(token) {
    localStorage.setItem('loggedInUserToken', token);
  }

  getToken() {
    return localStorage.getItem('loggedInUserToken');
  }

  removeToken() {
    return localStorage.removeItem('loggedInUserToken');
  }

  decodeToken(token) {
    return this.http.post('/api/users/decodeToken', {token: token});
  }

}
