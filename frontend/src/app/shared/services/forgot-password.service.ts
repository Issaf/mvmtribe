import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(private http: HttpClient) { }

  resetEmail(email) {
    return this.http.post('api/users/reset-password', {email});
  }

  decodeToken(token) {
    return this.http.post('api/users/decodeToken', {token});
  }

  updatePassword(dict) {
    return this.http.patch('api/users/updatePassword', dict);
  }
}
