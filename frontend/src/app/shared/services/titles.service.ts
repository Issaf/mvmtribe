import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TitlesService {

  constructor() { }


  titleProcessing(url) {
    let urlParams = url.split('/');
    // urlParams[0] is always ""
    if (urlParams.length === 2) {
      if (urlParams[1] === 'homepage') {
        return 'MVMTRIBE';
      } else if (urlParams[1] === 'tools') {
        return 'Tools';
      } else if (urlParams[1] === 'privacy-policy') {
        return 'Privacy Policy';
      } else if (urlParams[1] === 'privacy-policy') {
        return 'Privacy Policy';
      } else if (urlParams[1] === 'terms-of-use') {
        return 'Terms of use';
      } else if (urlParams[1] === '404') {
        return 'Oops...';
      } else if (urlParams[1] === 'forgot-password') {
        return 'Forgot password';
      } else if (urlParams[1].match(/^about$/)) {
        return 'About us';
      } else if (urlParams[1].match(/^about;isContact/)) {
        return 'Contact us';
      } else if (urlParams[1] === 'learning') {
        return 'Learning';
      } else if (urlParams[1] === 'specialists') {
        return 'Specialists';
      } else {
        return 'MVMTRIBE';
      }  

    } else if (urlParams.length === 3) {
        if (urlParams[1] === 'completion-step') {
          if (urlParams[2] === 'reset-password') {
            return 'Password reset';
          } else {
            return 'Email confirmation';
          }
        } else if (urlParams[1] === 'pro') {
          if (urlParams[2] === 'homepage') {
            return 'MVMTRIBE Pro';
          } else if (urlParams[2] === 'edit') {
            return 'Profile';
          }
        } else if (urlParams[1] === 'learning') {
          let category = urlParams[2].replace('-', ' ');
          return category.charAt(0).toUpperCase() + category.slice(1);
        } else if (urlParams[1] === 'specialists') {
          if (urlParams[2].match(/^search/)) {
            return 'Specialists search';
          } else if (urlParams[2].match(/^individual/)) {
              return 'Specialist';
          }
        } else {
          return 'MVMTRIBE';
        }

    } else if (urlParams.length === 4) {
        if (urlParams[1] === 'learning') {
          let article = urlParams[3].replace(/%20/g, ' ');
          return article.charAt(0).toUpperCase() + article.slice(1);
        } else if (urlParams[1] === 'specialists') {
          return 'Specialist';
        } else {
          return 'MVMTRIBE';
        }    
    }

  }
}
