import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompletionPageService {

  constructor(private http: HttpClient) { }

  decodeToken(token) {
    return this.http.post('api/users/decodeToken', {token});
  }

  updateVerification (email) {
    return this.http.patch('api/users/updateVerification', {email: email});
  }
}
