import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ForgotPasswordService } from '../services/forgot-password.service';
import { EmailConfirmationModalComponent } from '../email-confirmation-modal/email-confirmation-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-forgot-password-page',
  templateUrl: './forgot-password-page.component.html',
  styleUrls: ['./forgot-password-page.component.scss']
})
export class ForgotPasswordPageComponent implements OnInit {
  confirmModal: BsModalRef;
  atStep: number = 1;
  step1Form: FormGroup;
  step2Form: FormGroup;
  userDict = {email: '', password: ''};
  validationState = {
    emailEntered: false,
    password: false,
    passwordConfirmation: false
  }

  constructor(
    private routerLink: Router,
    private route: ActivatedRoute,
    private resetService: ForgotPasswordService,
    private bsModalService: BsModalService,
    private notifs: NotificationsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['token']) {
        this.resetService.decodeToken(params['token']).subscribe((res) => {
          if (res['email']) {
            this.userDict.email = res['email'];
            this.atStep = 2;
            this.createForm();
          } else {
            this.routerLink.navigate(['forgot-password']);
            this.notifs.error('Something wrong with the rest link', 'Please try again', {timeOut: 4000, animate: "fromBottom"});
          }
        })
      } else {
        this.createForm();
      }
    });
  }

  createForm() {
    if (this.atStep === 1) {
      this.step1Form = new FormGroup({
        email: new FormControl("", [Validators.required, Validators.email]),
      });
    }
    if (this.atStep === 2) {
      this.step2Form = new FormGroup({
        password: new FormControl("", [Validators.required, Validators.minLength(8)]),
        passwordConfirmation: new FormControl("", [Validators.required])
      });
    }
  }

  validationCheck() {
    // Checks if INVALID
    if (this.atStep === 1) {
    this.validationState.emailEntered = this.step1Form.controls.email.invalid;
    }
    if (this.atStep === 2) {
      this.validationState.password = this.step2Form.controls.password.invalid;
      this.validationState.passwordConfirmation = !this.passwordMatch();
    }
  }

  passwordMatch() {
    if (this.step2Form.controls.password.value && this.step2Form.controls.passwordConfirmation.value) {
      return this.step2Form.controls.password.value === this.step2Form.controls.passwordConfirmation.value;
    }
  }

  send() {
    this.validationCheck();
    if (!this.validationState.emailEntered) {
      this.resetService.resetEmail(this.step1Form.controls.email.value).subscribe((res) => {
        if (res['type'] === 'success') {
          var initialState = {type: 'passwordResetEmail'}
          this.confirmModal = this.bsModalService.show(EmailConfirmationModalComponent, { initialState, class: 'modal-dialog-centered' });
        } 
        else if (res['type'] === 'error') {
          this.notifs.error(res['msg'], 'Please try again', {timeOut: 4000, animate: "fromBottom"});
        } else {
          this.notifs.error('Something went wrong..', 'Please try again', {timeOut: 4000, animate: "fromBottom"});
        }
      });
    }
  }

  reset() {
    this.validationCheck();
    if (!this.validationState.password && !this.validationState.passwordConfirmation ) {
      this.userDict.password = this.step2Form.controls.password.value;
      this.resetService.updatePassword(this.userDict).subscribe((res) => {
        if (res['msg'] === 'success') {
          this.routerLink.navigate(['completion-step', 'reset-password'])
        } else {
          this.notifs.error('Something went wrong..', 'Please try again', {timeOut: 4000, animate: "fromBottom"});
        }
      });
    }
  }

}
