import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { EmailConfirmationModalComponent } from '../email-confirmation-modal/email-confirmation-modal.component';
import { Router } from '@angular/router';
import { LoggingModalService } from './logging-modal.service';
import { TokenServiceService } from '../services/token-service.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-logging-modal',
  templateUrl: './logging-modal.component.html',
  styleUrls: ['./logging-modal.component.scss']
})
export class LoggingModalComponent implements OnInit {
  confirmModal: BsModalRef;
  data: any;
  loginForm: FormGroup;
  signupForm: FormGroup;
  stateControl = {
    toSignup: true,
    userType: 'pro'
  };

  validationState = { // initial forced validity to show normal fields
    firstName: false,
    email: false,
    password: false,
    passwordConfirmation: false
  };
  authOption = "form"; // Set to 'unselected' when social login is done

  constructor(public bsModalRef: BsModalRef, 
              private bsModalService: BsModalService,
              private routerLink: Router,
              private loggingModalService: LoggingModalService,
              private tokenService: TokenServiceService,
              private notifs: NotificationsService
            ) { }

            
  ngOnInit() {
    this.stateControl.toSignup = this.data.toSignup;
    this.stateControl.userType = this.data.userType;
    this.createForms();    
  }


  createForms() {
    this.signupForm = new FormGroup({
      firstName: new FormControl("", Validators.required),
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.minLength(8)]),
      passwordConfirmation: new FormControl("", [Validators.required, Validators.minLength(8)])
    });
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.minLength(8)])
    });
    }


  switchPortal(type) {
    if (type === "login") {
      this.stateControl.toSignup = false;
      // uncomment when social login is done
      // this.authOption = "unselected";
      this.signupForm.reset();
    }
    if (type === "signup") {
      this.stateControl.toSignup = true;
      // uncomment when social login is done
      // this.authOption = "unselected";
      this.loginForm.reset();
    }
  }


  validationCheck(form) {
    // Checks if INVALID
    if (form === "signup") {
    this.validationState.firstName = this.signupForm.controls.firstName.invalid;
    this.validationState.email = this.signupForm.controls.email.invalid;
    this.validationState.password = this.signupForm.controls.password.invalid;
    this.validationState.passwordConfirmation = (this.signupForm.controls.password.value !== this.signupForm.controls.passwordConfirmation.value) || this.signupForm.controls.passwordConfirmation.invalid;
    }
    if (form === "login") {
      this.validationState.email = this.loginForm.controls.email.invalid;
      this.validationState.password = this.loginForm.controls.password.invalid;
    }
  }


  signup() {
    if (this.signupForm.invalid) {
      this.validationCheck("signup");
    } else {
    let infos = JSON.parse(JSON.stringify(this.signupForm.value));
    infos['userType'] = this.stateControl.userType;
    this.loggingModalService.addNewUser(infos)
    .subscribe((data) => {
      if (data["type"] === "success") {
        this.bsModalRef.hide();
        var initialState = {type: 'firstEmailVerification'}
        this.confirmModal = this.bsModalService.show(EmailConfirmationModalComponent, { initialState, class: 'modal-dialog-centered' });
      } else if (data["type"] === "error") {
        this.notifs.error(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
      } else {
        this.notifs.error('Ahh something went wrong...', '', {timeOut: 4000, animate: "fromBottom"});
      }
    });
    }
  }


  login() {
    if (this.loginForm.invalid) {
      this.validationCheck("login");
    } else {
    let infos = JSON.parse(JSON.stringify(this.loginForm.value));
    infos['userType'] = this.stateControl.userType;
    this.loggingModalService.verifyCreds(infos).subscribe((data) => {
      if (data["type"] === "success") {
        this.tokenService.setToken(data["token"]);
        this.bsModalRef.hide();
        this.routerLink.navigateByUrl('pro/edit');
      
      } else if (data["type"] === "needConfirmation") {
        this.bsModalRef.hide();
        var initialState = {type: 'anotherEmailVerification'}
        this.confirmModal = this.bsModalService.show(EmailConfirmationModalComponent, { initialState, class: 'modal-dialog-centered' });

      } else if (data["type"] === "error") {
        this.notifs.error(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
      
      } else {
        this.notifs.error('Eeeks... Something went wrong. Please try again', '', {timeOut: 4000, animate: "fromBottom"});
      }
    });
  }
  }


  resetpw() {
    this.bsModalRef.hide();
    this.routerLink.navigate(['forgot-password']);
  }
}
