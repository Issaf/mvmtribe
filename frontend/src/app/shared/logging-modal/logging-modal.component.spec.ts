import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggingModalComponent } from './logging-modal.component';

describe('LoggingModalComponent', () => {
  let component: LoggingModalComponent;
  let fixture: ComponentFixture<LoggingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoggingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
