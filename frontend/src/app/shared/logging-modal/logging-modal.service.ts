import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoggingModalService {

  constructor(private http: HttpClient) { }

  addNewUser(data) {
    return this.http.post('/api/users/signup', data);
  }

  verifyCreds(data) {
    return this.http.post('/api/users/login', data);
  }

}
