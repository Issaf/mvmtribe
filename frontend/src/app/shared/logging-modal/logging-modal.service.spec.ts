import { TestBed, inject } from '@angular/core/testing';

import { LoggingModalService } from './logging-modal.service';

describe('LoggingModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggingModalService]
    });
  });

  it('should be created', inject([LoggingModalService], (service: LoggingModalService) => {
    expect(service).toBeTruthy();
  }));
});
