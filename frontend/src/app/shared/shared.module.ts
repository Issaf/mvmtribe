import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TopnavbarComponent } from './topnavbar/topnavbar.component';
import { LoggingModalComponent } from './logging-modal/logging-modal.component';
import { EmailConfirmationModalComponent } from './email-confirmation-modal/email-confirmation-modal.component';
import { CompletionPageComponent } from './completion-page/completion-page.component';
import { ForgotPasswordPageComponent } from './forgot-password-page/forgot-password-page.component';
import { FooterComponent } from './footer/footer.component';
import { MobileNavbarComponent } from './mobile-navbar/mobile-navbar.component';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import { ShareModalComponent } from './share-modal/share-modal.component';
import { TokenLoginComponent } from './token-login/token-login.component';
import { ShareModule } from '@ngx-share/core';
import { RouterModule } from '@angular/router';

const components = [
  TopnavbarComponent,
  LoggingModalComponent,
  CompletionPageComponent,
  EmailConfirmationModalComponent,
  ForgotPasswordPageComponent,
  FooterComponent,
  MobileNavbarComponent,
  ShareModalComponent,
  TokenLoginComponent
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    NgxPageScrollModule,
    ShareModule.forRoot()
  ],
  declarations: [
    ...components
  ],
  exports: [
    ...components
  ]
})
export class SharedModule { }
