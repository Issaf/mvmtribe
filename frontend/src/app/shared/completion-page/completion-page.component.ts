import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LoggingModalComponent } from '../logging-modal/logging-modal.component';
import { Router } from '@angular/router';
import { CompletionPageService } from '../services/completion-page.service';

@Component({
  selector: 'app-completion-page',
  templateUrl: './completion-page.component.html',
  styleUrls: ['./completion-page.component.scss']
})
export class CompletionPageComponent implements OnInit {
  bsModalRef: BsModalRef;
  selected: any;
  
  token: string;

  // =========
  // Templates
  // =========
  templates = {
    confirmEmailSuccess:{
      header: "You're all set!",
      msg: "Thanks for confirming your email. You can now login and complete your profile."
    },
    confirmEmailError: {
      header: "Url error",
      msg: "Seems like the url is no longer valid. Just login and get automatically a new validation link sent to you by email."
    },
    confirmEmailErrorPatch: {
      header: "Internal Error",
      msg: "Please try again or just login and get automatically a new validation link sent to you by email."
    },
    confirmPasswordReset: {
      header: "Done!",
      msg: "Your password has been successfully reset."
    }
  }

  constructor(private bsModalService: BsModalService, private routerLink: Router, private localService: CompletionPageService) { }

  ngOnInit() {
    let urlPArams = this.routerLink.url.split('/');
    this.token = urlPArams[urlPArams.indexOf('completion-step') + 1];
    if (this.token === 'reset-password') {
      this.selected = 'confirmPasswordReset';
    } else {
      this.localService.decodeToken(this.token).subscribe((res) => {
        if (res['msg'] === 'error') {
          this.selected = 'confirmEmailError';
        } else {
          this.localService.updateVerification(res['email']).subscribe((res2) => {
            if (res['msg'] === 'error') {
              this.selected = 'confirmEmailErrorPatch';
            } else {
              this.selected = 'confirmEmailSuccess';
          }
        })};
      })
    }
  }

  openLoginModal(userType, toSignupBool) {
    let initialState = {
      data: {
        userType: userType,
        toSignup: toSignupBool
      }
    } 
    this.bsModalRef = this.bsModalService.show(LoggingModalComponent, { initialState, class: 'modal-dialog-centered' });
  }

}
