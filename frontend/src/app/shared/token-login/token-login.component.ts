import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenServiceService } from '../services/token-service.service';

@Component({
  selector: 'app-token-login',
  templateUrl: './token-login.component.html',
  styleUrls: ['./token-login.component.scss']
})
export class TokenLoginComponent implements OnInit {

  constructor(private route: ActivatedRoute, private routerLink: Router, private tokenService: TokenServiceService ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const token = params['token'];
      if (!token) {
        this.routerLink.navigate(['pro','homepage']);
      }
      else {
        this.tokenService.setToken(token);
        this.routerLink.navigate(['pro','dashboard']);
      }
    });
  }

}
