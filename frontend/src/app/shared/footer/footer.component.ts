import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { LoggingModalComponent } from '../logging-modal/logging-modal.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  bsModalRef: BsModalRef;

  constructor(private routerLink: Router, private bsModalService: BsModalService) { }

  ngOnInit() {
  }

  openLoginModal(isMemberBool, toSignupBool) {
    let initialState = {
      data: {
        toSignup: toSignupBool,
        isMember: isMemberBool
      }
    } 
    this.bsModalRef = this.bsModalService.show(LoggingModalComponent, { initialState, class: 'modal-dialog-centered' });
  }

}
