import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email-confirmation-modal',
  templateUrl: './email-confirmation-modal.component.html',
  styleUrls: ['./email-confirmation-modal.component.scss']
})
export class EmailConfirmationModalComponent implements OnInit {
  selected: any;
  firstEmailVerification = {
    header: "Welcome to the tribe!",
    msg: `An email has been sent to confirm your email address.
    I know it's super annoying but it is to avoid spammers and keep the website secure :)`
  }
  anotherEmailVerification = {
    header: "So close..",
    msg: `You have one small step left and you are good to go. Just go to your inbox and confirm your email.
    for your convenience, a new email was sent right now.`
  }
  passwordResetEmail = {
    header: "Sent!",
    msg: "Check your emails to reset your password."
  }

  constructor(private routerLink: Router, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.selected = this[this['type']];
  }

  closeAndReRoute(type) {
    this.bsModalRef.hide();
  }

}
