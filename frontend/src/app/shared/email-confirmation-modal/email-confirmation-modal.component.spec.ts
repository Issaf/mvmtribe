import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailConfirmationModalComponent } from './email-confirmation-modal.component';

describe('EmailConfirmationModalComponent', () => {
  let component: EmailConfirmationModalComponent;
  let fixture: ComponentFixture<EmailConfirmationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailConfirmationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailConfirmationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
