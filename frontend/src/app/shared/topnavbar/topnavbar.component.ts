import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { LoggingModalComponent } from '../logging-modal/logging-modal.component';
import { TokenServiceService } from '../services/token-service.service';

@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  styleUrls: ['./topnavbar.component.scss']
})
export class TopnavbarComponent implements OnInit {
  @HostListener('window:scroll', ['$event'])
  animateNavbar() {
    if (this.routerLink.url === "/homepage") {
      if (window.scrollY < 100) {this.isBgFilled = false; } 
      else { this.isBgFilled = true; }
    } else {
      this.isBgFilled = true;
    }
  }
  isBgFilled: boolean;
  bsModalRef: BsModalRef;


  constructor(private routerLink: Router, private bsModalService: BsModalService, private tokenService: TokenServiceService) {
    this.animateNavbar();
  }

  ngOnInit() {
    
  }

  activePage(key) {
    if (this.routerLink.url.match(/^\/about/) && key === 'about') {
      return true;
    }
    else if (this.routerLink.url.match(/^\/learning/) && key === 'learning') {
      return true;
    }
    else if (this.routerLink.url.match(/^\/specialist/) && key === 'specialist') {
      return true;
    }
    else if (this.routerLink.url.match(/^\/tools/) && key === 'tools') {
      return true;
    }
    else {
      return false;
    }
  }

  navigationAway(destination) {
    return ['/' + destination];
  }
}
