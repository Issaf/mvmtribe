import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { EasingLogic } from 'ngx-page-scroll';
import { ShareModalComponent } from '../share-modal/share-modal.component';

@Component({
  selector: 'app-mobile-navbar',
  templateUrl: './mobile-navbar.component.html',
  styleUrls: ['./mobile-navbar.component.scss']
})
export class MobileNavbarComponent implements OnInit {
  bsModalRef: BsModalRef;
  isMenuOpen: boolean = false;
  isNavbarShown: boolean = false;
  currentUser: boolean = true;
  constructor(private routerLink: Router, private bsModalService: BsModalService) { }

  ngOnInit() {
    this.showNavbar();
  }

  activePage(key) {
    if (this.routerLink.url.match(/^\/learning/) && key === 'learning') {
      return true;
    }
    else if (this.routerLink.url.match(/^\/specialist/) && key === 'specialist') {
      return true;
    }
    else if (this.routerLink.url.match(/^\/tools/) && key === 'tools') {
      return true;
    }
    else {
      return false;
    }
  }

  navFromMenu(destination) {
    return ['/' + destination];
  }

  showNavbar() {
    if (this.routerLink.url.match(/^\/learning\/.+\/.+/)) {
      this.isNavbarShown = true;
    } 
    else {
      this.isNavbarShown = false;
    }
  }

  backToCategories() {
    if (this.isNavbarShown) {
      let urlParams = this.routerLink.url.split('/');
      return ['/' + urlParams[1], urlParams[2]];
    }
  }

  openSocialsModal() {
    var initialState = {type: 'article', id: '1'}
    this.bsModalRef = this.bsModalService.show(ShareModalComponent, { initialState, class: 'modal-dialog-centered' });
}

  easeInOutExpo: EasingLogic = {
    ease: (t: number, b: number, c: number, d: number): number => {
      // easeInOutExpo easing
      if (t === 0) return b;
      if (t === d) return b + c;
      if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
      return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
  };
}
