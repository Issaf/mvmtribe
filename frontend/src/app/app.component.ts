import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { LoaderService } from './shared/services/loader.service';
import { Title } from '@angular/platform-browser';
import 'rxjs/add/operator/filter';
import { TitlesService } from './shared/services/titles.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'app';
  public showLoader: boolean = true;
  showCookiesMsg: boolean = false;
  
  constructor(private routerLink: Router, private loaderService: LoaderService, private titleService: Title, private titleFunction: TitlesService) { }
  
  ngOnInit() {
    this.loaderService.status.subscribe((val: boolean) => {
      setTimeout(() => this.showLoader = val, 0);
    });

    this.routerLink.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0);
    });


    this.routerLink.events
    .filter((event) => event instanceof NavigationEnd)
    .subscribe((event) => {
      let title = this.titleFunction.titleProcessing(event['url']);
      this.titleService.setTitle(title);
    })

  // // UNCOMMENT TO SHOW COOKIES DISCLAIMER MSG
  //   var that = this;
  //   setTimeout(function(){ that.showCookiesMsg = true; }, 10000);
  }
}
