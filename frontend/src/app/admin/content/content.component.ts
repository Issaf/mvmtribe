import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AdminService } from '../admin.service';
import { NotificationsService } from 'angular2-notifications';
import { TokenServiceService } from '../../shared/services/token-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  selectedOption: string = 'none';
  items: any;

  // Specialty specific
  addNewClicked: boolean = false;
  editBtnClicked: boolean = false;
  speNewValue = '';
  editSpe = {
    oldValue: '',
    newValue: ''
  }; 

  // Category specific
  categoryStateControl: string = 'none';
  categoryTempId: any;
  categoryElement = {name: '', description: ''};

  // Tools specific
  toolFormType: string = 'none';
  tempId: any;
  @ViewChild('pictureFile') pictureFile: ElementRef;
  toolPlaceholder = {
    name: '',
    price: 0,
    pictureURL: '',
    amazonURL: '',
    isOwnBrand: false,
    isNew: false,
    isOnSale: false
  };

  // Specialists search specific
  proEmail = '';
  proFound: any;

  // Article specific
  categories = [];
  articleStateControl: string = 'none';
  searchBy: string;
  currentArticleId: any;
  articleElement = {
    title: '',
    category: '',
    bgImgUrl: '',
    contentFileUrl: '',
    readTime: '',
    difficultyLevel: ''
  }

  constructor(private adminService: AdminService, private notifs: NotificationsService, private tokenService: TokenServiceService, private routerLink: Router) { }

  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.tokenService.decodeToken(this.tokenService.getToken()).subscribe((res) => {
        
        if (!res['msg']) {
          let t_now = new Date();
          let t_exp = new Date(res['exp']);
          
          if (t_exp <= t_now) {
            this.tokenService.removeToken();
            this.routerLink.navigate(['admin-settings']);
          } else {}
      
        } else {
            this.routerLink.navigate(['admin-settings']);
          }
    })
  } else {
      this.routerLink.navigate(['admin-settings']);
  }   
  }

  // =================
  // GENERAL FUNCTIONS
  // =================
  selected(option) {
    this.selectedOption = option;
    if (this.selectedOption === 'specialties' || this.selectedOption === 'blog categories') {
      this.adminService.getList(this.selectedOption).subscribe((data) => {
        this.items = data['list'];
      });
    }
    if (this.selectedOption === 'tools') {
      this.adminService.getProdutsDict().subscribe((data) => {
        this.items = data['list'];
      });
    }
    if (this.selectedOption === 'specialists') {
      this.selectedOption = option;
    }
    if (this.selectedOption === 'articles') {
      this.selectedOption = option;
      this.adminService.getList('blog categories').subscribe((res) => {
        res['list'].forEach(element => {
          this.categories.push(element.name);
        });
      })
    }
  }


  goBackToMenu() {
    this.selectedOption = 'none';
    this.items = [];
    this.addNewClicked = false;
    this.editBtnClicked= false;
    this.editSpe.oldValue= '';
    this.editSpe.newValue= '';
    this.categoryStateControl = 'none';
    this.toolFormType = 'none';
    this.categories = [];
    this.articleStateControl = 'none';
    this.searchBy = '';
    this.currentArticleId = '';  
  }


  logout() {
    this.tokenService.removeToken();
    this.routerLink.navigate(['admin-settings']);
  }

  // ======================
  // SPECIALTIES MANAGEMENT
  // ======================
  addNewEntry() {
    this.adminService.createInstance(this.selectedOption, {name: this.speNewValue}).subscribe((data) => {
      this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
    });
    this.items.push(this.speNewValue);
    this.speNewValue = '';
    this.addNewClicked = false;
  }

  editEntry(step, val) {
    if (step == 1) {
      this.editBtnClicked = true;
      this.editSpe.oldValue = val;
      this.editSpe.newValue = val;
    }
    if (step == 2) {
      this.adminService.updateInstance(this.selectedOption, {old: this.editSpe.oldValue, new: this.editSpe.newValue}).subscribe((data) => {
        this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
      });
      this.items[this.items.indexOf(this.editSpe.oldValue)] = this.editSpe.newValue;
      this.editBtnClicked = false;
      this.editSpe.oldValue = '';
      this.editSpe.newValue = '';

    }
    
  }

  deleteEntry(val) {
    this.adminService.deleteInstance(this.selectedOption, val).subscribe((data) => {
      this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
    });
    this.goBackToMenu();
  }
  // =====================
  // CATEGORIES MANAGEMENT
  // =====================
  categoryCRUD(type, obj) {
    if (type === 'delete') {
      this.adminService.deleteInstance('blog categories', obj.name).subscribe((data) => {
        this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
      });
      this.goBackToMenu();
    } else {
      if (this.categoryStateControl === 'new') {
        this.adminService.createInstance('blog categories', this.categoryElement).subscribe((data) => {
          this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        });
        this.categoryElement.name = '';
        this.categoryElement.description = '';
        this.categoryStateControl = 'none';
        this.goBackToMenu();
        };
      }
      if (this.categoryStateControl === 'edit') {
        this.adminService.updateInstance('blog categories', Object.assign({id: this.categoryTempId}, this.categoryElement)).subscribe((data) => {
          this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        });       
         this.categoryElement.name = '';
         this.categoryElement.description = '';
        this.categoryStateControl = 'none';
        this.goBackToMenu();
      }
  }

  categoryAutocomplete(obj) {
    this.categoryStateControl = 'edit';
    this.categoryTempId = obj.id;
    this.categoryElement.name = obj.name;
    this.categoryElement.description = obj.description;
  }


  // ================
  // TOOLS MANAGEMENT
  // ================
  processForm(type, obj) {
    if (type === 'delete') {
      this.adminService.deleteProductInstance(obj).subscribe((data) => {
        this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
      });
      this.goBackToMenu();
    } else {
      if (this.toolFormType === 'create') {
        this.adminService.createProductInstance(this.toolPlaceholder).subscribe((data) => {
          this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        });
        this.toolPlaceholder.name = '';
        this.toolPlaceholder.price = 0;
        this.toolPlaceholder.pictureURL = '';
        this.toolPlaceholder.amazonURL = '';
        this.toolPlaceholder.isOwnBrand = false;
        this.toolPlaceholder.isNew = false;
        this.toolPlaceholder.isOnSale = false;
        this.toolFormType = 'none';
        this.goBackToMenu();
        };
      }
      if (this.toolFormType === 'edit') {
        this.adminService.editProductInstance(this.tempId, this.toolPlaceholder).subscribe((data) => {
          this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        });
        this.toolPlaceholder.name = '';
        this.toolPlaceholder.price = 0;
        this.toolPlaceholder.pictureURL = '';
        this.toolPlaceholder.amazonURL = '';
        this.toolPlaceholder.isOwnBrand = false;
        this.toolPlaceholder.isNew = false;
        this.toolPlaceholder.isOnSale = false;
        this.toolFormType = 'none';
        this.tempId = null;
        this.goBackToMenu();
      }
  }
  editProductFormAutocomplete(obj) {
    this.toolFormType = 'edit';
    this.tempId = obj.id;
    this.toolPlaceholder.name = obj.name;
    this.toolPlaceholder.price = obj.price;
    this.toolPlaceholder.pictureURL = obj.pictureURL;
    this.toolPlaceholder.amazonURL = obj.amazonURL;
    this.toolPlaceholder.isOwnBrand = obj.isOwnBrand;
    this.toolPlaceholder.isNew = obj.isNew;
    this.toolPlaceholder.isOnSale = obj.isOnSale;

  }

  uploadPicture() {
    if (this.pictureFile.nativeElement.value) {
      this.adminService.imageApiCall(this.toolPlaceholder.name, this.pictureFile.nativeElement).subscribe((output) => {
        if (output['type'] === 'error') {
          this.notifs.error(output['msg'], '', {timeOut: 4000});
        }
        else {
          this.notifs.success(output['msg'], '', {timeOut: 4000});
          this.toolPlaceholder.pictureURL = output['url'];
        }
      });
    }
  }


  // ======================
  // SPECIALISTS MANAGEMENT
  // ======================
  proRD(action) { // RD for Read & Delete
    if (action === 'read') { 
      this.adminService.getPro(this.proEmail).subscribe((res) => {
        this.proFound = res['list'];
        this.proEmail = '';
      });
    }
    if (action === 'delete') {
      this.adminService.deletePro(this.proFound.email).subscribe((data) => {
        this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        this.proFound = null;
      });
    }
  }


  // =====================
  // ARTICLES MANAGEMENT
  // =====================
  articleCRUD(type, obj) {
    if (type === 'delete') {
      this.adminService.deleteArticle(obj.id).subscribe((data) => {
        if (!data) {
          this.notifs.error('Oops..', '', {timeOut: 4000, animate: "fromBottom"});
        } else {
        this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
      }});
      this.goBackToMenu();
    } else {
      if (this.articleStateControl === 'new') {
        this.adminService.createArticle(this.articleElement).subscribe((data) => {
          this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        });
        this.articleElement.title = '';
        this.articleElement.category = '';
        this.articleElement.bgImgUrl = '';
        this.articleElement.contentFileUrl = '';
        this.articleElement.readTime = '';
        this.articleElement.difficultyLevel = '';
        this.articleStateControl = 'none';
        this.goBackToMenu();
        };
      }
      if (this.articleStateControl === 'edit') {
        this.adminService.updateArticle(this.currentArticleId, this.articleElement).subscribe((data) => {
          this.notifs.success(data['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        });       
        this.articleElement.title = '';
        this.articleElement.category = '';
        this.articleElement.bgImgUrl = '';
        this.articleElement.contentFileUrl = '';
        this.articleElement.readTime = '';
        this.articleElement.difficultyLevel = '';
        this.articleStateControl = 'none';
        this.goBackToMenu();
      }
  }

  articleAutocomplete(obj) {
    this.articleStateControl = 'edit';
    this.currentArticleId = obj.id;
    this.articleElement.title = obj.title;
    this.articleElement.category = obj.category;
    this.articleElement.bgImgUrl = obj.bgImgUrl;
    this.articleElement.contentFileUrl = obj.contentFileUrl;
    this.articleElement.readTime = obj.readTime;
    this.articleElement.difficultyLevel = obj.difficultyLevel;
  }

  fetchArticles() {
    this.adminService.getArticles(this.searchBy).subscribe((res) => {
      this.items = res['list'];
    })
  }

}
