import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { ContentComponent } from './content/content.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

const components = [
  AdminComponent,
  ContentComponent
]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
      path: '',
      component: AdminComponent
      },
      {
      path: 'content',
      component: ContentComponent
      },
    ]),
  ],
  declarations: [
    ...components,
  ],
  exports: [
    ...components
  ]
})
export class AdminModule { }
