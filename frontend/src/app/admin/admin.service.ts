import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  getList(type) {
    if (type === "specialties") {
      return this.http.get('/api/specialists/specialties');
    } if (type === "blog categories") {
      return this.http.get('api/learning/categories');
    }
  }

  createInstance(type, newValue) {
    if (type === "specialties") {
      return this.http.post('/api/specialists/specialties', newValue);
    } if (type === "blog categories") {
      // newValue is actually a dictionary in this case
      return this.http.post('api/learning/categories', newValue);
    }
  }

  updateInstance(type, val) {
    if (type === "specialties") {
      return this.http.patch('/api/specialists/specialties', val);
    } if (type === "blog categories") {
      // val is actually a dictionary in this case
      return this.http.patch('api/learning/categories', val);
    }
  }

  deleteInstance(type, val) {
    if (type === "specialties") {
      return this.http.delete('/api/specialists/specialties/' + val);;
    } if (type === "blog categories") {
      return this.http.delete('api/learning/categories/' + val);
    }
  }

  // TOOLS
  createProductInstance(dict) {
    return this.http.post('/api/tools', dict);
  }

  getProdutsDict() {
    return this.http.get('api/tools');
  }

  editProductInstance(id, dict) {
    return this.http.put('api/tools', {id: id, dict: dict});
  }

  deleteProductInstance(dict) {
    return this.http.delete('api/tools/' + dict.id);
  }

  imageApiCall(name, picture) {
    var imageFormData = new FormData();
    imageFormData.append('picture', picture.files.item(0));
    return this.http.post('api/tools/img/' + name, imageFormData);
  }


  // Pros
  getPro(email) {
    return this.http.get('api/specialists/' + email);
  }

  deletePro(email) {
    return this.http.delete('api/specialists/' + email); 
  }

  // Articles
  getArticles(category) {
      return this.http.get('api/learning/articles/' + category);
  }

  createArticle(newValue) {
      return this.http.post('api/learning/articles', newValue);
  }

  updateArticle(id, newVersion) {
      return this.http.patch('api/learning/articles', {id, newVersion});
  }

  deleteArticle(id) {
      return this.http.delete('api/learning/articles/' + id);
  }


  // other - password auth
  checkValue(value) {
    return this.http.post('api/users/other/3070818948930944333033120', {value})
  }

}
