import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from './admin.service';
import { TokenServiceService } from '../shared/services/token-service.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  isLoggedin: boolean = false;
  pw: string;

  constructor(private routerLink: Router, private adminService: AdminService, private tokenService: TokenServiceService) { }

  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.tokenService.decodeToken(this.tokenService.getToken()).subscribe((res) => {
        if (!res['msg']) {
          let t_now = new Date();
          let t_exp = new Date(res['exp']);
          if (t_exp <= t_now) {
            this.tokenService.removeToken();
          } else {
            this.isLoggedin = true;
          }
        }
      })
    }
  }

  login() {
    this.adminService.checkValue(this.pw).subscribe((res) => {
      if (res['msg'] !== 'passed') {
        alert('access denied');
      } else {
        this.tokenService.setToken(res["token"]);
        this.isLoggedin = true;
      }
    })
  }

}
