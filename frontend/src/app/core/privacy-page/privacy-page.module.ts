import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyPageComponent } from './privacy-page.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: PrivacyPageComponent
    }]),
  ],
  declarations: [PrivacyPageComponent]
})
export class PrivacyPageModule { }
