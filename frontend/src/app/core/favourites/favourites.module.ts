import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavouritesComponent } from './favourites.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { SpecialistsListModule } from '../specialists/specialists-list/specialists-list.module';
import { LearningCategoryModule } from '../learning/learning-category/learning-category.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: FavouritesComponent
    }]),
    SpecialistsListModule,
    LearningCategoryModule
  ],
  declarations: [FavouritesComponent]
})
export class FavouritesModule { }
