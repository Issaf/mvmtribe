import { TestBed, inject } from '@angular/core/testing';

import { SpecialistsService } from './specialists.service';

describe('SpecialistsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpecialistsService]
    });
  });

  it('should be created', inject([SpecialistsService], (service: SpecialistsService) => {
    expect(service).toBeTruthy();
  }));
});
