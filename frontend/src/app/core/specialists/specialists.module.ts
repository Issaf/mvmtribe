import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialistSearchComponent } from './specialist-search/specialist-search.component';
import { specialistsListComponent } from './specialists-list/specialists-list.component';
import { SpecialistsResultsComponent } from './specialists-results/specialists-results.component';
import { SpecialistComponent } from './specialist/specialist.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ClipboardModule } from 'ngx-clipboard';
import { SpecialistsListModule } from './specialists-list/specialists-list.module';
import { StandAloneSpecialistComponent } from './stand-alone-specialist/stand-alone-specialist.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
      path: '',
      component: SpecialistSearchComponent
      },
      {
      path: 'search',
      component: SpecialistsResultsComponent
      },
      {
        path: 'individual/:id',
        component: StandAloneSpecialistComponent
      }
    ]),
    SpecialistsListModule,
    ClipboardModule
  ],
  exports: [specialistsListComponent, SpecialistComponent],
  declarations: [SpecialistSearchComponent, SpecialistsResultsComponent, SpecialistComponent, StandAloneSpecialistComponent ]
})
export class SpecialistsModule { }
