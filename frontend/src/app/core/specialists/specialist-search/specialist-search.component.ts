import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { SpecialistsService } from '../specialists.service';
import { LocationAutocompleteService } from '../../../shared/services/location-autocomplete.service';
import { FormControl } from '@angular/forms';
import 'rxjs/Rx';

@Component({
  selector: 'app-specialist-search',
  templateUrl: './specialist-search.component.html',
  styleUrls: ['./specialist-search.component.scss']
})
export class SpecialistSearchComponent implements OnInit {
  
  constructor(private routerLink: Router, private SpeService: SpecialistsService, private citiesService: LocationAutocompleteService ) { }
  cityFromAutocomplete = new FormControl();
  resultCities: any;

  searchObject = {
    bySpecialty: true,
    specialty: '',
    jobTitle: '',
    city: ''
  };

  specialties: any;
  jobs: any;

  ngOnInit() {
    this.SpeService.fetchSpecialties().subscribe((data) => {
      this.specialties = data['list'];
    });
    this.SpeService.fetchJobTitles().subscribe((data) => {
      this.jobs = data['list'];
    });
  
    this.cityFromAutocomplete.valueChanges
    .debounceTime(400)
    .distinctUntilChanged() 
    .subscribe(term => {
      this.resultCities = this.citiesService.getCities(term);
    });
  
  }

  selectedCity(name) {
    this.searchObject.city = name;
    this.cityFromAutocomplete.patchValue('');
  }

  search() {
    this.routerLink.navigate([this.routerLink.url,"search"], { queryParams:  this.searchObject });
  }

  switchSearchType(destType) {
    if (destType === 'spe') {
      this.searchObject.bySpecialty = true;
      this.searchObject.jobTitle = '';
    }
    if (destType === 'job') {
      this.searchObject.bySpecialty = false;
      this.searchObject.specialty = '';
    }
  }

}
