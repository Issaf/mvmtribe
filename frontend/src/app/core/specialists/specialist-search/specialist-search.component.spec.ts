import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialistSearchComponent } from './specialist-search.component';

describe('SpecialistSearchComponent', () => {
  let component: SpecialistSearchComponent;
  let fixture: ComponentFixture<SpecialistSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialistSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialistSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
