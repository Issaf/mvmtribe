import { SpecialistsListModule } from './specialists-list.module';

describe('SpecialistsListModule', () => {
  let specialistsListModule: SpecialistsListModule;

  beforeEach(() => {
    specialistsListModule = new SpecialistsListModule();
  });

  it('should create an instance', () => {
    expect(specialistsListModule).toBeTruthy();
  });
});
