import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { specialistsListComponent } from './specialists-list.component';

describe('specialistsListComponent', () => {
  let component: specialistsListComponent;
  let fixture: ComponentFixture<specialistsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ specialistsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(specialistsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
