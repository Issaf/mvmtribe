import { Component, OnInit, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SpecialistComponent } from '../specialist/specialist.component';
import { Router } from '@angular/router';
import { SpecialistsService } from '../specialists.service';

@Component({
  selector: 'app-specialists-list',
  templateUrl: './specialists-list.component.html',
  styleUrls: ['./specialists-list.component.scss']
})
export class specialistsListComponent implements OnInit {
  @Input() dataFromParent;
  bsModalRef: BsModalRef;
  isEmpty: boolean = false;
  prosList: any;

  constructor(private bsModalService: BsModalService, private routerLink: Router, private specialistService: SpecialistsService) { }

  ngOnInit() {
    if (this.dataFromParent.from === "specialists") {
      this.specialistService.fetchSpecialists(this.dataFromParent.data).subscribe((res) => {
        this.prosList = res['list'];
        if (this.prosList.length === 0) { this.isEmpty = true; }
      });
      
    }
    //  SECTION FOR FAVOURITES - NOT NEEDED NOW
    // if (this.dataFromParent.from === "favourites") {
    //   // fetch appropriate data
    //   // if empty set the the boolean to true
    //   this.isEmpty = false;
    // }

  }

  openFullProfile(proId) {
    var initialState = { from: 'specialists', id: proId };
    this.bsModalRef = this.bsModalService.show(SpecialistComponent, { initialState, class: 'modal-dialog-centered' });
  }

  elementDisplay(name) {
    if (this.dataFromParent.from === name && this.isEmpty) {
      return true;
    } else {
      return false;
    }
  }
}
