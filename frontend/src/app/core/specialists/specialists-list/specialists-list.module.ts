import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { specialistsListComponent } from './specialists-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [specialistsListComponent],
  declarations: [specialistsListComponent]
})
export class SpecialistsListModule { }
