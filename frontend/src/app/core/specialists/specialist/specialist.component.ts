import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ShareModalComponent } from '../../../shared/share-modal/share-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { SpecialistsService } from '../specialists.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-specialist',
  templateUrl: './specialist.component.html',
  styleUrls: ['./specialist.component.scss']
})
export class SpecialistComponent implements OnInit {
  shareModalRef: BsModalRef;
  state = {};
  modalColor: string = 'pro-modal-color1';
  showContact: boolean = false;
  langs = [['english', 'french', 'spanish', 'chinese', 'hindi', 'arabic'],
    {
    'english': 'en',
    'french': 'fr',
    'spanish': 'sp',
    'chinese': 'zh',
    'hindi': 'hi',
    'arabic': 'ar'
  }];

  pro: any;
  // pro = {
  //   firstName: "Eliott",
  //   lastName: "Hulse",
  //   languages: ['true', 'true', 'false', 'false', 'false'],
  //   jobTitle: "Strength & conditioning coach",
  //   location: "Tampa, FL, USA",
  //   institution: "Strength Camp",
  //   flexible: "Yes",
  //   pictureURL: "../../../../assets/images/pic.png",
  //   phone: "413-345-6492",
  //   email: "eliott.hulse@strengthcamp.com",
  //   website: "www.mvmtribe.com",
  //   bio: `Lorie Smith is a Loan Officer at XYZ Bank, where Lorie processes loan applications from start to finish, including mortgage
  //   refinancing and educating clients about their different financing options. Lorie has worked with reputable real
  //   estate agencies, including ReMax, Century 21, and Coldwell Banker, among others. Lorie helps homeowners and new
  //   buyers secure a loan that suits their budget and goals. You can expect 100% transparency, no horror stories, and
  //   nasty surprises when working with Lorie.`
  // }

  constructor(public bsModalRef: BsModalRef, private bsModalService: BsModalService, 
    private routerLink: Router, private proService: SpecialistsService,
    private notifs: NotificationsService) { }

  ngOnInit() {
    // Generating random class
    this.modalColor = 'pro-modal-color' + Math.floor((Math.random() * 5) + 1).toString();
    this.state['from'] = this['from'];
    this.state['id'] = this['id'];
    this.proService.fetchSpecialistDetails(this.state['id']).subscribe((res) => {
      this.pro = res['list'];
      if (this.pro.flexible == true) { this.pro.flexible = 'yes' }
      if (this.pro.flexible == false) { this.pro.flexible = 'no' }
    });
  }

  showInfos() {
    this.showContact = true;
  }
  copiedInfo() {
    this.notifs.info('Copied to clipboard', '', {timeOut: 4000, animate: "fromBottom"});
  }
  
  langsFormatting() {
    var tempList = [];
    for (let i=0 ; i < this.pro.languages.length ; i++) {
      if (this.pro.languages[i] == 'true') {
        tempList.push(this.langs[1][this.langs[0][i]]);
      } 
    }
    return tempList.join(' • ');
  }

  openSharingModal() {
    var initialState = {type: 'specialist', id: '1'}
    this.shareModalRef = this.bsModalService.show(ShareModalComponent, { initialState, class: 'modal-dialog-centered' });
  }

  closeModal() {
    this.bsModalRef.hide();
    if (this.state['from'] === "standalone") {
      this.routerLink.navigate(['specialists']);
    }
  }

}
