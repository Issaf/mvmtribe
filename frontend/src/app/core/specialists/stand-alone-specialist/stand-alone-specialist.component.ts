import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SpecialistComponent } from '../specialist/specialist.component';

@Component({
  selector: 'app-stand-alone-specialist',
  templateUrl: './stand-alone-specialist.component.html',
  styleUrls: ['./stand-alone-specialist.component.scss']
})
export class StandAloneSpecialistComponent implements OnInit {

  constructor(private bsModalService: BsModalService, private routerLink: Router) { }
  bsModalRef: BsModalRef;
  specialistId: string;

  ngOnInit() {
    this.getIdFromUrl();
    this.openFullProfile();
  }

  openFullProfile() {
   var initialState = {from:'standalone', id: this.specialistId};
    this.bsModalRef = this.bsModalService.show(SpecialistComponent, { initialState, ignoreBackdropClick: true, class: 'modal-dialog-centered'});
  }

  getIdFromUrl() {
    var urlElements = this.routerLink.url.split('/');
    var individualIndex = urlElements.indexOf('individual');
    this.specialistId = urlElements[individualIndex+1];
  }

}
