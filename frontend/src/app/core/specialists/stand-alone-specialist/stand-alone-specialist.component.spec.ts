import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandAloneSpecialistComponent } from './stand-alone-specialist.component';

describe('StandAloneSpecialistComponent', () => {
  let component: StandAloneSpecialistComponent;
  let fixture: ComponentFixture<StandAloneSpecialistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandAloneSpecialistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandAloneSpecialistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
