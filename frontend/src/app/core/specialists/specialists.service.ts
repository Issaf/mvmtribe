import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SpecialistsService {

  constructor(private http: HttpClient) { }

  fetchSpecialties() {
    return this.http.get('/api/specialists/specialties');
  }

  fetchJobTitles() {
    return this.http.get('/api/specialists/job-titles');
  }

  fetchSpecialists(params) {
    return this.http.post('/api/specialists', params);
  }

  fetchSpecialistDetails(id) {
    return this.http.get('/api/specialists/byId/' + id);
  }

}
