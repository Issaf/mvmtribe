import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-specialists',
  templateUrl: './specialists-results.component.html',
  styleUrls: ['./specialists-results.component.scss']
})
export class SpecialistsResultsComponent implements OnInit {
  searchDetails: any;
  constructor(private routerLink: Router, private router: ActivatedRoute) { }

  ngOnInit() {
    this.router.queryParams.subscribe((params) => {
      this.searchDetails = params;
    })
  }

  resultsSentence() {
    var part1 = '';
    var part2 = 'in ';
    if (this.searchDetails.bySpecialty === 'true' && this.searchDetails.specialty) {
      part1 = this.searchDetails.specialty + ' specialists ';
    }
    else if (this.searchDetails.bySpecialty === 'false' && this.searchDetails.jobTitle) {
      part1 = this.searchDetails.jobTitle + 's ';
    }
    else if (!this.searchDetails.specialty && !this.searchDetails.jobTitle) {
      part1 = 'specialists ';
    }

    if(this.searchDetails.city) {
      part2 = part2 + this.searchDetails.city;
    }
    if(!this.searchDetails.city) {
      part2 = part2 + 'the world';
    }
    return part1 + part2;
  }

}
