import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialistsResultsComponent } from './specialists-results.component';

describe('SpecialistsResultsComponent', () => {
  let component: SpecialistsResultsComponent;
  let fixture: ComponentFixture<SpecialistsResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialistsResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialistsResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
