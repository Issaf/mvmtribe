import { SpecialistsModule } from './specialists.module';

describe('SpecialistsModule', () => {
  let specialistsModule: SpecialistsModule;

  beforeEach(() => {
    specialistsModule = new SpecialistsModule();
  });

  it('should create an instance', () => {
    expect(specialistsModule).toBeTruthy();
  });
});
