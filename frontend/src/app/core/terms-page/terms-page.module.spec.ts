import { TermsPageModule } from './terms-page.module';

describe('TermsPageModule', () => {
  let termsPageModule: TermsPageModule;

  beforeEach(() => {
    termsPageModule = new TermsPageModule();
  });

  it('should create an instance', () => {
    expect(termsPageModule).toBeTruthy();
  });
});
