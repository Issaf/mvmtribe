import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsPageComponent } from './terms-page.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: TermsPageComponent
    }]),
  ],
  declarations: [TermsPageComponent]
})
export class TermsPageModule { }
