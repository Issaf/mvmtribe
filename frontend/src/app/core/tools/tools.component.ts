import { Component, OnInit } from '@angular/core';
import { ToolsService } from './tools.service';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {
  items: any;

  constructor(private toolsService: ToolsService) { }

  ngOnInit() {
    this.toolsService.getAll().subscribe((res) => {
      this.items = res['list'];
    })
  }

}
