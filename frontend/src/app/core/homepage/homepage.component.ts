import { Component, OnInit, HostListener } from '@angular/core';
import { EasingLogic } from 'ngx-page-scroll';
import { Router } from '@angular/router';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    if (window.innerWidth <= 500) {
      this.scrollOffset = -15;
    } else {
      this.scrollOffset = 40;
    }
}

  scrollOffset: any;
  constructor(private routerLink: Router) {
    this.onResize()
  }

  ngOnInit() {
  }

  easeInOutExpo: EasingLogic = {
    ease: (t: number, b: number, c: number, d: number): number => {
      // easeInOutExpo easing
      if (t === 0) return b;
      if (t === d) return b + c;
      if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
      return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
  };

}
