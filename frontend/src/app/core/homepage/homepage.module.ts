import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import { ParticlesModule } from 'angular-particle';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: HomepageComponent
    }]),
    NgxPageScrollModule,
    ParticlesModule
  ],
  declarations: [HomepageComponent]
})
export class HomepageModule { }
