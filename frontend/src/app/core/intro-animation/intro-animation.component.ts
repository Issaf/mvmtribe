import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-intro-animation',
  templateUrl: './intro-animation.component.html',
  styleUrls: ['./intro-animation.component.scss']
})
export class IntroAnimationComponent implements OnInit {

  constructor(private routerLink: Router) { }

  ngOnInit() {
    setTimeout((routerLink: Router) => {
      this.routerLink.navigate(['homepage']);
  }, 1480); 

  }
}
