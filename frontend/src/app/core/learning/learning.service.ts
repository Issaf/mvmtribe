import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LearningService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get('api/learning/categories');
  }

  getArticles(category) {
    return this.http.get('api/learning/articles/' + category);
  }

  getArticle(title) {
    return this.http.get('api/learning/' + title);
  }

  getMoreArticles(id) {
    return this.http.get('api/learning/articles/not/' + id);
  }

  saveEmail(obj) {
    return this.http.post('api/saveEmail', obj);
  }
  
}
