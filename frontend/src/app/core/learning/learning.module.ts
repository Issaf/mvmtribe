import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LearningMainComponent } from './learning-main/learning-main.component';
import { LearningArticleComponent } from './learning-article/learning-article.component';
import { LearningCategoryResultsComponent } from './learning-category-results/learning-category-results.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import { LearningCategoryModule } from './learning-category/learning-category.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild([
      {
      path: '',
      component: LearningMainComponent
      },
      {
        path: ':category',
        component: LearningCategoryResultsComponent
      },
      {
        path: ':category/:title',
        component: LearningArticleComponent
      }
    ]),
    LearningCategoryModule,
    NgxPageScrollModule
  ],
  declarations: [LearningMainComponent, LearningArticleComponent, LearningCategoryResultsComponent]
})
export class LearningModule { }
