import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningArticleComponent } from './learning-article.component';

describe('LearningArticleComponent', () => {
  let component: LearningArticleComponent;
  let fixture: ComponentFixture<LearningArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearningArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
