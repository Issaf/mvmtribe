import { Component, OnInit, HostListener, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ShareModalComponent } from '../../../shared/share-modal/share-modal.component';
import { DomSanitizer } from '@angular/platform-browser';
import { LearningService } from '../learning.service';
import { LearningCategoryComponent } from '../learning-category/learning-category.component';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-learning-article',
  templateUrl: './learning-article.component.html',
  styleUrls: ['./learning-article.component.scss']
})
export class LearningArticleComponent implements OnInit {
  bsModalRef: BsModalRef;
  @ViewChild('article')  articleElement: ElementRef;
  @HostListener('window:scroll', ['$event'])
  isSidebarShown() {
      let start = this.articleElement.nativeElement.offsetTop;
      let end = this.articleElement.nativeElement.offsetHeight;
      if (window.scrollY > start * .7 && window.scrollY < end * .95) {return true; } 
      else { return false; }
  }

  @ViewChild (LearningCategoryComponent) moreArticles;

  currentElements = {
    category: '',
    article: ''
  };

  articleInfos: any;
  articleContent: any;

  // Saving email section
  userEmail = {
    isValid: true,
    isSaved: false,
    value: ''
  };

  constructor(private routerLink: Router, private bsModalService: BsModalService, private sanitizer: DomSanitizer, private learningService: LearningService, private notifs: NotificationsService) { }

  ngOnInit() {
    let urlParmas = this.routerLink.url.split('/');
    this.currentElements.article = urlParmas[urlParmas.length -1];
    this.currentElements.category = urlParmas[urlParmas.length -2];
    
    this.learningService.getArticle(this.currentElements.article).subscribe((res) => {
      if (res['type'] === 'error') { this.goBack(); }
      if (res['type'] === 'success') {
        this.articleInfos = res['infos'];
        this.articleContent= this.sanitizer.bypassSecurityTrustHtml(res['content']);
      }
    })
  }

  goBack() {
    return ['/learning', this.currentElements.category];
  }

  openSocialsModal() {
    var initialState = {type: 'article', id: '1'}
    this.bsModalRef = this.bsModalService.show(ShareModalComponent, { initialState, class: 'modal-dialog-centered' });
}

  saveEmail() {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(String(this.userEmail.value).toLowerCase())) {
      this.learningService.saveEmail({email: this.userEmail.value, articleId: this.articleInfos.id}).subscribe((res) => {
        this.notifs.success(res['msg'], '', {timeOut: 4000, animate: "fromBottom"});
        this.userEmail.isSaved = true;
      })
    } else {
        this.userEmail.isValid = false;
    }
  }

}
