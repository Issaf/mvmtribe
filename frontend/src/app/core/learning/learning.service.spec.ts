import { TestBed, inject } from '@angular/core/testing';

import { LearningService } from './learning.service';

describe('LearningService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LearningService]
    });
  });

  it('should be created', inject([LearningService], (service: LearningService) => {
    expect(service).toBeTruthy();
  }));
});
