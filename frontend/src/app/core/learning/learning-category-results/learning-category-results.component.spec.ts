import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningCategoryResultsComponent } from './learning-category-results.component';

describe('LearningCategoryResultsComponent', () => {
  let component: LearningCategoryResultsComponent;
  let fixture: ComponentFixture<LearningCategoryResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearningCategoryResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningCategoryResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
