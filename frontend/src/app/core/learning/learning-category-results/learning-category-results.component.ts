import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LearningCategoryComponent } from '../learning-category/learning-category.component';

@Component({
  selector: 'app-learning-category-results',
  templateUrl: './learning-category-results.component.html',
  styleUrls: ['./learning-category-results.component.scss']
})
export class LearningCategoryResultsComponent implements OnInit {
  @ViewChild (LearningCategoryComponent) results;
  currentCategory: string = null;
  constructor(private routerLink: Router) { }

  ngOnInit() {
    this.currentCategory = this.routerLink.url.split('/')[2].replace('-', ' ');
  }

}
