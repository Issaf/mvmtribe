import { LearningCategoryModule } from './learning-category.module';

describe('LearningCategoryModule', () => {
  let learningCategoryModule: LearningCategoryModule;

  beforeEach(() => {
    learningCategoryModule = new LearningCategoryModule();
  });

  it('should create an instance', () => {
    expect(learningCategoryModule).toBeTruthy();
  });
});
