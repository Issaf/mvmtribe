import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LearningService } from '../learning.service';

@Component({
  selector: 'app-learning-category',
  templateUrl: './learning-category.component.html',
  styleUrls: ['./learning-category.component.scss']
})
export class LearningCategoryComponent implements OnInit {
  @Input() dataFromParent;
  currentCategory: string = null;
  articlesList = null;

  constructor(private routerLink: Router, private learningService: LearningService) { }

  ngOnInit() {
    if (this.dataFromParent.type === 'category') {
      this.currentCategory = this.dataFromParent.value;
      this.learningService.getArticles(this.currentCategory).subscribe((res) => {
        this.articlesList = res['list'];
      });
    
    } else if (this.dataFromParent.type === 'moreArticles') {
        this.learningService.getMoreArticles(this.dataFromParent.value).subscribe((res) => {
          this.articlesList = res['list'];
        });
    
    } else {
        this.routerLink.navigate(['learning']);
    }
  }

  goToArticle(article) {
    return '/learning/' + article.category + '/' + article.title;
  }
}
