import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LearningCategoryComponent } from './learning-category.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [LearningCategoryComponent],
  declarations: [LearningCategoryComponent]
})
export class LearningCategoryModule { }
