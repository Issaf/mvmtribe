import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LearningService } from '../learning.service';

@Component({
  selector: 'app-learning-main',
  templateUrl: './learning-main.component.html',
  styleUrls: ['./learning-main.component.scss']
})
export class LearningMainComponent implements OnInit {
  bgColors = [
    // "#8FD8d2","#FEDCD2","#DF744A","#DC8239"
    // "#6B7A8F","#F7882F","#F7C331","#DCC7AA"
    // "#6BBAA7","#FBA100","#6C648B","#B6A19E"
    "#A9B7C0","#C7D8C6","#EFD9C1","#CCCBC6"
  ]

  allCategories: any;
  
  constructor(private routerLink: Router, private activatedRoute: ActivatedRoute, private learningService: LearningService) { }

  ngOnInit() {
    this.learningService.getCategories().subscribe((res) => {
      this.allCategories = res['list'];
    })
  }

  categorySelected(category) {
    // this.routerLink.navigate([this.routerLink.url, category.replace(' ', '-')]);
    return [this.routerLink.url, category.replace(' ', '-')];
  }

}
