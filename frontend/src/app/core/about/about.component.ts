import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { DOCUMENT} from '@angular/common';
import { PageScrollConfig, PageScrollService, PageScrollInstance, EasingLogic } from 'ngx-page-scroll';
 
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  @ViewChild('contactUs')
  private container: ElementRef;
  
  email = "connect@mvmtribe.com";
  
  constructor(
    private routerLink: Router,
    private notifs: NotificationsService,
    private pageScrollService: PageScrollService, 
    @Inject(DOCUMENT) private document: any,
    private route: ActivatedRoute,
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['isContact']) { 
        this.goToContactUsSection();
      }
    });
  }

  public goToContactUsSection(): void {
    let pageScrollInstance: PageScrollInstance = PageScrollInstance.newInstance({
      document: this.document,
      scrollTarget: '#contactUs',
      pageScrollOffset: 50,
      pageScrollDuration: 1000,
      pageScrollEasingLogic: this.easeInOutExpo
    });
    this.pageScrollService.start(pageScrollInstance);
};   

  copiedInfo() {
    this.notifs.info("Copied to clipboard", '', {timeOut: 4000, animate: "fromBottom"});
  }


  easeInOutExpo: EasingLogic = {
    ease: (t: number, b: number, c: number, d: number): number => {
      // easeInOutExpo easing
      if (t === 0) return b;
      if (t === d) return b + c;
      if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
      return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
  };
}
