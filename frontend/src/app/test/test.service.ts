import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }

  getCall(data) {
    return this.http.post('api/test-get', data);
  }

  postCall(data) {
    return this.http.post('/api/test-post', data);
  }
}
